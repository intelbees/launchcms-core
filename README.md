# LaunchCMS Core

[![build status](https://gitlab.com/intelbees/launchcms-core/badges/master/build.svg)](https://gitlab.com/intelbees/launchcms-core/tree/master)

This is the launchcms-core package which provides all of main functionality of LaunchCMS.

## Install

Via Composer

``` bash
$ composer require intelbees/launchcms-core
```



## Credits

- [khoatrand][http://excitingthing.com]


## License

The MIT License (MIT). 
