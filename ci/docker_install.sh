#!/bin/bash
echo Starting the build process

set -xe


# Install phpunit, the tool that we will use for testing
curl -o /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Run mongod
nohup sh -c mongod &

#service mysql start
#echo "create database launchcms" | mysql -uroot -psecret
#cat sentinel-mysql-schema.sql | mysql -uroot -psecret

# Run redis
service redis_6379 start

# Run elastic search
service elasticsearch start

composer update
echo Finish the preparation

