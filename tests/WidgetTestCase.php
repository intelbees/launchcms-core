<?php

class WidgetTestCase extends Orchestra\Testbench\TestCase
{
    protected $app;
    protected $sentinel;

    /**
     * Get package providers.
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            'Jenssegers\Mongodb\MongodbServiceProvider',
            'LaunchCMS\LaunchCoreServiceProvider',

        ];
    }

    /**
     * Define environment setup.
     *
     * @param  Illuminate\Foundation\Application $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // reset base path to point to our package's src directory
        //$app['path.base'] = __DIR__ . '/../src';

        $config = require 'config/database.php';


        $app[ 'config' ]->set('database.default', 'mongodb');
        $app[ 'config' ]->set('database.connections.mysql', $config[ 'connections' ][ 'mysql' ]);
        $app[ 'config' ]->set('database.connections.mongodb', $config[ 'connections' ][ 'mongodb' ]);
        $app[ 'config' ]->set('database.connections.mongodb_admin', $config[ 'connections' ][ 'mongodb_admin' ]);
        $app[ 'config' ]->set('cache.driver', 'array');
        $widgetConfig = require 'config/launchcms.php';
        $app[ 'config' ]->set('launchcms', $widgetConfig);
        $this->app = $app;

    }
}