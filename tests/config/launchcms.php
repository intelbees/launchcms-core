<?php
return [
    'admin_slug' =>  'admin',
    'default_elastic_index' =>  'launchcms_test',
    'widgets' => [
        'test-widget' => [
            'class' => 'TestWidget'
        ],
        'new-order' => [
            'class' => 'TestWidget'
        ],
        'bounce-rate' => [
            'class' => 'TestWidget'
        ]
    ],
    'pages' => [
        'dashboard' => [
            'blocks' => [
                'stats' => [
                    'new-order', 'bounce-rate'
                ]
            ]
        ]
    ],
    'media' => [
        'webpath' => 'http://assets.launchcms.com'
    ]

];