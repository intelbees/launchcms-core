<?php

return [

    'connections' => [

        'mongodb' => [
            'name'       => 'mongodb',
            'driver'     => 'mongodb',
            'host'       => 'localhost',
            'database'   => 'unittest',
            //'username' => 'intelbees', //uncomment this line and update username if you use MongoDB Auth mode
            //'password' => 'secret', //uncomment this line and update username if you use MongoDB Auth mode
        ],
        'mongodb_admin' => [
            'name'       => 'mongodb',
            'driver'     => 'mongodb',
            'host'       => 'localhost',
            'database'   => 'admin',
            //'username' => 'intelbees', //uncomment this line and update username if you use MongoDB Auth mode
            //'password' => 'secret', //uncomment this line and update username if you use MongoDB Auth mode
        ],
        'mysql' => [
            'driver'    => 'mysql',
            'host'      => '127.0.0.1',
            'database'  => 'launchcms',
            'username'  => 'root',
            'password'  => 'secret',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
    ],

    'redis' => [

        'cluster' => false,

        'default' => [
            'host'     => '127.0.0.1',
            'port'     => 6379,
            'database' => 0,
        ],

    ],

];