<?php

use Illuminate\Support\Facades\Schema as Schema;
use LaunchCMS\MongoDB\DynamicModel;
use LaunchCMS\MongoDB\MongoHelper;

class DynamicModelTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('test_dynamic_model');
    }

    public function test_create_and_save_model()
    {
        $model = new DynamicModel();
        $model->setCollection('test_dynamic_model');
        $model->name = 'Test name';
        $model->description = 'Test description';
        $model->save();
        $record = MongoHelper::getMongoDB()->collection('test_dynamic_model')->first();
        $this->assertEquals('Test name', $record[ 'name' ]);

    }
}