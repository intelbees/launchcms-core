<?php

use LaunchCMS\Events\DataTypeWasCreated;
use LaunchCMS\Services\Exceptions\DataTypeException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Models\Content\EmbeddedField;
use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Models\Content\IntegerField;
use LaunchCMS\Models\Content\BooleanField;
use LaunchCMS\Models\Content\DoubleField;
use LaunchCMS\Models\Content\DateField;
use LaunchCMS\Models\Content\DateTimeField;
use LaunchCMS\Models\Content\ReferenceListField;
use LaunchCMS\Models\Content\ReferenceField;
use LaunchCMS\Models\Content\ArrayField;
use LaunchCMS\Models\Content\GeoField;

class DataTypeTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('test_content_type');
        Schema::drop('test_content_type_new');
        Schema::drop('test_content_type_ref');
        Schema::drop('order');
        Mockery::close();

    }
    private function createSampleStringField($name, $description, $alias)
    {
        $stringField = new StringField();
        $stringField->setName($name);
        $stringField->setDescription($description);
        $stringField->setAlias($alias);

        return $stringField;
    }

    public function test_create_data_type_must_throw_data_type_was_created_event()
    {
        $this->expectsEvents(DataTypeWasCreated::class);
        StructureService::createDataType([ 'name' => 'Order detail', 'alias' => 'order_detail', 'description' => 'Order detail line' ]);
    }



    public function test_create_data_type_without_alias_must_fail()
    {
        try {
            StructureService::createDataType([ 'name' => 'Order detail', 'description' => 'Order detail line' ]);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof DataTypeException);
            $this->assertEquals(DataTypeException::DATA_TYPE_EMPTY_ALIAS, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_delete_data_type_if_still_have_reference() {

        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);

        $shippingDataType = StructureService::createDataType([ 'name'        => 'Shipping info',
                                                               'description' => 'Shipping info', 'alias' => 'shipping_info']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        StructureService::addFieldToDataType('shipping_info', $addressField);

        $embeddedShippingField = new EmbeddedField($shippingDataType->_id);
        $embeddedShippingField->setName('shipping');
        $embeddedShippingField->setAlias('shipping');
        StructureService::addField('order', $embeddedShippingField);
        try {
            StructureService::deleteDataTypeByID($shippingDataType->_id);
        } catch(Exception $ex) {
            $this->assertTrue($ex instanceof DataTypeException);
            $this->assertEquals(DataTypeException::DATA_TYPE_STILL_HAVE_REFERENCE, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');

    }

    public function test_can_create_simple_field_of_data_type()
    {
        StructureService::createDataType([ 'name' => 'Order detail', 'alias' => 'order_detail', 'description' => 'Order detail line' ]);
        $integerField = new IntegerField();
        $integerField->setName('Quantity');
        $integerField->setAlias('quantity');
        StructureService::addFieldToDataType('order_detail', $integerField);
        /** @var \LaunchCMS\Models\Content\DataType $orderDetailDataType */
        $orderDetailDataType = StructureService::getDataTypeByAlias('order_detail');
        $this->assertNotNull($orderDetailDataType->getField('quantity'));
    }

    public function test_cannot_add_field_to_data_type_with_same_alias()
    {
        StructureService::createDataType([ 'name' => 'Order detail', 'alias' => 'order_detail', 'description' => 'Order detail line' ]);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addFieldToDataType('order_detail', $field);
        try {
            $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
            StructureService::addFieldToDataType('order_detail', $field);

        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof \LaunchCMS\Services\Exceptions\FieldException);
            $this->assertEquals(\LaunchCMS\Services\Exceptions\FieldException::DUPLICATED_FIELD, $ex->getCode());

            return;
        }
        $this->fail('Expect to throw exception because field alias already exists');
    }

    public function test_can_add_supported_field_type_to_data_type() {
        StructureService::createDataType([ 'name' => 'Order detail', 'alias' => 'order_detail', 'description' => 'Order detail line' ]);
        $fields = [];
        $stringField = new StringField();
        $stringField->setName('note');
        $stringField->setAlias('note');
        $fields [] = $stringField;

        $booleanField = new BooleanField();
        $booleanField->setName('Is shipped');
        $booleanField->setAlias('is_shipped');
        $fields [] = $booleanField;

        $integerField = new IntegerField();
        $integerField->setName('Quantity');
        $integerField->setAlias('quantity');
        $fields [] = $integerField;

        $doubleField = new DoubleField();
        $doubleField->setName('Price');
        $doubleField->setAlias('price');
        $fields [] = $doubleField;

        $dateField = new DateField();
        $dateField->setName('Shipping date');
        $dateField->setAlias('shipping_date');
        $fields [] = $dateField;

        $dateTimeField = new DateTimeField();
        $dateTimeField->setName('Receiving time');
        $dateTimeField->setAlias('receiving_time');
        $fields [] = $dateTimeField;

        $arrayField = new ArrayField(\LaunchCMS\Models\Content\Field::STRING);
        $arrayField->setName('Tags');
        $arrayField->setAlias('tags');
        $fields [] = $arrayField;

        $refContentType = StructureService::createContentType([ 'name'        => 'Test Content Type For Reference',
                                                                'description' => 'Content type for testing',
                                                                'alias'       => 'test_content_type_ref']);
        $referenceField = new ReferenceField($refContentType->_id);
        $referenceField->setAlias('test_ref_field');
        $referenceField->setName('Test reference field');
        $fields [] = $referenceField;


        $referenceListField = new ReferenceListField($refContentType->_id);
        $referenceListField->setAlias('test_ref_list_field');
        $referenceListField->setName('Test reference list field');
        $fields [] = $referenceListField;

        $geoField = new GeoField();
        $geoField->setAlias('test_geo_field');
        $geoField->setName('Test geo field');
        $fields [] = $geoField;

        $gotException = false;
        foreach ($fields as $field) {
            try {
                StructureService::addFieldToDataType('order_detail', $field);
            } catch (Exception $ex) {
                $gotException = true;
                break;
            }
        }
        $this->assertEquals(false, $gotException);

    }

    public function test_cannot_update_field_in_data_type_with_other_field_alias()
    {

        StructureService::createDataType([ 'name' => 'Order detail', 'alias' => 'order_detail', 'description' => 'Order detail line' ]);
        $field = $this->createSampleStringField('test field 1', 'test field description 1', 'test_field_1');
        StructureService::addFieldToDataType('order_detail', $field);
        $field = $this->createSampleStringField('test field 2', 'test field description 1', 'test_field_2');
        StructureService::addFieldToDataType('order_detail', $field);
        $fieldInfoToUpdate = $this->createSampleStringField('test field 2', 'test field description 2', 'test_field_1');
        try {
            StructureService::updateFieldOfDataType('order_detail', 'test_field_2', $fieldInfoToUpdate);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof \LaunchCMS\Services\Exceptions\FieldException);
            $this->assertEquals(\LaunchCMS\Services\Exceptions\FieldException::DUPLICATED_FIELD, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }



}