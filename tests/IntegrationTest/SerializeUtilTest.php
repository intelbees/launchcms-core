<?php

use LaunchCMS\Utils\SerializeUtil;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Models\Content\StringField;

class SerializeUtilTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('test_content_type');
        Schema::drop('test_content_type_new');
        Schema::drop('test_content_type_ref');
        Mockery::close();

    }
    public function test_serialize_with_basic_types() {
        $this->assertEquals('Hello World', SerializeUtil::unserialize(SerializeUtil::serialize('Hello World')));
        $this->assertEquals(false, SerializeUtil::unserialize(SerializeUtil::serialize(false)));
        $this->assertEquals(true, SerializeUtil::unserialize(SerializeUtil::serialize(true)));
        $this->assertEquals(10, SerializeUtil::unserialize(SerializeUtil::serialize(10)));
        $now = new \Carbon\Carbon();
        $this->assertEquals($now, SerializeUtil::unserialize(SerializeUtil::serialize($now)));
    }


    public function test_serializing_array() {
        $arrayData = ['Hello', 'World'];
        $serializedString = SerializeUtil::serialize($arrayData);
        $unserializedData = SerializeUtil::unserialize($serializedString);
        $this->assertTrue(is_array($unserializedData));
        $this->assertEquals('Hello', $unserializedData[0]);
    }

    public function test_serializing_object_id() {
        $objectID = new \MongoDB\BSON\ObjectID('56ec34100000000000000000');
        $serializedString = SerializeUtil::serialize($objectID);
        $unserializedData = SerializeUtil::unserialize($serializedString);
        $this->assertTrue(is_string($unserializedData));
        $this->assertEquals('56ec34100000000000000000', $unserializedData);
    }

    public function test_serializing_mongo_utc_time() {
        $mongoDateTime = \LaunchCMS\Utils\DateUtil::fromDateTime(new \Carbon\Carbon());
        $serializedString = SerializeUtil::serialize($mongoDateTime);
        $unserializedData = SerializeUtil::unserialize($serializedString);
        $this->assertTrue($unserializedData instanceof \Carbon\Carbon);
    }

    public function test_serialize_carbon() {
        $now = new \Carbon\Carbon();
        $serializedString = SerializeUtil::serialize($now);
        $unserializedData = SerializeUtil::unserialize($serializedString);
        $this->assertTrue($unserializedData instanceof \Carbon\Carbon);
    }
    public function test_serializing_nested_object() {
        $now = \LaunchCMS\Utils\DateUtil::fromDateTime(new \Carbon\Carbon());
        $objectID = new \MongoDB\BSON\ObjectID('56ec34100000000000000000');
        $object = [
            '_id' => $objectID,
            'name' => 'test',
            'created_at' => $now,
            'fields' => [
                [
                    '_id' => $objectID,
                    'name' => 'test field',
                    'created_at' => $now,
                ]
            ]];
        $serializedString = SerializeUtil::serialize($object);
        $unserializedData = SerializeUtil::unserialize($serializedString);
        $this->assertTrue(is_array($unserializedData));
        $this->assertTrue(is_array($unserializedData['fields']));
        $this->assertTrue($unserializedData['fields'][0]['created_at'] instanceof \Carbon\Carbon);

    }
    private function createSampleStringField($name, $description, $alias)
    {
        $stringField = new StringField();
        $stringField->setName($name);
        $stringField->setDescription($description);
        $stringField->setAlias($alias);
        return $stringField;
    }

    public function test_is_serialized_with_object_string() {
        StructureService::createContentType([ 'name'        => 'Test Content Type',
                                              'description' => 'Content type for testing',
                                              'alias'       => 'test_content_type']);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $serializedString = serialize($contentType);

        $this->assertTrue(SerializeUtil::is_serialized($serializedString));
    }
    public function test_serializing_content_type_with_field() {
        StructureService::createContentType([ 'name'        => 'Test Content Type',
                                              'description' => 'Content type for testing',
                                              'alias'       => 'test_content_type']);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
        /** @var \LaunchCMS\Models\Content\ContentType $contentType */
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $attributes = $contentType->getAttributes();


        $serializedString = SerializeUtil::serialize($attributes);
        $this->assertTrue(SerializeUtil::is_serialized($serializedString));

        $unserializedData = SerializeUtil::unserialize($serializedString);
        $this->assertTrue(is_array($unserializedData));
        foreach($unserializedData as $key => $value) {
            $contentType->setAttribute($key, $value);
        }
    }

    public function test_serializing_data_type_with_field() {
        StructureService::createDataType([ 'name' => 'Order detail', 'alias' => 'order_detail', 'description' => 'Order detail line' ]);
        $field = $this->createSampleStringField('address', 'test field description', 'address');
        StructureService::addFieldToDataType('order_detail', $field);
        /** @var \LaunchCMS\Models\Content\DataType $orderDetailDataType */
        $orderDetailDataType = StructureService::getDataTypeByAlias('order_detail');
        $attributes = $orderDetailDataType->getAttributes();


        $serializedString = SerializeUtil::serialize($attributes);
        $this->assertTrue(SerializeUtil::is_serialized($serializedString));

        $unserializedData = SerializeUtil::unserialize($serializedString);
        $this->assertTrue(is_array($unserializedData));
        $this->assertTrue($unserializedData['created_at'] instanceof \Carbon\Carbon);
    }


}