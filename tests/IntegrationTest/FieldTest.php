<?php


use LaunchCMS\Models\Content\FieldGroup;

use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Services\Facades\StructureService;

class FieldTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('test_content_type');
        Schema::drop('test_content_type_new');
        Schema::drop('test_content_type_ref');
        Mockery::close();

    }

    private function createContentTypeForTesting($setAlias = true)
    {
        $info = [ 'name'        => 'Test Content Type',
                  'description' => 'Content type for testing',
        ];
        if ($setAlias) {
            $info[ 'alias' ] = 'test_content_type';
        }

        return StructureService::createContentType($info);
    }






    private function createSampleStringField($name, $description, $alias)
    {
        $stringField = new StringField();
        $stringField->setName($name);
        $stringField->setDescription($description);
        $stringField->setAlias($alias);

        return $stringField;
    }

    public function test_can_add_field_without_group()
    {
        $this->createContentTypeForTesting(false);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $this->assertEquals(true, $contentType->isFieldExist('test_field'));
    }

    public function test_adding_field_must_throw_content_type_updated_event()
    {
        $this->createContentTypeForTesting(false);
        $this->expectsEvents(\LaunchCMS\Events\ContentTypeWasUpdated::class);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
    }

    public function test_adding_field_must_throw_field_added_event()
    {
        $this->createContentTypeForTesting(false);
        $this->expectsEvents(\LaunchCMS\Events\FieldWasAdded::class);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
    }

    public function test_can_add_field_with_field_group()
    {
        $this->createContentTypeForTesting(false);

        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group 1', 'description' => 'test group description', 'position' => 1 ]);
        StructureService::addFieldGroup('test_content_type', $fieldGroup);

        //test adding field with setting group
        $field = $this->createSampleStringField('test field with group', 'test field description', 'test_field_with_group');
        $field->setFieldGroup('test_group_1');

        StructureService::addField('test_content_type', $field);
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $field = $contentType->getField('test_field_with_group');
        $this->assertEquals('test_group_1', $field->getFieldGroup());
    }

    public function test_cannot_add_field_to_content_type_with_same_alias()
    {
        $this->createContentTypeForTesting(false);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
        try {
            $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
            StructureService::addField('test_content_type', $field);

        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof \LaunchCMS\Services\Exceptions\FieldException);
            $this->assertEquals(\LaunchCMS\Services\Exceptions\FieldException::DUPLICATED_FIELD, $ex->getCode());

            return;
        }
        $this->fail('Expect to throw exception because field alias already exists');
    }

    public function test_cannot_update_field_in_content_type_with_other_field_alias()
    {

        $this->createContentTypeForTesting(false);
        $field = $this->createSampleStringField('test field 1', 'test field description 1', 'test_field_1');
        StructureService::addField('test_content_type', $field);
        $field = $this->createSampleStringField('test field 2', 'test field description 1', 'test_field_2');
        StructureService::addField('test_content_type', $field);
        $fieldInfoToUpdate = $this->createSampleStringField('test field 2', 'test field description 2', 'test_field_1');
        try {
            StructureService::updateField('test_content_type', 'test_field_2', $fieldInfoToUpdate);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof \LaunchCMS\Services\Exceptions\FieldException);
            $this->assertEquals(\LaunchCMS\Services\Exceptions\FieldException::DUPLICATED_FIELD, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_can_update_field()
    {
        $this->createContentTypeForTesting(false);

        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);

        $field = $this->createSampleStringField('test field 2', 'test field description 2', 'test_field_2');
        StructureService::updateField('test_content_type', 'test_field', $field);
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $field = $contentType->getField('test_field_2');
        $this->assertNotNull($field);
    }

    public function test_updating_field_must_throw_content_type_updated_event()
    {
        $this->createContentTypeForTesting(false);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
        $this->expectsEvents(\LaunchCMS\Events\ContentTypeWasUpdated::class);
        $field = $this->createSampleStringField('test field 2', 'test field description 2', 'test_field_2');
        StructureService::updateField('test_content_type', 'test_field', $field);
    }

    public function test_updating_field_must_throw_field_updated_event()
    {
        $this->createContentTypeForTesting(false);
        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);
        $this->expectsEvents(\LaunchCMS\Events\FieldWasUpdated::class);
        $field = $this->createSampleStringField('test field 2', 'test field description 2', 'test_field_2');
        StructureService::updateField('test_content_type', 'test_field', $field);
    }

    public function test_can_remove_field()
    {
        $this->createContentTypeForTesting(false);

        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);

        StructureService::deleteField('test_content_type', 'test_field');
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $this->assertEquals(false, $contentType->isFieldExist('test_field'));
    }

    public function test_removing_field_must_throw_content_type_updated_event()
    {
        $this->createContentTypeForTesting(false);

        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);

        $this->expectsEvents(\LaunchCMS\Events\ContentTypeWasUpdated::class);
        StructureService::deleteField('test_content_type', 'test_field');
    }

    public function test_removing_field_must_throw_field_was_deleted_event()
    {
        $this->createContentTypeForTesting(false);

        $field = $this->createSampleStringField('test field', 'test field description', 'test_field');
        StructureService::addField('test_content_type', $field);

        $this->expectsEvents(\LaunchCMS\Events\FieldWasDeleted::class);
        StructureService::deleteField('test_content_type', 'test_field');
    }

}