<?php

use LaunchCMS\Models\Content\DateField;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\MongoDB\MongoHelper;

class DateFieldTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('order');

        Mockery::close();

    }

    private function createOrderStructure()
    {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order' ]);

        $purchasedDate = new DateField();
        $purchasedDate->setName('Purchased date');
        $purchasedDate->setAlias('purchased_date');
        StructureService::addField('order', $purchasedDate);
    }

    public function test_create_content_with_date_field_invalid_data()
    {
        $this->createOrderStructure();

        $content = new Content();
        $content->name = 'test order';
        $content->purchased_date = 'wrong date';
        try {
            ContentService::saveContent('order', $content);
            $this->fail("Expected exception Invalid content not thrown");
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $errorMessage = $ex->getErrors()->first();
            $this->assertEquals(Field::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, $errorMessage);
        }

    }

    public function test_value_after_save_must_be_mongo_utc_date()
    {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->purchased_date = '2016-02-25';
        ContentService::saveContent('order', $content);
        $record = MongoHelper::getMongoDB()->collection('order')->first();
        $this->assertTrue($record[ 'purchased_date' ] instanceof MongoDB\BSON\UTCDateTime);
    }

    public function test_date_field_can_be_serialized() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->purchased_date = '2016-02-25';
        ContentService::saveContent('order', $content);
        $record = MongoHelper::getMongoDB()->collection('order')->first();
        $contentAfterSave = Content::fromArray($record);
        $gotException = false;
        $serializedValue = null;
        try {
            $serializedValue = serialize($contentAfterSave);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
        return $serializedValue;
    }

    public function test_date_field_can_be_deserialized() {
        $serializedValue = $this->test_date_field_can_be_serialized();
        $gotException = false;
        $contentAfterUnserialized = null;
        try {
            $contentAfterUnserialized = unserialize($serializedValue);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
        $this->assertTrue($contentAfterUnserialized->purchased_date instanceof \Carbon\Carbon);
        return $contentAfterUnserialized;
    }

    public function test_can_save_content_after_deserializing() {
        $contentAfterUnserialized = $this->test_date_field_can_be_deserialized();
        $gotException = false;
        try {
            ContentService::saveContent('order', $contentAfterUnserialized);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
    }
}