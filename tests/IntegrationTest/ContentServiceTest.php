<?php

use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Models\Content\DataObject\Content;
use Illuminate\Support\Facades\DB;
use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Models\Content\DateField;
use LaunchCMS\Models\Content\BooleanField;
use LaunchCMS\Models\Content\IntegerField;
use LaunchCMS\Models\Content\EmbeddedField;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Facades\UserService;
use LaunchCMS\Models\User\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use LaunchCMS\Models\Content\WorkflowSystemStatus;
use LaunchCMS\RuntimeSetting;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\MongoDB\MongoHelper;

class ContentServiceTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('cms_content_versions');
        Schema::drop('test_content_type');
        Schema::drop('test_content_type_ref');
        Schema::drop('order');
        Schema::drop('cms_users');
    }

    private function createContentTypeForTesting()
    {
        StructureService::createContentType([ 'name'        => 'Test Content Type',
                                              'description' => 'Content type for testing' ]);
    }

    private function createSampleStringField($name, $description, $alias)
    {
        $stringField = new StringField();
        $stringField->setName($name);
        $stringField->setDescription($description);
        $stringField->setAlias($alias);

        return $stringField;
    }

    protected function createOrderStructure()
    {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order' ]);

        $shippingDataType = StructureService::createDataType([ 'name'        => 'Shipping info',
                                                               'description' => 'Shipping info', 'alias' => 'shipping_info' ]);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        StructureService::addFieldToDataType('shipping_info', $addressField);

        $embeddedShippingField = new EmbeddedField($shippingDataType->_id);
        $embeddedShippingField->setName('shipping');
        $embeddedShippingField->setAlias('shipping');
        StructureService::addField('order', $embeddedShippingField);
    }

    protected function createSampleOrder($name, $address, $visibility = Content::VISIBILITY_ANYONE,
                                         $visibleUsers = [ ], $visibleGroups = [ ], $visibleRoles = [ ])
    {
        $content = new Content();
        $content->name = $name;
        $content->shipping = [ 'address' => $address ];
        $content->visibility = $visibility;
        $content->visibleUsers = $visibleUsers;
        $content->visibleGroups = $visibleGroups;
        $content->visibleRoles = $visibleRoles;

        return ContentService::saveContent('order', $content);
    }

    protected function createUserData() {
        UserService::createRole('Order manager', 'order_manager');
        UserService::createRole('Delivery manager', 'delivery_manager');
        UserService::createRole('Accounting manager', 'accounting_manager');

        $john = Sentinel::registerAndActivate([
            'email'      => 'john@email.local',
            'name'       => 'John',
            'password'   => 'john',
            'admin_user' => true,
        ]);
        $john->addRole('order_manager');
        /** @var User $marry */
        $marry = Sentinel::registerAndActivate([
            'email'      => 'marry@email.local',
            'name'       => 'Marry',
            'password'   => 'marry',
            'admin_user' => true,
        ]);
        $marry->addRole('accounting_manager');

        Sentinel::registerAndActivate([
            'email'      => 'peter@email.local',
            'name'       => 'Peter',
            'password'   => 'peter',
            'admin_user' => true,
        ]);
    }

    public function test_convert_content_from_mongo_with_date_fields()
    {
        $this->createContentTypeForTesting();
        $dateField = new DateField();
        $dateField->setName('test field');
        $dateField->setDescription('test field description');
        $dateField->setAlias('test_field');

        StructureService::addField('test_content_type', $dateField);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = '2016-02-25';
        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();

        $content = Content::fromArray($record);
        $this->assertTrue($content->createdTime instanceof \Carbon\Carbon);
        $this->assertTrue($content->lastUpdatedTime instanceof \Carbon\Carbon);
        $this->assertTrue($content->test_field instanceof \Carbon\Carbon);

    }

    public function test_create_content_with_string_field_invalid_data()
    {
        $this->createContentTypeForTesting();
        $field = $this->createSampleStringField('test field', 'Test field description', 'test_field');
        //test with passing wrong string value
        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 10;
        try {
            ContentService::saveContent('test_content_type', $content);
            $this->fail("Expected exception Invalid content not thrown");
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $errorMessage = $ex->getErrors()->first();
            $this->assertEquals(Field::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, $errorMessage);
        }
    }

    public function test_create_content_with_string_field_valid_data()
    {
        $this->createContentTypeForTesting();
        $field = $this->createSampleStringField('test field', 'Test field description', 'test_field');
        //test with passing wrong string value
        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'this is a string';
        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertEquals('this is a string', $record[ 'test_field' ]);
    }

    public function test_create_content_with_boolean_field_invalid_data()
    {
        $this->createContentTypeForTesting();
        $field = new BooleanField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');

        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 10;
        try {
            ContentService::saveContent('test_content_type', $content);
            $this->fail("Expected exception Invalid content not thrown");
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
        }
    }

    public function test_create_content_with_boolean_field_valid_data()
    {
        $this->createContentTypeForTesting();
        $field = new BooleanField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');

        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = true;
        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertEquals(true, $record[ 'test_field' ]);
    }

    public function test_create_content_with_integer_type_invalid_data()
    {
        $this->createContentTypeForTesting();
        $field = new IntegerField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'wrong type of integer';
        try {
            ContentService::saveContent('test_content_type', $content);
            $this->fail("Expected exception Invalid content not thrown");
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
        }
    }

    public function test_create_content_with_integer_type_valid_data()
    {
        $this->createContentTypeForTesting();
        $field = new IntegerField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 10;
        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertEquals(true, $record[ 'test_field' ]);
    }


    public function test_create_content_with_require_rule_exception_flow()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        $field->addValidationRule([ 'string', 'required' ]);
        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = null;

        try {
            ContentService::saveContent('test_content_type', $content);
            $this->fail('Expect to have exception of invalid content');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $this->assertEquals(ContentException::INVALID_CONTENT, $ex->getCode());
        }

    }

    public function test_create_content_with_require_rule_happy_flow()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        $field->addValidationRule([ 'string', 'required' ]);
        StructureService::addField('test_content_type', $field);

        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'test field value';

        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertEquals('test field value', $record[ 'test_field' ]);
    }

    public function test_create_content_with_email_rule_exception_flow()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        $field->addValidationRule([ 'string', 'email' ]);
        StructureService::addField('test_content_type', $field);

        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'email not valid';

        try {
            ContentService::saveContent('test_content_type', $content);
            $this->fail('Expect to have exception of invalid content');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $this->assertEquals(ContentException::INVALID_CONTENT, $ex->getCode());
        }

    }

    public function test_create_content_with_email_rule_happy_flow()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        $field->addValidationRule([ 'string', 'email' ]);
        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';

        $content->test_field = 'email@email.com';
        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertEquals('email@email.com', $record[ 'test_field' ]);
    }

    public function test_save_content_with_version()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);

        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'test field value';
        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();

        $content = Content::fromArray($record);
        $content->test_field = 'test field value 1';
        ContentService::saveContent('test_content_type', $content, true);

        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertEquals(1, $record[ 'last_version_number' ]);

        $versionRecord = MongoHelper::getMongoDB()->collection('cms_content_versions')->first();
        $versionRecordData = json_decode($versionRecord[ 'data' ], true);
        $this->assertNotNull($versionRecord);
        $this->assertEquals('test field value', $versionRecordData[ 'test_field' ]);
    }

    public function test_delete_content()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);

        //test delete content without any version
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'test field value';
        $id = ContentService::saveContent('test_content_type', $content);

        ContentService::deleteContentByID('test_content_type', $id);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertNull($record);

    }

    public function test_delete_content_with_version()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);

        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'test field value';
        ContentService::saveContent('test_content_type', $content);

        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $content = Content::fromArray($record);
        $content->test_field = 'test field value 1';
        $id = ContentService::saveContent('test_content_type', $content, true);
        ContentService::deleteContentByID('test_content_type', $id);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertNull($record);
        $versionRecord = MongoHelper::getMongoDB()->collection('cms_content_versions')->first();
        $this->assertNull($versionRecord);
    }

    public function test_move_to_trash()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);
        //test delete content without any version
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'test field value';
        $id = ContentService::saveContent('test_content_type', $content);
        ContentService::moveToTrash('test_content_type', $id);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertTrue(true, $record[ 'trashed' ]);
    }



    public function test_content_object_can_be_serialized()
    {
        $this->createOrderStructure();
        $this->createSampleOrder('test order', 'NewYork');

        $record = MongoHelper::getMongoDB()->collection('order')->first();
        $content = Content::fromArray($record);
        $gotException = false;
        try {
            serialize($content);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
    }

    public function test_content_object_can_be_deserialized()
    {
        $this->createOrderStructure();
        $this->createSampleOrder('test order', 'NewYork');

        $record = MongoHelper::getMongoDB()->collection('order')->first();
        $content = Content::fromArray($record);
        $gotException = false;
        $serializedObject = serialize($content);

        /** @var Content $contentAfterDeserialized */
        $contentAfterDeserialized = null;
        try {
            $contentAfterDeserialized = unserialize($serializedObject);
        } catch (Exception $ex) {
            $gotException = true;
        }

        $this->assertFalse($gotException);
        $this->assertEquals('test order', $contentAfterDeserialized->name);
        $this->assertTrue($contentAfterDeserialized->id instanceof \MongoDB\BSON\ObjectID);

    }

    public function test_expect_throwing_event_when_content_saved()
    {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);

        $this->expectsEvents(\LaunchCMS\Events\ContentAfterSaving::class);

        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'test field value';
        ContentService::saveContent('test_content_type', $content);
    }


    public function test_query_visible_content_return_all_for_anonymous_user()
    {
        $this->createOrderStructure();
        $this->createSampleOrder('order 1', 'New York', Content::VISIBILITY_ANYONE);
        $this->createSampleOrder('order 2', 'Ho Chi Minh', Content::VISIBILITY_ANYONE);
        $this->createSampleOrder('order 3', 'Tokyo', Content::VISIBILITY_ANYONE);
        $orderQuery = ContentService::getQueryBuilder('order');
        $result = ContentService::addVisibleCheckingToQuery($orderQuery)->get();
        $this->assertEquals(3, count($result));
    }

    public function test_query_visible_content_return_all_for_specific_user()
    {
        $this->createUserData();
        $this->createOrderStructure();
        $this->createSampleOrder('order 1', 'New York', Content::VISIBILITY_ANYONE);
        $this->createSampleOrder('order 2', 'Ho Chi Minh', Content::VISIBILITY_ANYONE);
        $this->createSampleOrder('order 3', 'Tokyo', Content::VISIBILITY_ANYONE);
        $orderQuery = ContentService::getQueryBuilder('order');
        $peter = UserService::getUserByEmail('peter@email.local');
        $result = ContentService::addVisibleCheckingToQuery($orderQuery, $peter)->get();
        $this->assertEquals(3, count($result));
    }

    public function test_query_visible_content_return_data_for_user_specific_permission()
    {
        $this->createUserData();
        $this->createOrderStructure();
        $peter = UserService::getUserByEmail('peter@email.local');
        $john = UserService::getUserByEmail('john@email.local');
        $mary = UserService::getUserByEmail('marry@email.local');

        $this->createSampleOrder('order 1', 'New York', Content::VISIBILITY_SPECIFIC_USERS, [$peter->_id]);
        $this->createSampleOrder('order 2', 'Ho Chi Minh', Content::VISIBILITY_SPECIFIC_USERS, [$john->_id]);
        $this->createSampleOrder('order 3', 'Tokyo', Content::VISIBILITY_SPECIFIC_USERS, [$mary->_id]);
        $orderQuery = ContentService::getQueryBuilder('order');

        $result = ContentService::addVisibleCheckingToQuery($orderQuery, $peter)->get();
        $this->assertEquals(1, count($result));
        $this->assertEquals('order 1', $result[0]['name']);
    }

    public function test_query_visible_content_return_data_for_role_specific_permission()
    {
        $this->createUserData();
        $this->createOrderStructure();
        /** @var User $mary */
        $mary = UserService::getUserByEmail('marry@email.local');

        $accountingRole = UserService::getRoleBySlug('accounting_manager');
        $orderManagerRole = UserService::getRoleBySlug('order_manager');

        $this->createSampleOrder('order 1', 'New York', Content::VISIBILITY_SPECIFIC_ROLES, [], [], [$accountingRole->_id]);
        $this->createSampleOrder('order 2', 'Ho Chi Minh', Content::VISIBILITY_SPECIFIC_ROLES,[], [], [$orderManagerRole->_id]);
        $this->createSampleOrder('order 3', 'Tokyo', Content::VISIBILITY_SPECIFIC_USERS, [$mary->_id]);
        $orderQuery = ContentService::getQueryBuilder('order');

        $result = ContentService::addVisibleCheckingToQuery($orderQuery, $mary)->get();
        //Marry must see 2 orders: order 1 as she is accounting manager role and order 3 as she is assigned specific to this
        $this->assertEquals(2, count($result));
    }

    public function test_create_draft_content_not_throw_any_exception() {
        $this->createOrderStructure();
        ContentService::createDraftContent('order', 'order of Bill', null);
        $record = MongoHelper::getMongoDB()->collection('order')->first();
        $this->assertEquals(WorkflowSystemStatus::DRAFT, $record['workflow_status']);
    }


    public function test_can_create_content_with_dev_fields() {
        RuntimeSetting::$runningMode = RuntimeSetting::INSTALLATION_MODE;
        StructureService::createContentType([
            'name' => 'Users',
            'description' => 'The built-in structure of user',
            'alias' => 'cms_users',
            'dev_settings' => [
                'builtin' => true,
                'dev_fields' => ['password', 'email', 'last_login']
            ]
        ]);

        $content = new Content();
        $content->name = 'Test user';
        $content->email = 'test@email.com';
        $content->password = 'password';
        ContentService::saveContent('cms_users', $content);
        $contentRow = MongoHelper::getMongoDB()->collection('cms_users')->first();
        $this->assertNotNull($contentRow);
        $this->assertEquals('test@email.com', $contentRow['email']);
    }

    public function test_can_not_create_content_with_not_found_field() {
        RuntimeSetting::$runningMode = RuntimeSetting::INSTALLATION_MODE;
        StructureService::createContentType([
            'name' => 'Users',
            'description' => 'The built-in structure of user',
            'alias' => 'cms_users',
            'dev_settings' => [
                'builtin' => true,
                'dev_fields' => ['password', 'email', 'last_login']
            ]
        ]);

        $content = new Content();
        $content->name = 'Test user';
        $content->not_found_field = 'test@email.com';

        try {
            ContentService::saveContent('cms_users', $content);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof FieldException);
            $this->assertEquals(FieldException::FIELD_NOT_FOUND, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');

    }

    public function test_change_content_state_with_success_case() {
        $this->createContentTypeForTesting();
        $field = new StringField();
        $field->setName('test field');
        $field->setDescription('Test field description');
        $field->setAlias('test_field');
        StructureService::addField('test_content_type', $field);
        //test delete content without any version
        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'test field value';
        $id = ContentService::saveContent('test_content_type', $content);
        ContentService::changeContentWorkflowStatus('test_content_type', $id, 'draft');
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertEquals('draft', $record['workflow_status']);
    }
}