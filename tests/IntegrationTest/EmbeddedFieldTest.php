<?php


use LaunchCMS\Models\Content\EmbeddedField;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Exceptions\DataTypeException;
use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Models\Content\EmbeddedListField;
use LaunchCMS\MongoDB\MongoHelper;

class EmbeddedFieldTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('order');

        Mockery::close();

    }
    private function createOrderStructure() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);

        $shippingDataType = StructureService::createDataType([ 'name'        => 'Shipping info',
                                                               'description' => 'Shipping info', 'alias' => 'shipping_info']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        StructureService::addFieldToDataType('shipping_info', $addressField);

        $embeddedShippingField = new EmbeddedField($shippingDataType->_id);
        $embeddedShippingField->setName('shipping');
        $embeddedShippingField->setAlias('shipping');
        StructureService::addField('order', $embeddedShippingField);
    }

    public function test_can_see_index_after_creating_embedded_field_with_data_type_include_indexed_field() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $orderCodeDataType = StructureService::createDataType([ 'name'        => 'Order Code',
                                                                'alias' => 'order_code']);
        $codeField = new StringField();
        $codeField->setName('Code');
        $codeField->setAlias('code');
        $codeField->setIndex(true);
        StructureService::addFieldToDataType('order_code', $codeField);

        $embeddedCodeField = new EmbeddedField($orderCodeDataType->_id);
        $embeddedCodeField->setName('Order Code');
        $embeddedCodeField->setAlias('order_code');
        StructureService::addField('order', $embeddedCodeField);

        $index = $this->getIndex('order', 'order_code.code');

        $this->assertEquals(1, $index['key']['order_code.code']);
    }

    public function test_can_see_index_after_creating_embedded_list_field_with_data_type_include_indexed_field() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $orderCodeDataType = StructureService::createDataType([ 'name'        => 'Order Code',
                                                                'alias' => 'order_code']);
        $codeField = new StringField();
        $codeField->setName('Code');
        $codeField->setAlias('code');
        $codeField->setIndex(true);
        StructureService::addFieldToDataType('order_code', $codeField);

        $embeddedCodeField = new EmbeddedListField($orderCodeDataType->_id);
        $embeddedCodeField->setName('Order Code');
        $embeddedCodeField->setAlias('order_code');
        StructureService::addField('order', $embeddedCodeField);

        $index = $this->getIndex('order', 'order_code.code');

        $this->assertEquals(1, $index['key']['order_code.code']);
    }

    public function test_can_see_index_after_adding_new_indexed_field_to_data_type_in_all_related_content_types() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $orderCodeDataType = StructureService::createDataType([ 'name'        => 'Order Code',
                                                                'alias' => 'order_code']);
        $embeddedCodeField = new EmbeddedField($orderCodeDataType->_id);
        $embeddedCodeField->setName('Order Code');
        $embeddedCodeField->setAlias('order_code');
        StructureService::addField('order', $embeddedCodeField);

        $codeField = new StringField();
        $codeField->setName('Code');
        $codeField->setAlias('code');
        $codeField->setIndex(true);
        StructureService::addFieldToDataType('order_code', $codeField);
        $index = $this->getIndex('order', 'order_code.code');
        $this->assertEquals(1, $index['key']['order_code.code']);
    }

    public function test_can_see_index_after_updating_existing_field_index_attribute_of_data_type_in_all_related_content_types() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $orderCodeDataType = StructureService::createDataType([ 'name'        => 'Order Code',
                                                                'alias' => 'order_code']);
        $embeddedCodeField = new EmbeddedField($orderCodeDataType->_id);
        $embeddedCodeField->setName('Order Code');
        $embeddedCodeField->setAlias('order_code');
        StructureService::addField('order', $embeddedCodeField);

        $codeField = new StringField();
        $codeField->setName('Code');
        $codeField->setAlias('code');
        StructureService::addFieldToDataType('order_code', $codeField);
        $codeField->setIndex(true);
        StructureService::updateFieldOfDataType('order_code', 'code', $codeField);
        $index = $this->getIndex('order', 'order_code.code');
        $this->assertEquals(1, $index['key']['order_code.code']);
    }

    public function test_cannot_create_embedded_field_with_invalid_data_type() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $embeddedShippingField = new EmbeddedField('invalid_data_type_id');
        $embeddedShippingField->setName('shipping');
        $embeddedShippingField->setAlias('shipping');
        try {
            StructureService::addField('order', $embeddedShippingField);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof FieldException);
            $this->assertEquals(FieldException::INVALID_REFERENCE_DATA_TYPE, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');

    }

    public function test_cannot_create_embedded_field_of_data_type()
    {
        /** @var \LaunchCMS\Models\Content\DataType $dataType */
        $dataType = StructureService::createDataType([ 'name' => 'Order detail', 'alias' => 'order_detail', 'description' => 'Order detail line' ]);
        $embeddedField = new EmbeddedField($dataType->_id);
        $embeddedField->setName('Embedded field test');
        $embeddedField->setAlias('test');
        try {
            StructureService::addFieldToDataType('order_detail', $embeddedField);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof DataTypeException);
            $this->assertEquals(DataTypeException::FIELD_TYPE_CANNOT_BE_USED_FOR_DATA_TYPE, $ex->getCode());

            return;
        }
        $this->fail('Expect to have invalid field type exception');
    }


    public function test_can_create_content_with_embedded_field() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->shipping = ['address' => 'NewYork city'];
        ContentService::saveContent('order', $content);

        $record = MongoHelper::getMongoDB()->collection('order')->first();
        $this->assertNotNull($record);
    }

    public function test_field_must_be_removed_in_content_after_deleting_field_in_data_type() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->shipping = ['address' => 'NewYork city'];
        ContentService::saveContent('order', $content);
        StructureService::deleteFieldOfDataType('shipping_info', 'address');
        $record = MongoHelper::getMongoDB()->collection('order')->first();

        $this->assertFalse(isset($record['shipping']['address']));
    }

    public function test_cannot_save_embedded_value_with_wrong_field() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->shipping = ['unknown_field' => 'NewYork city'];
        try {
            ContentService::saveContent('order', $content);
        } catch(Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $this->assertEquals(ContentException::INVALID_CONTENT, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_save_embedded_value_with_wrong_value_type() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->shipping = ['address' => 1];
        try {
            ContentService::saveContent('order', $content);
        } catch(Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $this->assertEquals(ContentException::INVALID_CONTENT, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

}