<?php


use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Models\Content\ArrayField;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\MongoDB\MongoHelper;
class ArrayFieldTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('article');

        Mockery::close();

    }

    public function test_cannot_create_array_field_with_invalid_element_type() {
        StructureService::createContentType([ 'name'  => 'Article',
                                              'alias' => 'article' ]);
        $tagFields = new ArrayField('invalid_element_type');
        $tagFields->setName('Tags');
        $tagFields->setAlias('tags');
        try {
            StructureService::addField('article', $tagFields);
            $this->fail("Expected exception has not been thrown");
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof FieldException);
            $this->assertEquals(FieldException::INVALID_ARRAY_ELEMENT_TYPE, $ex->getCode());
        }
    }

    public function test_cannot_create_content_with_string_array_field_invalid_data()
    {
        StructureService::createContentType([ 'name'  => 'Article',
                                              'alias' => 'article' ]);
        $tagFields = new ArrayField(Field::STRING);
        $tagFields->setName('Tags');
        $tagFields->setAlias('tags');
        StructureService::addField('article', $tagFields);

        $content = new Content();
        $content->name = 'test order';
        $content->tags = [ 12 ];
        try {
            ContentService::saveContent('article', $content);
            $this->fail("Expected exception Invalid content not thrown");
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $errorMessage = $ex->getErrors()->first();
            $this->assertEquals(Field::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, $errorMessage);
        }

    }

    public function test_can_create_content_with_string_array_field_valid_data()
    {
        StructureService::createContentType([ 'name'  => 'Article',
                                              'alias' => 'article' ]);
        $tagFields = new ArrayField(Field::STRING);
        $tagFields->setName('Tags');
        $tagFields->setAlias('tags');
        StructureService::addField('article', $tagFields);

        $content = new Content();
        $content->name = 'test order';
        $content->tags = [ 'good', 'cool' ];
        ContentService::saveContent('article', $content);
        $record = MongoHelper::getMongoDB()->collection('article')->first();
        $this->assertNotNull($record);
        $this->assertEquals(2, count($record['tags']));

    }

}