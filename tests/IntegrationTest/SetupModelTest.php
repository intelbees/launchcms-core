<?php
use LaunchCMS\Services\Facades\InstallationService;
use Illuminate\Support\Facades\Schema as Schema;
use LaunchCMS\Models\User\User;
use LaunchCMS\Models\Content\ContentType;

class SetupModelTest extends TestCase
{
    public function tearDown()
    {
        User::truncate();
        ContentType::truncate();
        Schema::drop('cms_users');
        Schema::drop('cms_roles');
        Schema::drop('cms_content_types');
        Schema::drop('cms_content_versions');
    }

    public function test_setup_model()
    {
        InstallationService::install();
        $this->assertTrue(Schema::hasCollection('cms_users'));
        $this->assertTrue(Schema::hasCollection('cms_content_types'));
    }

}