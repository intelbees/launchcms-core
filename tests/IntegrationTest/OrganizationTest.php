<?php

use LaunchCMS\Services\Facades\OrganizationService;
use LaunchCMS\Services\Exceptions\OrganizationException;
class OrganziationTest extends TestCase
{
    public function tearDown ()
    {
        Schema::drop('cms_sites');
        Schema::drop('cms_groups');
    }

    public function test_cannot_create_site_with_invalid_alias() {

        try {
            OrganizationService::createSite(['alias' => '', 'name' => 'first website', 'description' => 'My beautiful website']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::INVALID_SITE_ALIAS, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_create_site_with_invalid_alias_naming() {

        try {
            OrganizationService::createSite(['alias' => '1 @#', 'name' => 'first website', 'description' => 'My beautiful website']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::INVALID_SITE_ALIAS, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_create_site_without_name() {

        try {
            OrganizationService::createSite(['alias' => 'my_site', 'description' => 'My beautiful website']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::INVALID_SITE_NAME, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_can_create_site_when_all_info_valid() {
        $site = OrganizationService::createSite(['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);
        $this->assertNotEmpty($site);
    }

    public function test_cannot_create_site_with_exist_alias() {
        OrganizationService::createSite(['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);

        try {
            OrganizationService::createSite(['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::DUPLICATED_SITE_ALIAS, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_update_site_without_passing_alias() {
        try {
            OrganizationService::updateSite(null, ['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_throw_exception_if_updating_not_found_site() {
        try {
            OrganizationService::updateSite('a_not_found_site_alias', ['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::SITE_NOT_FOUND, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_update_site_with_duplicated_alias() {
        OrganizationService::createSite(['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);
        OrganizationService::createSite(['alias' => 'another_site', 'name'=> 'My another website', 'description' => 'My beautiful website']);
        try {
            OrganizationService::updateSite('another_site', ['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::DUPLICATED_SITE_ALIAS, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_throw_exception_if_delete_site_by_id_not_found() {
        try {
            OrganizationService::deleteSiteByID('a_not_found_id');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::SITE_NOT_FOUND, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_throw_exception_if_delete_site_by_alias_not_found() {
        try {
            OrganizationService::deleteSiteByAlias('a_not_found_alias');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::SITE_NOT_FOUND, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }


    public function test_cannot_create_group_with_invalid_alias() {

        try {
            OrganizationService::createGroup(['alias' => '', 'name' => 'first group', 'description' => 'My community group']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::INVALID_GROUP_ALIAS, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_create_group_with_invalid_alias_naming() {

        try {
            OrganizationService::createGroup(['alias' => '1 @#', 'name' => 'first group', 'description' => 'My community group']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::INVALID_GROUP_ALIAS, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_create_group_without_name() {

        try {
            OrganizationService::createGroup(['alias' => 'my_Group', 'description' => 'My cool group']);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof OrganizationException);
            $this->assertEquals(OrganizationException::INVALID_GROUP_NAME, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_can_create_group_when_all_info_valid() {
        $group = OrganizationService::createGroup(['alias' => 'my_group', 'name'=> 'My group', 'description' => 'My cool group']);
        $this->assertNotEmpty($group);
    }

    public function test_can_create_group_with_site_association() {
        $site = OrganizationService::createSite(['alias' => 'my_site', 'name'=> 'My website', 'description' => 'My beautiful website']);
        $group = OrganizationService::createGroup(['alias' => 'my_group', 'name'=> 'My group',
                                                   'description' => 'My cool group',
                                                'site_id' => $site->_id]);
        $this->assertNotNull($group);
        $reloadGroup = OrganizationService::getGroupByID($group->_id);
        $associatedSite = $reloadGroup->site;
        $this->assertNotNull($associatedSite);
        $this->assertEquals($associatedSite->_id,$site->_id);
    }

}