<?php

use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\ReferenceField;
use LaunchCMS\MongoDB\MongoHelper;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;

class ReferenceFieldTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('test_content_type');
        Schema::drop('test_content_type_new');
        Schema::drop('test_content_type_ref');
        Schema::drop('order');
        Mockery::close();

    }

    public function test_cannot_create_reference_field_if_content_type_invalid() {
        StructureService::createContentType([ 'name'        => 'Test Content Type',
                                              'description' => 'Content type for testing']);

        $field = new ReferenceField('a_not_found_id');
        $field->setAlias('test_field');
        $field->setName('Test field');
        try {
            StructureService::addField('test_content_type', $field);
        } catch(Exception $ex) {
            $this->assertTrue($ex instanceof FieldException);
            $this->assertEquals(FieldException::INVALID_REFERENCE_CONTENT_TYPE, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_create_content_with_reference_field() {
        StructureService::createContentType([ 'name'        => 'Test Content Type',
                                              'description' => 'Content type for testing']);

        $refContentType = StructureService::createContentType([ 'name'        => 'Test Content Type For Reference',
                                                                'description' => 'Content type for testing',
                                                                'alias'       => 'test_content_type_ref']);


        $refContent = new Content();
        $refContent->name = 'Ref content';
        $refContentID = ContentService::saveContent('test_content_type_ref', $refContent);

        $field = new ReferenceField($refContentType->_id);
        $field->setAlias('test_field');
        $field->setName('Test field');
        StructureService::addField('test_content_type', $field);
        $content = new Content();
        $content->name = 'test content';

        $content->test_field = $refContentID;
        ContentService::saveContent('test_content_type', $content);
        $record = MongoHelper::getMongoDB()->collection('test_content_type')->first();
        $this->assertTrue($record['test_field'] === $refContentID);

    }

    public function test_cannot_create_content_with_invalid_ref_content_value() {
        StructureService::createContentType([ 'name'        => 'Test Content Type',
                                              'description' => 'Content type for testing']);

        $refContentType = StructureService::createContentType([ 'name'        => 'Test Content Type For Reference',
                                                                'description' => 'Content type for testing',
                                                                'alias'       => 'test_content_type_ref']);
        $field = new ReferenceField($refContentType->_id);
        $field->setAlias('test_field');
        $field->setName('Test field');
        StructureService::addField('test_content_type', $field);

        $content = new Content();
        $content->name = 'test content';
        $content->test_field = 'a not exist content id';
        try {
            ContentService::saveContent('test_content_type', $content);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            return;
        }
        $this->fail('Expected exception has not been thrown');

    }
}