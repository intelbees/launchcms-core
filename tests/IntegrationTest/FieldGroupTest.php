<?php

use LaunchCMS\Models\Content\FieldGroup;
use LaunchCMS\Services\Exceptions\FieldGroupException;
use LaunchCMS\Services\Facades\StructureService;

class FieldGroupTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('test_content_type');
        Schema::drop('test_content_type_new');
        Schema::drop('test_content_type_ref');
        Mockery::close();

    }

    private function createContentTypeForTesting($setAlias = true)
    {
        $info = [ 'name'        => 'Test Content Type',
                  'description' => 'Content type for testing',
        ];
        if ($setAlias) {
            $info[ 'alias' ] = 'test_content_type';
        }

        return StructureService::createContentType($info);
    }



    public function test_can_create_field_group()
    {
        $this->createContentTypeForTesting(false);
        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group', 'alias' => 'test_group', 'description' => 'test group description' ]);
        StructureService::addFieldGroup('test_content_type', $fieldGroup);

        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $this->assertEquals(1, $contentType->fieldGroups()->count());

    }

    public function test_cannot_create_duplicated_field_group()
    {
        $this->createContentTypeForTesting(false);
        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group', 'alias' => 'test_group', 'description' => 'test group description' ]);
        StructureService::addFieldGroup('test_content_type', $fieldGroup);
        try {
            StructureService::addFieldGroup('test_content_type', $fieldGroup);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof FieldGroupException);
            $this->assertEquals(FieldGroupException::DUPLICATED_FIELD_GROUP, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');


    }

    public function test_update_field_group()
    {
        $this->createContentTypeForTesting(false);
        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group', 'alias' => 'test_group', 'description' => 'test group description' ]);
        StructureService::addFieldGroup('test_content_type', $fieldGroup);

        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group 2', 'alias' => 'test_group_2', 'description' => 'test group description 2' ]);
        StructureService::updateFieldGroup('test_content_type', 'test_group', $fieldGroup);

        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $this->assertEquals(1, $contentType->fieldGroups()->count());
        $fieldGroup = $contentType->getFieldGroupByAlias('test_group_2');
        $this->assertNotNull($fieldGroup);

        $this->assertEquals('test group 2', $fieldGroup->getName());
        $this->assertEquals('test group description 2', $fieldGroup->getDescription());

    }


    public function test_field_group_must_be_sorted_ascending_by_position()
    {
        $this->createContentTypeForTesting(false);
        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group 2', 'alias' => 'test_group_2', 'description' => 'test group description', 'position' => 2 ]);
        StructureService::addFieldGroup('test_content_type', $fieldGroup);

        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group 1', 'alias' => 'test_group_1', 'description' => 'test group description', 'position' => 1 ]);
        StructureService::addFieldGroup('test_content_type', $fieldGroup);

        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        /** @var FieldGroup $firstFieldGroup */
        $firstFieldGroup = $contentType->getFieldGroups()[ 0 ];
        $this->assertEquals(1, $firstFieldGroup->getPosition());

    }

    public function test_can_delete_field_group_by_alias()
    {
        $this->createContentTypeForTesting(false);

        $fieldGroup = FieldGroup::fromArray([ 'name' => 'test group 1', 'alias' => 'test_group_1', 'description' => 'test group description', 'position' => 1 ]);
        StructureService::addFieldGroup('test_content_type', $fieldGroup);

        StructureService::deleteFieldGroup('test_content_type', 'test_group_1');
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $this->assertEquals(0, count($contentType->getFieldGroups()));

    }

}