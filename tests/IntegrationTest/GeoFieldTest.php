<?php

use LaunchCMS\Models\Content\Field;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Models\Content\GeoField;
use LaunchCMS\MongoDB\MongoHelper;
class GeoFieldTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('poi');

        Mockery::close();

    }

    private function createTestDataStructure()
    {
        StructureService::createContentType([ 'name'        => 'Point of interest',
                                              'alias' => 'poi' ]);

        $geoField = new GeoField();
        $geoField->setName('Map location');
        $geoField->setAlias('geo_point');
        StructureService::addField('poi', $geoField);
    }

    public function test_can_create_content_with_geo_field_valid_data()
    {
        $this->createTestDataStructure();
        $content = new Content();
        $content->name = 'Cool restaurant in district 4';
        $content->geo_point = GeoField::createLocationData(10.767103, 106.706762);
        ContentService::saveContent('poi', $content);
        $record = MongoHelper::getMongoDB()->collection('poi')->first();
        $this->assertNotNull($record);

    }

    public function test_cannot_create_content_with_geo_field_invalid_data()
    {
        $this->createTestDataStructure();
        $content = new Content();
        $content->name = 'Cool restaurant in district 4';
        $content->geo_point = ['not right value', 'not right value'];

        try {
            ContentService::saveContent('poi', $content);
            $this->fail("Expected exception Invalid content not thrown");
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $errorMessage = $ex->getErrors()->first();
            $this->assertEquals(Field::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, $errorMessage);
        }
    }

    public function test_can_do_geo_search_in_mongo_on_geo_field() {
        $this->createTestDataStructure();
        $content = new Content();
        $content->name = 'Cool restaurant in district 4';
        $content->geo_point = GeoField::createLocationData(10.767103, 106.706762);
        ContentService::saveContent('poi', $content);

        $content = new Content();
        $content->name = 'Cool hotel in district 2';
        $content->geo_point = GeoField::createLocationData(10.819875, 106.633837);
        ContentService::saveContent('poi', $content);

        $content = new Content();
        $content->name = 'Cool sushi restaurant in district 1';
        $content->geo_point = GeoField::createLocationData(10.825903, 106.637056);
        ContentService::saveContent('poi', $content);

        /** @var \Jenssegers\Mongodb\Query\Builder $queryBuilder */
        $queryBuilder = ContentService::getQueryBuilder('poi');

        $result = $queryBuilder->where('geo_point', 'near', [
            '$geometry' =>  [
                'type' => 'Point',
                'coordinates' => [floatval(106.637056), floatval(10.825903)]
            ],
            '$maxDistance' => 10000,
        ])->get();

        
        $this->assertTrue(count($result) > 0);
    }

}