<?php


use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Models\Content\EmbeddedListField;
USE LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\MongoDB\MongoHelper;
class EmbeddedFieldListTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('order');

        Mockery::close();

    }
    private function createOrderStructure() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);

        $shippingDataType = StructureService::createDataType([ 'name'        => 'Shipping info',
                                                               'description' => 'Shipping info', 'alias' => 'shipping_info']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        StructureService::addFieldToDataType('shipping_info', $addressField);

        $embeddedShippingField = new EmbeddedListField($shippingDataType->_id);
        $embeddedShippingField->setName('Shipping Addresses');
        $embeddedShippingField->setAlias('shipping_addresses');
        StructureService::addField('order', $embeddedShippingField);
    }

    public function test_can_create_embedded_field_list_value() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->shipping_addresses = [
            ['address' => 'NewYork city'],
            ['address' => 'Ho Chi Minh city']
        ];
        ContentService::saveContent('order', $content);

        $record = MongoHelper::getMongoDB()->collection('order')->first();
        $this->assertNotNull($record);
        $content = Content::fromArray($record);
        $this->assertTrue(is_array($content->shipping_addresses));
        $this->assertTrue(count($content->shipping_addresses) === 2);
        $firstShippingAddress = $content->shipping_addresses[0];
        $this->assertTrue(array_key_exists('_id', $firstShippingAddress));
    }

    public function test_embedded_field_list_value_invalid() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->shipping_addresses = [
            ['wrong_field' => 'NewYork city'],
            ['address' => 'Ho Chi Minh city']
        ];
        try {
            ContentService::saveContent('order', $content);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof \LaunchCMS\Services\Exceptions\ContentException);
            $this->assertEquals(\LaunchCMS\Services\Exceptions\ContentException::INVALID_CONTENT, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');

    }

    public function test_cannot_save_embedded_list_value_with_wrong_value_type() {
        $this->createOrderStructure();
        $content = new Content();
        $content->name = 'test order';
        $content->shipping_addresses = [
            ['wrong_field' => 1],
            ['address' => 'Ho Chi Minh city']
        ];
        try {
            ContentService::saveContent('order', $content);
        } catch(Exception $ex) {
            $this->assertTrue($ex instanceof ContentException);
            $this->assertEquals(ContentException::INVALID_CONTENT, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

}