<?php

use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Services\Exceptions\ContentTypeException;
use LaunchCMS\Services\Facades\StructureService;
use Illuminate\Support\Facades\Cache;
use LaunchCMS\RuntimeSetting;
use LaunchCMS\MongoDB\MongoHelper;
class ContentTypeTest extends TestCase
{
    public function tearDown()
    {
        RuntimeSetting::$runningMode = RuntimeSetting::LIVE;
        Cache::flush();
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('test_content_type');
        Schema::drop('test_content_type_new');
        Schema::drop('test_content_type_ref');
        Schema::drop('cms_users');
        Schema::drop('order');

        Mockery::close();

    }


    private function createContentTypeForTesting($setAlias = true)
    {
        $info = [ 'name'        => 'Test Content Type',
                  'description' => 'Content type for testing',
        ];
        if ($setAlias) {
            $info[ 'alias' ] = 'test_content_type';
        }

        return StructureService::createContentType($info);
    }



    public function test_create_content_type()
    {
        $this->createContentTypeForTesting();

        $this->assertTrue(Schema::hasCollection('test_content_type'));
        $contentType = ContentType::where('name', 'Test Content Type')->first();

        $this->assertNotNull($contentType);
        $inheritPath = $contentType->_id;
        $this->assertEquals($inheritPath, $contentType->getInheritPath());
        $this->assertEquals("Test Content Type", $contentType->name);
    }

    public function test_create_content_type_must_throw_event()
    {
        $this->expectsEvents(\LaunchCMS\Events\ContentTypeWasCreated::class);
        $this->createContentTypeForTesting();
    }

    public function test_cannot_create_content_type_with_invalid_variable_naming_alias()
    {
        $info = [ 'name'        => 'Test Content Type',
                  'description' => 'Content type for testing',
                  'alias'       => '1@#Invalid alias',
        ];
        try {
            StructureService::createContentType($info);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentTypeException);
            $this->assertEquals(ContentTypeException::CONTENT_TYPE_ALIAS_INVALID, $ex->getCode());

            return;
        }
        $this->fail('Expect to have invalid content type alias exception');
    }

    public function test_cannot_create_content_type_with_builtin_collection_naming_alias()
    {
        //test with name duplicated with built-in collection
        $info = [ 'name'        => 'Test Content Type',
                  'description' => 'Content type for testing',
                  'alias'       => 'cms_content_types',
        ];
        try {
            StructureService::createContentType($info);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentTypeException);
            $this->assertEquals(ContentTypeException::CONTENT_TYPE_ALIAS_INVALID, $ex->getCode());
            return;
        }
        $this->fail('Expect to have invalid content type alias exception');
    }

    public function test_can_create_content_type_without_passing_alias()
    {
        $this->createContentTypeForTesting(false);

        $this->assertTrue(Schema::hasCollection('test_content_type'));
        $contentType = ContentType::where('name', 'Test Content Type')->first();
        $this->assertNotNull($contentType);
        $this->assertEquals("test_content_type", $contentType->alias);
    }

    public function test_can_create_content_type_with_inheritance()
    {
        $parentContentType = $this->createContentTypeForTesting(false);
        $info = [ 'name'        => 'Test Inherit Content Type',
                  'alias'       => 'test_inherit',
                  'inherit'     => (string) $parentContentType->_id,
                  'description' => 'Content type for testing',
        ];

        StructureService::createContentType($info);
        $contentType = StructureService::getContentTypeByAlias('test_inherit');
        $inheritPath = sprintf("%s,%s", $parentContentType->_id, $contentType->_id);
        $this->assertEquals($inheritPath, $contentType->getInheritPath());
        $this->assertFalse(Schema::hasCollection('test_inherit'));

    }

    public function test_update_content_type_info()
    {
        $this->createContentTypeForTesting(false);
        //test update content type without changing alias (collection name)
        $info = [ 'alias' => 'test_content_type', 'description' => 'new description', 'name' => 'new name' ];
        StructureService::updateContentTypeInfo('test_content_type', $info);
        $contentType = StructureService::getContentTypeByAlias('test_content_type');
        $this->assertTrue($contentType->getDescription() === 'new description');
        $this->assertTrue($contentType->getName() === 'new name');
    }

    public function test_update_content_type_must_throw_event()
    {
        $this->createContentTypeForTesting(false);

        $this->expectsEvents(\LaunchCMS\Events\ContentTypeWasUpdated::class);
        //test update content type without changing alias (collection name)
        $info = [ 'alias' => 'test_content_type', 'description' => 'new description', 'name' => 'new name' ];
        StructureService::updateContentTypeInfo('test_content_type', $info);
    }

    public function test_cannot_update_content_type_with_invalid_alias_naming()
    {
        $this->createContentTypeForTesting(false);
        $info = [ 'alias' => '123!@', 'description' => 'new description', 'name' => 'new name' ];
        try {
            StructureService::updateContentTypeInfo('test_content_type', $info);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentTypeException);
            $this->assertEquals(ContentTypeException::CONTENT_TYPE_ALIAS_INVALID, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_cannot_update_content_type_if_alias_in_builtin_collection()
    {
        $this->createContentTypeForTesting(false);
        $info = [ 'alias' => 'cms_content_types', 'description' => 'new description', 'name' => 'new name' ];
        try {
            StructureService::updateContentTypeInfo('test_content_type', $info);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentTypeException);
            $this->assertEquals(ContentTypeException::CONTENT_TYPE_ALIAS_INVALID, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_update_content_type_info_change_alias_of_root_content_type()
    {
        $this->createContentTypeForTesting(false);
        $info = [ 'alias' => 'test_content_type_new', 'description' => 'new description', 'name' => 'new name' ];
        StructureService::updateContentTypeInfo('test_content_type', $info);
        $contentType = StructureService::getContentTypeByAlias('test_content_type_new');
        $this->assertTrue($contentType->getDescription() === 'new description');
        $this->assertTrue($contentType->getName() === 'new name');
        $this->assertFalse(Schema::hasCollection('test_content_type'));
        $this->assertTrue(Schema::hasCollection('test_content_type_new'));
    }

    public function test_update_content_type_info_change_alias_of_inherit_content_type()
    {
        $parentContentType = $this->createContentTypeForTesting(false);
        $info = [ 'name'        => 'Test Inherit Content Type',
                  'alias'       => 'test_inherit',
                  'inherit'     => (string) $parentContentType->_id,
                  'description' => 'Content type for testing',
        ];

        StructureService::createContentType($info);

        $info = [ 'alias' => 'test_content_type_new', 'description' => 'new description', 'name' => 'new name' ];
        StructureService::updateContentTypeInfo('test_inherit', $info);

        $contentType = StructureService::getContentTypeByAlias('test_content_type_new');
        $this->assertTrue($contentType->getDescription() === 'new description');
        $this->assertTrue($contentType->getName() === 'new name');
        $this->assertFalse(Schema::hasCollection('test_content_type_new'));
    }

    public function test_update_content_type_with_empty_alias()
    {
        $this->createContentTypeForTesting(false);
        $info = [ 'alias' => '', 'description' => 'new description', 'name' => 'new name' ];
        try {
            StructureService::updateContentTypeInfo('test_content_type', $info);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentTypeException);
            $this->assertEquals(ContentTypeException::CONTENT_TYPE_EMPTY_ALIAS, $ex->getCode());

            return;
        }
        $this->fail('Expect to have empty content type alias exception');
    }

    public function test_update_content_type_with_empty_name()
    {
        $this->createContentTypeForTesting(false);
        $info = [ 'alias' => 'new_alias', 'description' => 'new description', 'name' => '' ];
        try {
            StructureService::updateContentTypeInfo('test_content_type', $info);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentTypeException);
            $this->assertEquals(ContentTypeException::CONTENT_TYPE_WITH_EMPTY_NAME, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_can_delete_content_type_by_id()
    {
        /** @var ContentType $contentType */
        $contentType = $this->createContentTypeForTesting(false);
        $contentAlias = $contentType->getAlias();
        StructureService::deleteContentTypeByID($contentType->_id);
        $contentType = StructureService::getContentTypeByAlias($contentAlias);
        $this->assertTrue(empty( $contentType ));
        $this->assertFalse(Schema::hasCollection($contentAlias));
    }

    public function test_delete_content_type_by_id_must_throw_event()
    {
        $contentType = $this->createContentTypeForTesting(false);
        $this->expectsEvents(\LaunchCMS\Events\ContentTypeWasDeleted::class);
        StructureService::deleteContentTypeByID($contentType->_id);
    }

    public function test_can_delete_content_type_by_alias()
    {
        $contentType = $this->createContentTypeForTesting(false);
        $contentAlias = $contentType->getAlias();
        StructureService::deleteContentTypeByAlias($contentAlias);
        $contentType = StructureService::getContentTypeByAlias($contentAlias);
        $this->assertTrue(empty( $contentType ));
        $this->assertFalse(Schema::hasCollection($contentAlias));
    }

    public function test_delete_content_type_by_alias_must_throw_event()
    {
        $contentType = $this->createContentTypeForTesting(false);
        $this->expectsEvents(\LaunchCMS\Events\ContentTypeWasDeleted::class);
        $contentAlias = $contentType->getAlias();
        StructureService::deleteContentTypeByAlias($contentAlias);
    }

    public function test_content_type_can_be_serialized() {
        $contentType = $this->createContentTypeForTesting(false);
        $gotException = false;
        $dataAfterSerialized = null;
        try {
            $dataAfterSerialized = serialize($contentType);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
        return $dataAfterSerialized;
    }

    public function test_content_type_can_be_deserialized() {
        $dataAfterSerialized = $this->test_content_type_can_be_serialized();
        $gotException = false;
        /** @var ContentType $contentType */
        $contentType = null;
        try {
            $contentType = unserialize($dataAfterSerialized);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
        $attributes = $contentType->getAttributes();
        $this->assertTrue($attributes['_id'] instanceof \MongoDB\BSON\ObjectID);
        return $contentType;
    }

    public function test_content_type_can_be_save_after_serializing() {
        $contentType = $this->test_content_type_can_be_deserialized();
        $contentType->name = 'Order';
        $contentType->save();
        $persistContentType = StructureService::getContentTypeByAlias('test_content_type');
        $this->assertEquals('Order', $persistContentType->name);
    }

    public function test_content_type_with_field_can_be_serialized() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $addressField = new \LaunchCMS\Models\Content\StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        StructureService::addField('order', $addressField);

        $contentType = StructureService::getContentTypeByAlias('order');
        $gotException = false;
        $dataAfterSerialized = null;
        try {
            $dataAfterSerialized = serialize($contentType);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
        return $dataAfterSerialized;
    }

    public function test_content_type_with_field_can_be_deserialized() {
        $dataAfterSerialized = $this->test_content_type_with_field_can_be_serialized();
        /** @var ContentType $contentType */
        $contentType = null;
        $gotException = false;
        try {
            $contentType = unserialize($dataAfterSerialized);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);
        $attributes = $contentType->getAttributes();
        $this->assertTrue(is_array($attributes['fields']));
        $firstField = $attributes['fields'][0];
        $this->assertTrue(is_array($firstField));
        $this->assertTrue($firstField['_id'] instanceof  \MongoDB\BSON\ObjectID);
        return $contentType;
    }

    public function test_content_type_with_field_can_be_save_after_deserializing() {
        $contentType = $this->test_content_type_with_field_can_be_deserialized();
        /** @var \LaunchCMS\Models\Content\Field $field */
        $field = $contentType->fields()->first();
        $field->setName('New Address');
        $field->save();

        $contentType = StructureService::getContentTypeByAlias('order');
        $field = $contentType->fields()->first();
        $this->assertEquals('New Address', $field->getName());

    }



    public function testCreatingContentTypeWithInstallationMode() {
        RuntimeSetting::$runningMode = RuntimeSetting::INSTALLATION_MODE;
        StructureService::createContentType([
            'name' => 'Users',
            'description' => 'The built-in structure of user',
            'alias' => 'cms_users'
        ]);
    }

    public function test_cannot_delete_builtin_content_type() {
        RuntimeSetting::$runningMode = RuntimeSetting::INSTALLATION_MODE;
        StructureService::createContentType([
            'name' => 'Users',
            'description' => 'The built-in structure of user',
            'alias' => 'cms_users',
            'dev_settings' => [
                'builtin' => true
            ]
        ]);
        try {
            StructureService::deleteContentTypeByAlias('cms_users');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof ContentTypeException);
            $this->assertEquals(ContentTypeException::CANNOT_DELETE_BUILTIN_CONTENT_TYPE, $ex->getCode());
            return;
        }
        $this->fail('Expected exception has not been thrown');

    }

    public function test_can_define_dev_fields_in_content_type() {
        RuntimeSetting::$runningMode = RuntimeSetting::INSTALLATION_MODE;
        StructureService::createContentType([
            'name' => 'Users',
            'description' => 'The built-in structure of user',
            'alias' => 'cms_users',
            'dev_settings' => [
                'builtin' => true,
                'dev_fields' => ['password', 'email', 'last_login']
            ]
        ]);
        $contentTypeRow = MongoHelper::getMongoDB()->collection('cms_content_types')->first();
        $this->assertTrue(count($contentTypeRow['dev_settings']['dev_fields']) === 3);


    }

}