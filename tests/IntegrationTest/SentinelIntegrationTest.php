<?php
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

class SentinelIntegrationTest extends TestCase
{
    public function tearDown()
    {

        Schema::drop('cms_users');
        Schema::drop('cms_roles');
        Schema::drop('cms_activations');
        Schema::drop('cms_throttles');
        Schema::drop('cms_persistences');
    }

    protected function registerTestUser()
    {
        return Sentinel::registerAndActivate([
            'email'    => 'test@example.com',
            'password' => 'password',
        ]);
    }

    public function test_register()
    {
        $this->registerTestUser();
        $result = Sentinel::authenticate([
            'email'    => 'test@example.com',
            'password' => 'password',
        ]);
        $this->assertTrue($result instanceof \LaunchCMS\Models\User\User);

        //test with login failed case
        $result = Sentinel::authenticate([
            'email'    => 'test@example.com',
            'password' => 'passwordwrong',
        ]);
        $this->assertFalse($result);
    }

    public function test_login()
    {
        $guest = Sentinel::guest();
        $this->assertTrue($guest);
        $this->registerTestUser();
        Sentinel::authenticate([
            'email'    => 'test@example.com',
            'password' => 'password',
        ]);
        $user = Sentinel::getUser();
        $this->assertTrue($user instanceof \LaunchCMS\Models\User\User);
    }

    public function test_create_role_and_assign_to_user()
    {
        $role = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Subscribers',
            'slug' => 'subscribers',
        ]);
        $user = $this->registerTestUser();

        //test add role by role interface
        $user->addRole($role);
        $user = Sentinel::findById($user->getUserId());
        $this->assertTrue($user->inRole($role));
        $this->assertTrue($user->inRole('subscribers'));
        $this->assertTrue(in_array($role->getRoleId(), $user->roles));
        $this->assertEquals(1, count($user->getRoles()));

        $role = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Tester',
            'slug' => 'tester',
        ]);
        //test add role by slug
        $user->addRole('tester');
        $user = Sentinel::findById($user->getUserId());
        $this->assertTrue(in_array($role->getRoleId(), $user->roles));
        $this->assertEquals(2, count($user->getRoles()));

        $role = Sentinel::findRoleBySlug('tester');
        $this->assertEquals(1, count($role->users()));


    }

    public function test_remove_role()
    {
        $role = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Subscribers',
            'slug' => 'subscribers',
        ]);
        $user = $this->registerTestUser();
        $user->addRole($role);
        //test remove role by role interface
        $user->removeRole($role);
        $user = Sentinel::findById($user->getUserId());
        $this->assertTrue(!in_array($role->getRoleId(), $user->roles));
        $this->assertEquals(0, count($user->getRoles()));


        //test remove role by slug
        $user->addRole($role);
        $user->removeRole('subscribers');
        $user = Sentinel::findById($user->getUserId());
        $this->assertTrue(!in_array($role->getRoleId(), $user->roles));
        $this->assertEquals(0, count($user->getRoles()));

    }

    public function test_add_permission_to_user()
    {
        $user = $this->registerTestUser();
        $user->addPermission('user.create');
        $user->addPermission('user.update');
        $user->save();
        $user = Sentinel::findById($user->getUserId());
        $this->assertTrue($user instanceof \LaunchCMS\Models\User\User);
        $this->assertTrue($user->hasAccess('user.create'));
        $this->assertTrue($user->hasAccess('user.update'));
    }

    public function test_add_permission_to_role()
    {
        $role = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Subscribers',
            'slug' => 'subscribers',
        ]);
        $role->addPermission('user.create');
        $role->save();
        $role = Sentinel::findRoleBySlug('subscribers');
        $this->assertTrue($role->hasAccess('user.create'));
    }

    public function test_activation()
    {
        $user = Sentinel::register([
            'email'    => 'test@example.com',
            'password' => 'password',
        ]);
        $activation = Activation::exists($user);
        $this->assertFalse($activation);

        Activation::create($user);
        $activation = Activation::exists($user);
        $this->assertTrue($activation instanceof \LaunchCMS\Models\User\Activation);
    }

}