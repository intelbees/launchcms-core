<?php
use Cviebrock\LaravelElasticsearch\Facade as ElasticSearch;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\DateField;
use LaunchCMS\Models\Content\EmbeddedField;
use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Facades\SearchService;

class ElasticSearchTest extends TestCase
{
    public function tearDown ()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('order');
        try {
            SearchService::deleteIndex(config('launchcms.default_elastic_index'));
        } catch (Exception $ex) {

        }

    }


    public function test_indexing_without_exception() {
        $data = [
            'body' => [
                'testField' => 'abc'
            ],
            'index' => config('launchcms.default_elastic_index'),
            'type' => 'my_type',
            'id' => 'my_id',
        ];

        Elasticsearch::index($data);
    }

    public function test_get_index_data_of_content() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        $addressField->setAllowFullTextSearch(true);
        StructureService::addField('order', $addressField);

        $customerNameField = new StringField();
        $customerNameField->setName('Customer name');
        $customerNameField->setAlias('customer_name');

        StructureService::addField('order', $customerNameField);

        $content = new Content();
        $content->name = 'test order';
        $content->address = 'NewYork city';
        $content->customer_name = 'John';
        $id = ContentService::saveContent('order', $content);

        $content = ContentService::getContentByID('order', $id );
        $indexData = $content->getIndexData();
        $this->assertTrue(array_key_exists('address', $indexData));
        $this->assertFalse(array_key_exists('customer_name', $indexData));

    }

    public function test_all_built_in_fields_must_be_in_index_data() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        $addressField->setAllowFullTextSearch(true);
        StructureService::addField('order', $addressField);
        $content = new Content();
        $content->name = 'test order';
        $content->address = 'NewYork city';
        $id = ContentService::saveContent('order', $content);

        $content = ContentService::getContentByID('order', $id );
        $indexData = $content->getIndexData();

        foreach (Content::$builtinField as $fieldKey) {
            if($fieldKey === '_id' || $fieldKey === 'workflow_data') {
                continue;
            }
            $this->assertTrue(array_key_exists($fieldKey, $indexData));
        }
    }

    public function test_field_date_must_be_coverted_to_string_in_indexed_content() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $purchasedDateField = new DateField();
        $purchasedDateField->setName('Purchased Date');
        $purchasedDateField->setAlias('purchased_date');
        $purchasedDateField->setAllowFullTextSearch(true);
        StructureService::addField('order', $purchasedDateField);


        $shippingTimeField = new DateField();
        $shippingTimeField->setName('Shipping Time');
        $shippingTimeField->setAlias('shipping_time');
        $shippingTimeField->setAllowFullTextSearch(true);
        StructureService::addField('order', $shippingTimeField);


        $content = new Content();
        $content->name = 'test order';
        $content->purchased_date = new \Carbon\Carbon();
        $content->shipping_time = new \Carbon\Carbon();
        $id = ContentService::saveContent('order', $content);

        $content = ContentService::getContentByID('order', $id );
        $indexData = $content->getIndexData();

        $this->assertTrue(is_string($indexData['created_at']));
        $this->assertTrue(is_string($indexData['updated_at']));
        $this->assertTrue(is_string($indexData['purchased_date']));
        $this->assertTrue(is_string($indexData['shipping_time']));

    }

    public function test_create_indexed_content_with_embedded_field() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);

        $shippingDataType = StructureService::createDataType([ 'name'        => 'Shipping info',
                                                               'description' => 'Shipping info', 'alias' => 'shipping_info']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAllowFullTextSearch(true);
        $addressField->setAlias('address');
        StructureService::addFieldToDataType('shipping_info', $addressField);


        $customerNameField = new StringField();
        $customerNameField->setName('Customer name');
        $customerNameField->setAlias('customer_name');
        StructureService::addFieldToDataType('shipping_info', $customerNameField);


        $embeddedShippingField = new EmbeddedField($shippingDataType->_id);
        $embeddedShippingField->setName('shipping');
        $embeddedShippingField->setAllowFullTextSearch(true);
        $embeddedShippingField->setAlias('shipping');
        StructureService::addField('order', $embeddedShippingField);

        $content = new Content();
        $content->name = 'test order';
        $content->shipping = ['address' => 'NewYork city', 'customer_name' => 'John'];
        $id = ContentService::saveContent('order', $content);

        $content = ContentService::getContentByID('order', $id );
        $indexData = $content->getIndexData();
        $this->assertTrue(array_key_exists('shipping', $indexData));
        $this->assertTrue(array_key_exists('address', $indexData['shipping']));
        $this->assertFalse(array_key_exists('customer_name', $indexData['shipping']));
    }

    public function test_index_content_success_case() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        $addressField->setAllowFullTextSearch(true);
        StructureService::addField('order', $addressField);
        $content = new Content();
        $content->name = 'test order';
        $content->address = 'NewYork city';
        $id = ContentService::saveContent('order', $content);
        $content = ContentService::getContentByID('order', $id );
        $gotException = false;
        try {
            SearchService::indexContent($content);
        } catch (Exception $ex) {
            $gotException = true;
        }
        $this->assertFalse($gotException);

    }

    private function createOrderStructure() {
        StructureService::createContentType([ 'name'        => 'Order',
                                              'description' => 'Order description', 'alias' => 'order']);
        $addressField = new StringField();
        $addressField->setName('Address');
        $addressField->setAlias('address');
        $addressField->setAllowFullTextSearch(true);
        StructureService::addField('order', $addressField);
    }

    private function createAndIndexSampleOrder($name, $address) {
        $content = new Content();
        $content->name = $name;
        $content->address = $address;
        $id = ContentService::saveContent('order', $content);
        $content = ContentService::getContentByID('order', $id );
        SearchService::indexContent($content);
    }

    public function test_search_with_all_query() {

        $this->createOrderStructure();
        $this->createAndIndexSampleOrder('test order', 'NewYork city');
        // Need to sleep here to wait for the content indexing.
        // Refer to: https://www.elastic.co/guide/en/elasticsearch/guide/current/near-real-time.html
        sleep(2);
        $matchAll = new ONGR\ElasticsearchDSL\Query\MatchAllQuery();
        $search = new ONGR\ElasticsearchDSL\Search();
        $search->addQuery($matchAll);
        $results = SearchService::search($search);
        $this->assertEquals(1, $results['hits']['total']);
    }

    public function test_search_in_content_type() {
        $this->createOrderStructure();
        $this->createAndIndexSampleOrder('test order 1', 'NewYork city');
        $this->createAndIndexSampleOrder('test order 2', 'Tokyo city');

        // Need to sleep here to wait for the content indexing.
        // Refer to: https://www.elastic.co/guide/en/elasticsearch/guide/current/near-real-time.html
        sleep(2);
        $matchAll = new ONGR\ElasticsearchDSL\Query\MatchAllQuery();
        $results = SearchService::searchInContentType('order', $matchAll);
        $this->assertEquals(2, $results['hits']['total']);
    }

}