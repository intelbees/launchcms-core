<?php
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use LaunchCMS\Models\User\RelationshipDefinition;
use LaunchCMS\Services\Facades\UserService;
use LaunchCMS\Models\User\User;
use LaunchCMS\MongoDB\MongoHelper;

class UserServiceTest extends TestCase
{
    public function tearDown ()
    {

        Schema::drop ( 'cms_users' );
        Schema::drop ( 'cms_roles' );
        Schema::drop ( 'cms_activations' );
        Schema::drop ( 'cms_throttles' );
        Schema::drop ( 'cms_persistences' );
        Schema::drop ( 'cms_relationship_definitions' );
        Schema::drop ( 'cms_user_connection_requests' );
    }

    protected function registerTestUser ()
    {
        Sentinel::registerAndActivate ( [
            'email'    => 'marry@example.com' ,
            'password' => 'password' ,
        ] );

        Sentinel::registerAndActivate ( [
            'email'    => 'john@example.com' ,
            'password' => 'password' ,
        ] );
    }

    public function test_can_create_relationship_from_array() {
        RelationshipDefinition::create(['name' => 'Friend', 'alias'=>'friends', 'inverse' => true, 'need_approval' => false]);
        $dbRecord = MongoHelper::getMongoDB()->collection('cms_relationship_definitions')->first();
        $this->assertNotNull($dbRecord);
        $this->assertEquals('friends', $dbRecord['alias']);
    }
    public function test_add_relationship_require_approval ()
    {
        $this->registerTestUser ();
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );
        $relationshipDefinition = new RelationshipDefinition();
        $relationshipDefinition->setName ( 'Friend relationship' );
        $relationshipDefinition->setAlias ( 'friends' );
        $relationshipDefinition->setNeedApproval ( true );
        $relationshipDefinition->setInverse ( true );
        $relationshipDefinition->save ();
        UserService::createRelationship ( 'friends' , $marry , $john );
        $requestConnectionRecord = MongoHelper::getMongoDB()->collection ( 'cms_user_connection_requests' )->first ();
        $this->assertNotNull ( $requestConnectionRecord );
        $this->assertTrue ( $requestConnectionRecord[ 'request_user_id' ] == $marry->_id );
        $this->assertTrue ( $requestConnectionRecord[ 'target_user_id' ] == $john->_id );
        $this->assertTrue ( $requestConnectionRecord[ 'relationship_id' ] == $relationshipDefinition->_id );

    }

    public function test_add_non_inverse_relationship_without_approval ()
    {
        $this->registerTestUser ();
        /** @var \LaunchCMS\Models\User\User $marry */
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        /** @var \LaunchCMS\Models\User\User $john */
        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );
        $relationshipDefinition = new RelationshipDefinition();
        $relationshipDefinition->setName ( 'Friend relationship' );
        $relationshipDefinition->setAlias ( 'follows' );
        $relationshipDefinition->setNeedApproval ( false );
        $relationshipDefinition->setInverse ( false );
        $relationshipDefinition->save ();
        
        UserService::createRelationship ( 'follows' , $marry , $john );
        $requestConnectionRecord = MongoHelper::getMongoDB()->collection ( 'cms_user_connection_requests' )->first ();
        $this->assertNull ( $requestConnectionRecord );

        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $this->assertTrue ( $marry->inRelationship ( 'follows' , $john->_id ) );
    }

    public function test_add_inverse_relationship_without_approval ()
    {
        $this->registerTestUser ();
        /** @var \LaunchCMS\Models\User\User $marry */
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        /** @var \LaunchCMS\Models\User\User $john */
        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );

        $relationshipDefinition = new RelationshipDefinition();
        $relationshipDefinition->setName ( 'Friend relationship' );
        $relationshipDefinition->setAlias ( 'friends' );
        $relationshipDefinition->setNeedApproval ( false );
        $relationshipDefinition->setInverse ( true );
        $relationshipDefinition->save ();
        UserService::createRelationship ( 'friends' , $marry , $john );
        $requestConnectionRecord = MongoHelper::getMongoDB()->collection ( 'cms_user_connection_requests' )->first ();
        $this->assertNull ( $requestConnectionRecord );

        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $this->assertTrue ( $marry->inRelationship ( 'friends' , $john->_id ) );

        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );
        $this->assertTrue ( $john->inRelationship ( 'friends' , $marry->_id ) );
    }

    public function test_approve_inverse_relationship_connection ()
    {
        $this->registerTestUser ();
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );
        $relationshipDefinition = new RelationshipDefinition();
        $relationshipDefinition->setName ( 'Friend relationship' );
        $relationshipDefinition->setAlias ( 'friends' );
        $relationshipDefinition->setNeedApproval ( true );
        $relationshipDefinition->setInverse ( true );
        $relationshipDefinition->save ();
        UserService::createRelationship ( 'friends' , $marry , $john );
        $requestConnectionRecord = MongoHelper::getMongoDB()->collection ( 'cms_user_connection_requests' )->first ();
        $this->assertNotNull ( $requestConnectionRecord );
        UserService::approveConnectionRequest ( $requestConnectionRecord[ '_id' ] );
        $requestConnectionRecord = MongoHelper::getMongoDB()->collection ( 'cms_user_connection_requests' )->first ();
        //expect the request record to be deleted
        $this->assertNull ( $requestConnectionRecord );
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $this->assertTrue ( $marry->inRelationship ( 'friends' , $john->_id ) );

        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );
        $this->assertTrue ( $john->inRelationship ( 'friends' , $marry->_id ) );
    }

    public function test_remove_relationship ()
    {
        $this->test_approve_inverse_relationship_connection ();
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );
        UserService::removeRelationship ( 'friends' , $marry->_id , $john->_id );
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $this->assertFalse ( $marry->inRelationship ( 'friends' , $john->_id ) );
        $john = Sentinel::findByCredentials ( [ 'login' => 'john@example.com' ] );
        $this->assertFalse ( $john->inRelationship ( 'friends' , $marry->_id ) );
    }

    public function test_update_user_credential ()
    {
        $this->registerTestUser ();
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        UserService::setUserPassword ( $marry->_id , 'newpassword' );
        $result = Sentinel::authenticate ( [
            'email'    => 'marry@example.com' ,
            'password' => 'newpassword' ,
        ] );
        $this->assertTrue ( $result instanceof \LaunchCMS\Models\User\User );
    }

    public function test_update_user_must_throw_event ()
    {
        $this->registerTestUser ();
        /** @var \LaunchCMS\Models\User\User $marry */
        $marry = Sentinel::findByCredentials ( [ 'login' => 'marry@example.com' ] );
        $marry->name = 'New name';
        $this->expectsEvents ( \LaunchCMS\Events\UserWasUpdated::class );
        UserService::updateUser ( $marry );
    }

    public function test_must_login_after_activation_registration_successfully ()
    {
        $user = new User();
        $user->name = 'Test user';
        $user->email = 'testuser@dev.com';
        $user->password = 'testpassword';
        UserService::registerAndActivate ( $user );
        $result = Sentinel::authenticate ( [
            'email'    => 'testuser@dev.com' ,
            'password' => 'testpassword' ,
        ] );
        $this->assertTrue ( $result instanceof \LaunchCMS\Models\User\User );
    }

    public function test_must_keep_information_in_registration_process ()
    {
        $user = new User();
        $user->name = 'Test user';
        $user->email = 'testuser@dev.com';
        $user->password = 'testpassword';
        $user->additional_email = 'another_email@dev.com';
        UserService::registerAndActivate ( $user );
        $userAfterRegistration = User::where ( 'email' , 'testuser@dev.com' )->first ();
        $this->assertEquals ( 'another_email@dev.com' , $userAfterRegistration->additional_email );
    }

}