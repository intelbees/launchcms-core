<?php

use LaunchCMS\Widget\AbstractWidget;

class TestWidget extends AbstractWidget
{
    public function render($params = null)
    {
        return "Widget content";
    }

}