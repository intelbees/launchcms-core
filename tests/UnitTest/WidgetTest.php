<?php
use LaunchCMS\Widget\WidgetLocator;
use LaunchCMS\Widget\Facade\WidgetService;

class WidgetTest extends WidgetTestCase
{
    public function test_load_widget()
    {
        $widget = WidgetService::get('test-widget');
        $this->assertNotNull($widget);
        $content = $widget->render();
        $this->assertTrue("Widget content" === $content);
    }

    public function test_load_widget_in_block()
    {
        $widgets = WidgetService::widgetsInBlock('dashboard', 'stats');
        $this->assertNotNull($widgets);
        $this->assertEquals(2, count($widgets));
    }
}