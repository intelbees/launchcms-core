<?php
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\WorkflowSystemStatus;
use LaunchCMS\Services\Exceptions\WorkflowException;
use LaunchCMS\Services\WorkflowEngine;
use LaunchCMS\Models\User\User;

class WorkflowEngineTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('cms_workflows');

        Mockery::close();
    }
    protected function getWorkflowDefinitionWithoutAuthorization() {
        return [
            'states'      => [
                [ 'name' => 'Draft', 'alias' => 'draft', 'mapped_system_state' => WorkflowSystemStatus::DRAFT ],
                [ 'name' => 'In progress', 'alias' => 'in_progress', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED ],
                [ 'name' => 'Published', 'alias' => 'published', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED ],
            ],
            'transitions' => [
                [ 'name' => 'Initiate', 'from' => 'nil', 'to' => 'draft' ],
                [ 'name' => 'Start working', 'from' => 'draft', 'to' => 'in_progress' ],
                [ 'name' => 'Send to publish', 'from' => 'in_progress', 'to' => 'published' ],
                [ 'name' => 'Send back to in progress', 'from' => 'published', 'to' => 'in_progress' ],
            ],
        ];
    }

    protected function getWorkflowDefinitionWithAuthorization() {
        return [
            'states'      => [
                [ 'name' => 'Draft', 'alias' => 'draft', 'mapped_system_state' => WorkflowSystemStatus::DRAFT ],
                [ 'name' => 'In progress', 'alias' => 'in_progress', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED ],
                [ 'name' => 'Published', 'alias' => 'published', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED ],
            ],
            'transitions' => [
                [ 'name' => 'Initiate', 'from' => 'nil', 'to' => 'draft', 'responsible_users'=>['test_user@email.local'] ],
                [ 'name' => 'Start working', 'from' => 'draft', 'to' => 'in_progress', 'responsible_roles' =>['content_writer'] ],
                [ 'name' => 'Send to publish', 'from' => 'in_progress', 'to' => 'published'],
                [ 'name' => 'Send back to in progress', 'from' => 'published', 'to' => 'in_progress' ],
            ],
        ];
    }
    public function test_throw_exception_if_passing_null_workflow_state()
    {
        $workflowEngine = new WorkflowEngine();
        $content = new Content();
        try {
            $workflowEngine->changeState($content, null);
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof WorkflowException);
            $this->assertEquals(WorkflowException::INVALID_WORKFLOW_STATE, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_throw_exception_if_passing_non_exist_workflow_state()
    {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithoutAuthorization();
        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $content = new Content();
        try {
            $workflowEngine->changeState($content, 'not_exist_state');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof WorkflowException);
            $this->assertEquals(WorkflowException::INVALID_WORKFLOW_STATE, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_throw_exception_if_workflow_json_is_invalid_when_loading_workflow() {
        $workflowEngine = new WorkflowEngine();
        try {
            $workflowEngine->loadWorkflow('Invalid JSON format');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof WorkflowException);
            $this->assertEquals(WorkflowException::INVALID_WORKFLOW_JSON_FORMAT, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_throw_exception_if_new_state_not_invalid_after_checking_transition_rules() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithoutAuthorization();

        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $content = new Content();
        $content->setWorkflowStatus('draft');
        try {
            $workflowEngine->changeState($content, 'published');
        } catch (Exception $ex) {
            $this->assertTrue($ex instanceof WorkflowException);
            $this->assertEquals(WorkflowException::NO_TRIGGER_RULE_TO_THE_NEW_STATE, $ex->getCode());

            return;
        }
        $this->fail('Expected exception has not been thrown');
    }

    public function test_state_must_be_change_if_everything_valid() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithoutAuthorization();

        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $content = new Content();
        $content->setWorkflowStatus('draft');
        $workflowEngine->changeState($content, 'in_progress');
        $this->assertEquals('in_progress', $content->getWorkflowStatus());
    }

    public function test_get_next_transitions_returned_correct_data_with_null_state() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithoutAuthorization();
        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $nextTransitions = $workflowEngine->getNextTransitionsFromState(null);
        $this->assertEquals(1, count($nextTransitions));
        $this->assertEquals('nil', $nextTransitions[0]['from']);
        $this->assertEquals('draft', $nextTransitions[0]['to']);
    }

    public function test_get_next_transitions_returned_correct_data_with_not_null_state() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithoutAuthorization();
        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $nextTransitions = $workflowEngine->getNextTransitionsFromState('draft');
        $this->assertEquals(1, count($nextTransitions));
        $this->assertEquals('draft', $nextTransitions[0]['from']);
        $this->assertEquals('in_progress', $nextTransitions[0]['to']);
    }

    public function test_return_transition_with_responsible_users_happy_case() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithAuthorization();
        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $user = new User();
        $user->email = 'test_user@email.local';
        $nextTransitions = $workflowEngine->getNextTransitionsFromState(null, $user);
        $this->assertEquals(1, count($nextTransitions));
        $this->assertEquals('draft', $nextTransitions[0]['to']);
    }

    public function test_return_transition_with_responsible_users_not_match_email_case() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithAuthorization();
        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $user = new User();
        $user->email = 'not_match@email.local';
        $nextTransitions = $workflowEngine->getNextTransitionsFromState(null, $user);
        $this->assertEquals(0, count($nextTransitions));
    }

    public function test_return_transition_with_responsible_roles_happy_case() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithAuthorization();
        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $user = $this->getMock(User::class);
        $user->expects($this->once())->method('getRoleSlugs')->will($this->returnValue(['content_writer']));
        $nextTransitions = $workflowEngine->getNextTransitionsFromState('draft', $user);
        $this->assertEquals(1, count($nextTransitions));
        $this->assertEquals('in_progress', $nextTransitions[0]['to']);
    }

    public function test_return_transition_with_responsible_roles_no_match_case() {
        $workflowEngine = new WorkflowEngine();
        $workflowDefinition = $this->getWorkflowDefinitionWithAuthorization();
        $workflowEngine->loadWorkflow(json_encode($workflowDefinition));
        $user = $this->getMock(User::class);
        $user->expects($this->once())->method('getRoleSlugs')->will($this->returnValue(['no_match_role']));
        $nextTransitions = $workflowEngine->getNextTransitionsFromState('draft', $user);
        $this->assertEquals(0, count($nextTransitions));
    }

}