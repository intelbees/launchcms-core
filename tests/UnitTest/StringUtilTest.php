<?php

use LaunchCMS\Utils\StringUtil;

class StringUtilTest extends TestCase
{
    public function test_sanitize_default()
    {
        $title = "Test Content Type";
        $output = StringUtil::sanitize($title);
        $this->assertEquals("test-content-type", $output);
    }

    public function test_sanitize_with_replacement()
    {
        $title = "Test Content Type";
        $output = StringUtil::sanitize($title, '_');
        $this->assertEquals("test_content_type", $output);
    }
}