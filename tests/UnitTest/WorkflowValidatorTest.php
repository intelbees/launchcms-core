<?php
use LaunchCMS\Services\WorkflowValidator;
use LaunchCMS\Models\Content\WorkflowSystemStatus;
use LaunchCMS\Services\WorkflowEngine;
class WorkflowValidatorTest extends TestCase
{
    public function tearDown()
    {
        Schema::drop('cms_content_types');
        Schema::drop('cms_data_types');
        Schema::drop('cms_workflows');

        Mockery::close();
    }

    public function test_return_false_if_workflow_is_not_a_valid_json_format() {
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON("very bad json format");
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_JSON_FORMAT, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_workflow_json_not_contains_states_section() {
        $jsonArray = [];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::MISSING_STATES_SECTION, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_workflow_json_not_contains_transitions_section() {
        $jsonArray = [
            'states' => [
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => 'published']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::MISSING_TRANSITIONS_SECTION, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_any_workflow_state_not_contain_alias() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress'],
                ['name' => 'Reviewing', 'alias' => 'reviewing']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_STATE_DATA, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_state_alias_is_invalid_naming() {

        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'nil', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED], //nil is invalid name
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_STATE_DATA, $workflowValidator->getErrorCode());
    }


    public function test_return_false_if_state_without_name_but_not_contains_trans_key() {
        $jsonArray = [
            'states' => [
                ['alias' => 'in_progress', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED]
            ],
            'transitions' => [
                ['name' => 'Send to review', 'from' => 'in_progress', 'to' => 'reviewing']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_STATE_DATA, $workflowValidator->getErrorCode());
    }


    public function test_return_false_workflow_state_not_contain_mapped_system_state() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'in_progress', ],
                ['name' => 'Reviewing', 'alias' => 'reviewing']
            ],
            'transitions' => [
                ['name' => 'Send to review', 'from' => 'composing', 'to' => 'reviewing'],
                ['name' => 'Reject', 'from' => 'reviewing', 'to' => 'rejected'],
                ['name' => 'Approve', 'from' => 'reviewing', 'to' => 'approved']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_STATE_DATA, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_any_transition_not_contain_from() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'composing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Rejected', 'alias' => 'rejected', 'mapped_system_state' => WorkflowSystemStatus::REJECTED],
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED]
            ],
            'transitions' => [
                ['name' => 'Send to review',  'to' => 'reviewing'],
                ['name' => 'Reject', 'from' => 'reviewing', 'to' => 'rejected'],
                ['name' => 'Approve', 'from' => 'reviewing', 'to' => 'approved']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_TRANSITION_DATA, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_any_transition_not_contain_name_or_trans_key() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'composing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Rejected', 'alias' => 'rejected', 'mapped_system_state' => WorkflowSystemStatus::REJECTED],
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED]
            ],
            'transitions' => [
                ['from' => 'reviewing', 'to' => 'rejected'],
                ['name' => 'Approve', 'from' => 'reviewing', 'to' => 'approved']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_TRANSITION_DATA, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_any_transition_not_contain_to() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'composing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Rejected', 'alias' => 'rejected', 'mapped_system_state' => WorkflowSystemStatus::REJECTED],
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED]
            ],
            'transitions' => [
                ['name' => 'Reject', 'from' => 'reviewing'],
                ['name' => 'Approve', 'from' => 'reviewing', 'to' => 'approved']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::INVALID_TRANSITION_DATA, $workflowValidator->getErrorCode());
    }

    public function test_return_false_transition_from_point_to_not_exist_state() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'composing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Rejected', 'alias' => 'rejected', 'mapped_system_state' => WorkflowSystemStatus::REJECTED],
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED]
            ],
            'transitions' => [
                ['name' => 'Reject', 'from' => 'non_exist_state', 'to' => 'rejected'],

            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::TRANSITION_POINT_TO_NON_EXIST_STATE, $workflowValidator->getErrorCode());
    }

    public function test_return_false_transition_to_point_to_not_exist_state() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'composing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Rejected', 'alias' => 'rejected', 'mapped_system_state' => WorkflowSystemStatus::REJECTED],
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED]
            ],
            'transitions' => [
                ['name' => 'Reject', 'from' => 'reviewing', 'to' => 'non_exist_state'],

            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::TRANSITION_POINT_TO_NON_EXIST_STATE, $workflowValidator->getErrorCode());
    }

    public function test_return_false_if_workflow_contains_duplicated_state() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'composing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Rejected', 'alias' => 'rejected', 'mapped_system_state' => WorkflowSystemStatus::REJECTED],
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED]
            ],
            'transitions' => [
                ['name' => 'Reject', 'from' => 'reviewing', 'to' => 'rejected'],

            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertFalse($result);
        $this->assertEquals(WorkflowValidator::DUPLICATED_STATE, $workflowValidator->getErrorCode());
    }

    public function test_return_true_if_workflow_schema_ok() {
        $jsonArray = [
            'states' => [
                ['name' => 'Work in progress', 'alias' => 'composing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Reviewing', 'alias' => 'reviewing', 'mapped_system_state' => WorkflowSystemStatus::UNPUBLISHED],
                ['name' => 'Rejected', 'alias' => 'rejected', 'mapped_system_state' => WorkflowSystemStatus::REJECTED],
                ['name' => 'Approved', 'alias' => 'approved', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED]
            ],
            'transitions' => [
                ['name' => 'Send to review', 'from' => 'composing', 'to' => 'reviewing'],
                ['name' => 'Reject', 'from' => 'reviewing', 'to' => 'rejected'],
                ['name' => 'Approve', 'from' => 'reviewing', 'to' => 'approved']
            ]
        ];
        $jsonString = json_encode($jsonArray);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertTrue($result);
    }

    public function test_default_workflow_definition_must_pass_validation() {
        $jsonString = json_encode(WorkflowEngine::$defaultWorkflowDefinition);
        $workflowValidator = new WorkflowValidator();
        $result = $workflowValidator->validateWorkflowJSON($jsonString);
        $this->assertTrue($result);
    }


}