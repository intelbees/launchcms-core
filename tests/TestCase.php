<?php
use Illuminate\Support\Facades\Cache;
use LaunchCMS\MongoDB\MongoHelper;

class TestCase extends Orchestra\Testbench\TestCase
{
    protected $app;
    protected $sentinel;

    /**
     * Get package providers.
     *
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            'Illuminate\Cache\CacheServiceProvider',
            'Jenssegers\Mongodb\MongodbServiceProvider',
            'Jenssegers\Mongodb\Auth\PasswordResetServiceProvider',
            'Cartalyst\Sentinel\Laravel\SentinelServiceProvider',
            'LaunchCMS\LaunchCoreServiceProvider',
            'Cviebrock\LaravelElasticsearch\ServiceProvider',

        ];
    }

    /**
     * Define environment setup.
     *
     * @param  Illuminate\Foundation\Application $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // reset base path to point to our package's src directory
        //$app['path.base'] = __DIR__ . '/../src';

        $config = require 'config/database.php';


        $app[ 'config' ]->set('database.default', 'mongodb');
        $app[ 'config' ]->set('database.connections.mysql', $config[ 'connections' ][ 'mysql' ]);
        $app[ 'config' ]->set('database.connections.mongodb', $config[ 'connections' ][ 'mongodb' ]);
        $app[ 'config' ]->set('database.connections.mongodb_admin', $config[ 'connections' ][ 'mongodb_admin' ]);
        $app[ 'config' ]->set('database.redis', $config['redis']);
        $app[ 'config' ]->set('auth.model', 'User');
        $app[ 'config' ]->set('auth.providers.users.model', 'User');
        $app[ 'config' ]->set('cache.driver', 'array');
        $sentinelConfig = require 'config/cartalyst.sentinel.php';
        $app[ 'config' ]->set('cartalyst.sentinel', $sentinelConfig);


        $cacheConfig = require 'config/cache.php';
        $app['config']->set('cache.default', $cacheConfig['default' ]);
        $app['config']->set('cache.stores.redis', $cacheConfig['stores']['redis']);

        $elasticConfig = require 'config/elasticsearch.php';
        $app['config']->set('elasticsearch.connections', $elasticConfig['connections']);
        $app['config']->set('elasticsearch.defaultConnection', $elasticConfig['defaultConnection']);

        $launchcmsConfig = require 'config/launchcms.php';
        $app['config']->set('launchcms', $launchcmsConfig);

        $fileSystemConfig = require 'config/filesystems.php';
        $app['config']->set('filesystems', $fileSystemConfig);

        $this->app = $app;
        //Below line is a very tricky way to trigger Cache in test case to make it run properly.
        //If you remove below line, it will cause the ContentTypeWasCreatedEvent cannot be trigger when running
        //test cases anymore and throw the exception: Illuminate_Contracts_Events_Dispatcher::listen() does not exist on this mock object
        //TODO should check the application setup using TestCase of Illuminate (not Orchestra)
        Cache::flush();

    }

    protected function getIndex($collection, $name)
    {
        $collection = MongoHelper::getMongoDB()->getCollection($collection);
        foreach ($collection->listIndexes() as $index) {
            if (isset($index['key'][$name])) {
                return $index;
            }
        }
        return false;
    }
}