<?php

namespace LaunchCMS\Utils;

use Carbon\Carbon;
use DateTime;
use MongoDB\BSON\UTCDateTime;

class DateUtil
{
    /**
     * Return a timestamp as DateTime object.
     *
     * @param  mixed $value
     * @return \Carbon\Carbon
     */
    public static function asDateTime($value, $dateFormat = 'Y-m-d H:i:s')
    {
        if ($value == null) {
            return null;
        }
        // Convert UTCDateTime instances.
        if ($value instanceof UTCDateTime) {
            return Carbon::createFromTimestamp($value->toDateTime()->getTimestamp());
        }
        // If this value is already a Carbon instance, we shall just return it as is.
        // This prevents us having to reinstantiate a Carbon instance when we know
        // it already is one, which wouldn't be fulfilled by the DateTime check.
        if ($value instanceof Carbon) {
            return $value;
        }

        // If the value is already a DateTime instance, we will just skip the rest of
        // these checks since they will be a waste of time, and hinder performance
        // when checking the field. We will just return the DateTime right away.
        if ($value instanceof DateTime) {
            return Carbon::instance($value);
        }

        // If this value is an integer, we will assume it is a UNIX timestamp's value
        // and format a Carbon object from this timestamp. This allows flexibility
        // when defining your date fields as they might be UNIX timestamps here.
        if (is_numeric($value)) {
            return Carbon::createFromTimestamp($value);
        }

        // If the value is in simply year, month, day format, we will instantiate the
        // Carbon instances from that format. Again, this provides for simple date
        // fields on the database, while still supporting Carbonized conversion.
        if (preg_match('/^(\d{4})-(\d{1,2})-(\d{1,2})$/', $value)) {
            return Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
        }

        // Finally, we will just assume this date is in the format used by default on
        // the database connection and use that format to create the Carbon object
        // that is returned back out to the developers after we convert it here.

        return Carbon::createFromFormat($dateFormat, $value);
    }

    /**
     * Return a timestamp as unix timestamp.
     *
     * @param  mixed $value
     * @return int
     */
    public static function asTimeStamp($value, $dateFormat = 'Y-m-d H:i:s')
    {
        if ($value == null) {
            return $value;
        }

        return (int) self::asDateTime($value, $dateFormat)->timestamp;
    }

    /**
     * Convert a DateTime to a storable UTCDateTime object.
     *
     * @param  DateTime|int $value
     * @return UTCDateTime
     */
    public static function fromDateTime($value)
    {
        if ($value == null) {
            return null;
        }
        // If the value is already a UTCDateTime instance, we don't need to parse it.
        if ($value instanceof UTCDateTime) {
            return $value;
        }

        if ( !$value instanceof DateTime) {
            $value = self::asDateTime($value);
        }

        return new UTCDateTime($value->getTimestamp() * 1000);
    }

    public static function formatDateToIndexedFormat(Carbon $date)
    {
        return $date->toDateString();
    }

    public static function formatDateTimeToIndexedFormat(Carbon $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}