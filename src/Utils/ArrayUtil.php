<?php


namespace LaunchCMS\Utils;


class ArrayUtil
{
    public static function removeElement($needle, array &$haystack)
    {
        $index = 0;
        $foundValue = false;
        foreach ($haystack as $value) {
            if ($value === $needle) {
                $foundValue = true;
                break;
            }
            $index += 1;
        }
        if ($foundValue) {
            self::removeElementAtIndex($index, $haystack);
        }
    }

    public static function removeElementAtIndex($index, array &$haystack)
    {
        array_splice($haystack, $index, 1);
    }
}