<?php

namespace LaunchCMS;


class RuntimeSetting
{
    const INSTALLATION_MODE = 0;
    const LIVE = 1;
    public static $runningMode = self::LIVE;
}