<?php

namespace LaunchCMS\Listeners;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Facades\SearchService;

class ContentIndexerHandler
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function onDeleteContent($event) {
        /** @var Content $content */
        $content = $event->content;
        SearchService::deleteDocument($content->contentTypeID, $content->id);
    }

    public function onSaveContent($event) {
        /** @var Content $content */
        $content = $event->content;
        SearchService::indexContent($content);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {

        $events->listen(
            'LaunchCMS\Events\ContentAfterDeleting',
            'LaunchCMS\Listeners\ContentIndexerHandler@onDeleteContent'
        );

        $events->listen(
            'LaunchCMS\Events\ContentAfterMovingToTrash',
            'LaunchCMS\Listeners\ContentIndexerHandler@onDeleteContent'
        );

        $events->listen(
            'LaunchCMS\Events\ContentAfterSaving',
            'LaunchCMS\Listeners\ContentIndexerHandler@onSaveContent'
        );
    }

}