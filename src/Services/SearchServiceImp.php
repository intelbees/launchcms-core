<?php

namespace LaunchCMS\Services;


use Cviebrock\LaravelElasticsearch\Facade as ElasticSearch;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Support\Facades\Log;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Exceptions\ContentTypeException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Interfaces\SearchServiceInterface;
use ONGR\ElasticsearchDSL\BuilderInterface;
use ONGR\ElasticsearchDSL\Query\BoolQuery;
use ONGR\ElasticsearchDSL\Query\TermQuery;
use ONGR\ElasticsearchDSL\Search;

class SearchServiceImp implements SearchServiceInterface
{
    public function createSimpleIndex($indexName)
    {
        Elasticsearch::indices()->create([ 'index' => $indexName ]);
    }

    protected function getDefaultIndexName()
    {
        return config('launchcms.default_elastic_index');
    }

    public function deleteIndex($indexName)
    {
        try {
            Elasticsearch::indices()->delete([ 'index' => $indexName ]);
        } catch (Missing404Exception $ex) {
            Log::error('[Elastic] Index not found: ' . $indexName);
        }

    }

    public function indexContent(Content $content)
    {
        if (empty( $content )) {
            return;
        }

        $indexData = $content->getIndexData();
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByID($content->contentTypeID);
        $data = [
            'body'  => $indexData,
            'index' => $this->getDefaultIndexName(),
            'type'  => $contentType->rootContentType()->getAlias(),
            'id'    => $content->id,
        ];
        Elasticsearch::index($data);
    }

    public function deleteDocument($contentTypeID, $contentID)
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByID($contentTypeID);
        $params = [
            'index' => $this->getDefaultIndexName(),
            'type'  => $contentType->rootContentType()->getAlias(),
            'id'    => $contentID,
        ];
        try {
            Elasticsearch::delete($params);
        } catch (Missing404Exception $ex) {
            Log::error('[Elastic] Document not found to delete: ' . $contentID);
        }

    }

    public function deleteDocumentWithContentTypeAlias($contentTypeAlias, $contentID)
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        $params = [
            'index' => $this->getDefaultIndexName(),
            'type'  => $contentType->rootContentType()->getAlias(),
            'id'    => $contentID,
        ];
        Elasticsearch::delete($params);
    }

    public function search(Search $search)
    {
        $params = [
            'index' => $this->getDefaultIndexName(),
            'body'  => $search->toArray(),
        ];

        return ElasticSearch::search($params);
    }

    public function searchInContentType($contentTypeAlias, BuilderInterface $searchQuery)
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $bool = new BoolQuery();
        $termQueryForContentType = new TermQuery("content_type_id", $contentType->rootContentType()->_id);
        $bool->add($termQueryForContentType, BoolQuery::MUST);
        $bool->add($searchQuery, BoolQuery::MUST);
        $search = new Search();
        $search->addQuery($bool);

        $queryArray = $search->toArray();
        $params = [
            'index' => $this->getDefaultIndexName(),
            'body'  => $queryArray,
        ];

        return ElasticSearch::search($params);
    }
}