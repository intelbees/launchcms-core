<?php
namespace LaunchCMS\Services;

use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Models\User\Role;
use LaunchCMS\Models\User\User;
use LaunchCMS\MongoDB\MongoHelper;
use LaunchCMS\RuntimeSetting;
use LaunchCMS\Services\Facades\OrganizationService;
use LaunchCMS\Services\Facades\PermissionService;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Facades\UserService;
use LaunchCMS\Services\Interfaces\InstallationServiceInterface;

class InstallationServiceImp implements InstallationServiceInterface
{

    public function install()
    {
        RuntimeSetting::$runningMode = RuntimeSetting::INSTALLATION_MODE;
        $this->setupSchema();
        $this->setupData();
        RuntimeSetting::$runningMode = RuntimeSetting::LIVE;
    }

    protected function createUserContentType()
    {
        //We create the cms_users, cms_roles in content type to allow developer can add custom fields
        //using the same structure of Content type.
        StructureService::createContentType([
            'name'                         => 'Users',
            'description'                  => 'The built-in structure of user',
            'alias'                        => 'cms_users',
            ContentType::ENABLE_VERSIONING => false,
            ContentType::DEV_SETTINGS      => [
                ContentType::BUILTIN    => true,
                ContentType::DEV_FIELDS => [ 'admin_user', 'password', 'roles', 'last_login', 'permissions', 'groups' ],

            ],
        ]);
        $emailField = new StringField();
        $emailField->setName('Email');
        $emailField->setAlias('email');
        $emailField->setRequired(true);
        $emailField->setUnique(true);
        $emailField->setDevSettings([ 'builtin' => true, 'allow_modify_meta_data' => false ]);
        StructureService::addField('cms_users', $emailField);


        $homeFolderField = new StringField();
        $homeFolderField->setName('Home folder');
        $homeFolderField->setAlias('home');
        $homeFolderField->setRequired(true);
        $homeFolderField->setUnique(true);
        $homeFolderField->setDevSettings([ 'builtin' => true, 'allow_modify_meta_data' => false ]);
        StructureService::addField('cms_users', $homeFolderField);

        StructureService::createContentType([
            'name'                         => 'Roles',
            'description'                  => 'The built-in structure of CMS role',
            'alias'                        => 'cms_roles',
            ContentType::ENABLE_VERSIONING => false,
            ContentType::DEV_SETTINGS      => [
                ContentType::BUILTIN    => true,
                ContentType::DEV_FIELDS => [ 'permissions', 'content_permission_matrix' ],
            ],
        ]);

        $emailField = new StringField();
        $emailField->setName('Role slug');
        $emailField->setAlias('slug');
        $emailField->setRequired(true);
        $emailField->setUnique(true);
        $emailField->setDevSettings([ 'builtin' => true, 'allow_modify_meta_data' => false ]);
        StructureService::addField('cms_roles', $emailField);
    }

    protected function setupSchemaForUserStructure()
    {
        $this->createUserContentType();
        MongoHelper::getMongoDBSchema()->collection('cms_users', function($collection) {
            $collection->string('full_name');
            $collection->index('permissions');
            $collection->index('roles');
            $collection->index('activation_code');
            $collection->index('persist_code');
            $collection->index('name');
            $collection->index('admin_user');
        });

        MongoHelper::getMongoDBSchema()->collection('cms_roles', function($collection) {
            $collection->string('name');
            $collection->unique('slug');
            $collection->index('name');
            $collection->index('permissions');
        });

        MongoHelper::getMongoDBSchema()->collection('cms_sites', function($collection) {
            $collection->string('name');
            $collection->index('name');
            $collection->unique('alias');
        });

        MongoHelper::getMongoDBSchema()->collection('cms_groups', function($collection) {
            $collection->string('name');
            $collection->index('name');
            $collection->unique('alias');
        });

        MongoHelper::getMongoDBSchema()->create('cms_throttles', function($collection) {
            $collection->string('ip');
            $collection->string('type');
            $collection->index('ip');
            $collection->index('type');
        });
    }

    protected function setupSchemaForContentStructure()
    {
        MongoHelper::getMongoDBSchema()->collection('cms_content_types', function($collection) {
            $collection->index('name');
            $collection->string('description');
            $collection->unique('alias');
            $collection->index('inheritPath');
        });

        MongoHelper::getMongoDBSchema()->collection('cms_content_versions', function($collection) {
            $collection->index('content_type_id');
            $collection->index('contentID');

        });
    }

    protected function setupSchema()
    {
        $this->setupSchemaForContentStructure();
        $this->setupSchemaForUserStructure();
    }

    protected function setupData()
    {
        $adminUser = new User();
        $adminUser->email = 'developer@email.local';
        $adminUser->name = 'Developer';
        $adminUser->password = 'secret';
        $adminUser->admin_user = true;
        $adminUser = UserService::registerAndActivate($adminUser);

        /** @var Role $sysAdminRole */
        $sysAdminRole = UserService::createRole('Developer', 'developer');
        $allPermissions = PermissionService::getAllPermissions();
        foreach ($allPermissions as $permissionKey => $value) {
            $sysAdminRole->addPermission($permissionKey, true);
        }
        $sysAdminRole->save();
        $adminUser->addRole('developer');

        //setup default site
        OrganizationService::createSite([ 'name' => 'Default', 'alias' => 'default', 'Description' => 'Default website' ]);
    }
}