<?php

namespace LaunchCMS\Services\Facades;

use Illuminate\Support\Facades\Facade;

class OrganizationService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'OrganizationService';
    }
}