<?php

namespace LaunchCMS\Services\Exceptions;


class MediaException extends CMSServiceException
{
    const FOLDER_EXISTS_ALREADY = 802;
    const FOLDER_MUST_BE_EMPTY_TO_DELETE = 803;
    const FILE_DOES_NOT_EXIST = 804;
    const FOLDER_DOES_NOT_EXIST = 805;


    protected static $errorMessageMapping = [
        self::FOLDER_EXISTS_ALREADY             => [ 'trans_key' => 'launchcms.cms_errors.folder_exist_already',
                                                            'fallback'  => 'Folder already exists' ],
        self::FOLDER_MUST_BE_EMPTY_TO_DELETE  => [ 'trans_key' => 'launchcms.cms_errors.folder_must_be_empty_to_delete',
                                                         'fallback'  => 'Folder must be empty to delete' ],
        self::FILE_DOES_NOT_EXIST  => [ 'trans_key' => 'launchcms.cms_errors.file_does_not_exist',
                                        'fallback'  => 'File does not exist' ],
        self::FOLDER_DOES_NOT_EXIST => [ 'trans_key' => 'launchcms.cms_errors.folder_does_not_exist',
                                        'fallback'  => 'Folder does not exist' ],
    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function folderAlreadyExist()
    {
        return new static("Folder already exists", self::FOLDER_EXISTS_ALREADY);
    }

    public static function folderMustBeEmptyToDelete()
    {
        return new static("Folder must be empty to delete", self::FOLDER_MUST_BE_EMPTY_TO_DELETE);
    }

    public static function fileDoesNotExist()
    {
        return new static("File does not exist", self::FILE_DOES_NOT_EXIST);
    }

    public static function folderDoesNotExist()
    {
        return new static("Folder does not exist", self::FOLDER_DOES_NOT_EXIST);
    }


}