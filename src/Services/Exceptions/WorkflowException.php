<?php

namespace LaunchCMS\Services\Exceptions;


class WorkflowException extends CMSServiceException
{
    const INVALID_WORKFLOW_JSON_FORMAT = 701;
    const INVALID_WORKFLOW_STATE = 702;
    const NO_TRIGGER_RULE_TO_THE_NEW_STATE = 703;
    const WORKFLOW_ALIAS_EMPTY = 704;
    const INVALID_WORKFLOW_ALIAS = 705;
    const WORKFLOW_STILL_HAVE_REFERENCE = 706;
    const WORKFLOW_NOT_FOUND = 707;
    const SCHEDULE_TIME_IS_REQUIRED_FOR_THIS_STATE = 708;

    protected static $errorMessageMapping = [
        self::INVALID_WORKFLOW_JSON_FORMAT             => [ 'trans_key' => 'launchcms.cms_errors.invalid_workflow_json_format',
                                                            'fallback'  => 'Invalid workflow json' ],
        self::INVALID_WORKFLOW_STATE                   => [ 'trans_key' => 'launchcms.cms_errors.invalid_workflow_state',
                                                            'fallback'  => 'Invalid workflow state' ],
        self::NO_TRIGGER_RULE_TO_THE_NEW_STATE         => [ 'trans_key' => 'launchcms.cms_errors.no_trigger_rule_to_the_new_state',
                                                            'fallback'  => 'No trigger rule that causes this new state changes' ],
        self::WORKFLOW_ALIAS_EMPTY                     => [ 'trans_key' => 'launchcms.cms_errors.cannot_set_empty_workflow_alias',
                                                            'fallback'  => 'Cannot set empty alias for workflow' ],
        self::INVALID_WORKFLOW_ALIAS                   => [ 'trans_key' => 'launchcms.cms_errors.invalid_workflow_alias',
                                                            'fallback'  => 'Invalid workflow alias' ],
        self::WORKFLOW_STILL_HAVE_REFERENCE            => [ 'trans_key' => 'launchcms.cms_errors.workflow_still_have_references',
                                                            'fallback'  => 'Workflow still has references' ],
        self::WORKFLOW_NOT_FOUND                       => [ 'trans_key' => 'launchcms.cms_errors.workflow_not_found',
                                                            'fallback'  => 'Workflow not found' ],
        self::SCHEDULE_TIME_IS_REQUIRED_FOR_THIS_STATE => [ 'trans_key' => 'launchcms.cms_errors.schedule_time_is_required_for_this_state',
                                                            'fallback'  => 'Schedule time is required for this state' ],

    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function invalidWorkflowJSON($detailErrorMessage)
    {
        return new WorkflowException("Invalid workflow json", self::INVALID_WORKFLOW_JSON_FORMAT, null, [ $detailErrorMessage ]);
    }

    public static function invalidWorkflowState()
    {
        return new WorkflowException("Invalid workflow state", self::INVALID_WORKFLOW_STATE);
    }

    public static function noTriggerRuleForTheNewState($state)
    {
        return new WorkflowException("No trigger rule that causes this new state changes: " . $state, self::NO_TRIGGER_RULE_TO_THE_NEW_STATE);
    }

    public static function cannotSetEmptyAliasForWorkflow()
    {
        return new WorkflowException("Cannot set empty alias for workflow", self::WORKFLOW_ALIAS_EMPTY);
    }

    public static function invalidWorkflowAlias()
    {
        return new WorkflowException("Invalid workflow alias", self::INVALID_WORKFLOW_ALIAS);
    }

    public static function workflowStillHaveReferences()
    {
        return new WorkflowException("Workflow still has references", self::WORKFLOW_STILL_HAVE_REFERENCE);
    }

    public static function workflowNotFound()
    {
        return new WorkflowException("Workflow not found", self::WORKFLOW_NOT_FOUND);
    }

    public static function scheduleTimeRequired()
    {
        return new WorkflowException("Schedule time is required for this status", self::SCHEDULE_TIME_IS_REQUIRED_FOR_THIS_STATE);
    }
}