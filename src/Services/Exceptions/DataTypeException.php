<?php

namespace LaunchCMS\Services\Exceptions;


class DataTypeException extends CMSServiceException
{
    const FIELD_TYPE_CANNOT_BE_USED_FOR_DATA_TYPE = 300;
    const DATA_TYPE_ALIAS_INVALID = 301;
    const DUPLICATED_DATA_TYPE = 302;
    const DATA_TYPE_EMPTY_ALIAS = 303;
    const DATA_TYPE_NOT_FOUND = 304;
    const DATA_TYPE_EMPTY_NAME = 305;
    const DATA_TYPE_STILL_HAVE_REFERENCE = 306;
    const CANNOT_DELETE_BUILTIN_DATA_TYPE = 307;
    const CANNOT_CHANGE_ALIAS_OF_BUILTIN_DATA_TYPE = 308;

    protected static $errorMessageMapping = [
        self::DATA_TYPE_ALIAS_INVALID                  => [ 'trans_key' => 'launchcms.cms_errors.data_type_alias_invalid', 'fallback' => 'Data type alias is invalid. You must name it as variable name and lowercase' ],
        self::DUPLICATED_DATA_TYPE                     => [ 'trans_key' => 'launchcms.cms_errors.duplicated_data_type', 'fallback' => 'Duplicated data type alias' ],
        self::DATA_TYPE_EMPTY_ALIAS                    => [ 'trans_key' => 'launchcms.cms_errors.data_type_alias_empty', 'fallback' => 'You cannot set empty alias for data type' ],
        self::DATA_TYPE_NOT_FOUND                      => [ 'trans_key' => 'launchcms.cms_errors.data_type_not_found', 'fallback' => 'Data type not found' ],
        self::DATA_TYPE_EMPTY_NAME                     => [ 'trans_key' => 'launchcms.cms_errors.data_type_empty_name', 'fallback' => 'You cannot set empty name for data type' ],
        self::FIELD_TYPE_CANNOT_BE_USED_FOR_DATA_TYPE  => [ 'trans_key' => 'launchcms.cms_errors.field_invalid_type', 'fallback' => 'This field is not supported for adding to the data type' ],
        self::DATA_TYPE_STILL_HAVE_REFERENCE           => [ 'trans_key' => 'launchcms.cms_errors.data_type_still_have_reference', 'fallback' => 'You cannot delete the data type which still has reference from other content types point to it' ],
        self::CANNOT_DELETE_BUILTIN_DATA_TYPE          => [ 'trans_key' => 'launchcms.cms_errors.cannot_delete_builtin_data_type', 'fallback' => 'You cannot delete builtin data types' ],
        self::CANNOT_CHANGE_ALIAS_OF_BUILTIN_DATA_TYPE => [ 'trans_key' => 'launchcms.cms_errors.cannot_change_alias_of_builtin_data_type', 'fallback' => 'You cannot change alias of builtin data types' ],

    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function invalidDataTypeAlias()
    {
        return new DataTypeException("Invalid data type alias", self::DATA_TYPE_ALIAS_INVALID);
    }

    public static function duplicatedDataTypeAlias()
    {
        return new DataTypeException("Data type alias exists", self::DUPLICATED_DATA_TYPE);
    }

    public static function cannotSetEmptyNameForDataType()
    {
        return new DataTypeException("Cannot set empty name for data type", self::DATA_TYPE_EMPTY_NAME);
    }

    public static function cannotSetEmptyAliasForDataType()
    {
        return new DataTypeException("Cannot set empty alias for data type", self::DATA_TYPE_EMPTY_ALIAS);
    }

    public static function dataTypeNotFound()
    {
        return new DataTypeException("Data type does not exist", self::DATA_TYPE_NOT_FOUND);
    }

    public static function invalidFieldType()
    {
        return new DataTypeException("Invalid field type", self::FIELD_TYPE_CANNOT_BE_USED_FOR_DATA_TYPE);
    }

    public static function dataTypeStillHaveReference()
    {
        return new DataTypeException("Data type still have reference", self::DATA_TYPE_STILL_HAVE_REFERENCE);
    }

    public static function cannotDeleteBuiltinDataType()
    {
        return new DataTypeException("Cannot delete builtin data type", self::CANNOT_DELETE_BUILTIN_DATA_TYPE);
    }

    public static function cannotChangeAliasOfBuiltinContentType()
    {
        return new DataTypeException("Cannot change alias of builtin data type", self::CANNOT_CHANGE_ALIAS_OF_BUILTIN_DATA_TYPE);
    }
}