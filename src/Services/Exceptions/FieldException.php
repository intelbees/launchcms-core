<?php

namespace LaunchCMS\Services\Exceptions;


class FieldException extends CMSServiceException
{

    const FIELD_NOT_FOUND = 400;
    const FIELD_ALIAS_EMPTY = 401;
    const INVALID_VALIDATION_RULE = 402;
    const FIELD_ALIAS_INVALID = 403;
    const DUPLICATED_FIELD = 404;
    const INVALID_FIELD_DATA_TYPE = 405;
    const INVALID_REFERENCE_CONTENT_TYPE = 406;
    const INVALID_REFERENCE_DATA_TYPE = 407;
    const CANNOT_MAKE_UNIQUE_INDEX = 408;
    const CANNOT_MAKE_INDEX = 409;
    const CANNOT_DROP_INDEX = 410;
    const INVALID_ARRAY_ELEMENT_TYPE = 411;

    protected static $errorMessageMapping = [
        self::DUPLICATED_FIELD               => [ 'trans_key' => 'launchcms.cms_errors.duplicated_field', 'fallback' => 'Duplicated field alias' ],
        self::FIELD_ALIAS_EMPTY              => [ 'trans_key' => 'launchcms.cms_errors.field_alias_empty', 'fallback' => 'You cannot empty alias for a field' ],
        self::FIELD_NOT_FOUND                => [ 'trans_key' => 'launchcms.cms_errors.field_not_found', 'fallback' => 'Field not found' ],
        self::INVALID_VALIDATION_RULE        => [ 'trans_key' => 'launchcms.cms_errors.invalid_validation_rule', 'fallback' => 'Invalid validation rule' ],
        self::FIELD_ALIAS_INVALID            => [ 'trans_key' => 'launchcms.cms_errors.field_alias_invalid', 'fallback' => 'Field alias is invalid. You must name it following variable name and lowercase' ],
        self::INVALID_FIELD_DATA_TYPE        => [ 'trans_key' => 'launchcms.cms_errors.invalid_field_data_type', 'fallback' => 'Invalid field data type' ],
        self::INVALID_REFERENCE_CONTENT_TYPE => [ 'trans_key' => 'launchcms.cms_errors.invalid_reference_content_type', 'fallback' => 'Invalid reference content type' ],
        self::INVALID_REFERENCE_DATA_TYPE    => [ 'trans_key' => 'launchcms.cms_errors.invalid_reference_data_type', 'fallback' => 'Invalid reference data type' ],
        self::CANNOT_MAKE_UNIQUE_INDEX       => [ 'trans_key' => 'launchcms.cms_errors.cannot_make_unique_index', 'fallback' => 'Cannot make unique index because data is violated unique constraints' ],
        self::CANNOT_MAKE_INDEX              => [ 'trans_key' => 'launchcms.cms_errors.cannot_make_index', 'fallback' => 'Cannot make index because of runtime exception. Please do this in the mongo console to see the detail error' ],
        self::CANNOT_DROP_INDEX              => [ 'trans_key' => 'launchcms.cms_errors.cannot_drop_index', 'fallback' => 'Cannot drop index because of runtime exception.  Please do this in the mongo console to see the detail error' ],
        self::INVALID_ARRAY_ELEMENT_TYPE     => [ 'trans_key' => 'launchcms.cms_errors.invalid_array_element_type', 'fallback' => 'Invalid array element type' ],

    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function cannotSetEmptyFieldAlias()
    {
        return new FieldException("Cannot set empty alias for field", self::FIELD_ALIAS_EMPTY);
    }

    public static function duplicatedField()
    {
        return new FieldException("Duplicated field alias", self::DUPLICATED_FIELD);
    }

    public static function fieldNotFound()
    {
        return new FieldException("Field does not exist", self::FIELD_NOT_FOUND);
    }

    public static function invalidValidationRule($rule)
    {
        return new FieldException("Invalid validation rule: " . $rule, self::INVALID_VALIDATION_RULE);
    }

    public static function invalidFieldAlias()
    {
        return new FieldException("Invalid field alias", self::FIELD_ALIAS_INVALID);
    }

    public static function invalidFieldDataType()
    {
        return new FieldException("Invalid field data type", self::INVALID_FIELD_DATA_TYPE);
    }

    public static function invalidReferenceContentType()
    {
        return new FieldException("Invalid reference content type", self::INVALID_REFERENCE_CONTENT_TYPE);
    }

    public static function invalidArrayElementType()
    {
        return new FieldException("Invalid array element type", self::INVALID_ARRAY_ELEMENT_TYPE);
    }

    public static function invalidReferenceDataType()
    {
        return new FieldException("Invalid reference data type", self::INVALID_REFERENCE_DATA_TYPE);
    }

    public static function cannotMakeUniqueIndex()
    {
        return new FieldException("Cannot make unique index because data is violate constraints", self::CANNOT_MAKE_UNIQUE_INDEX);
    }

    public static function cannotMakeIndex()
    {
        return new FieldException("Cannot make index because of runtime exception", self::CANNOT_MAKE_INDEX);
    }

    public static function cannotDropIndex()
    {
        return new FieldException("Cannot drop index because of runtime exception", self::CANNOT_DROP_INDEX);
    }

}