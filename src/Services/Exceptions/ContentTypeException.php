<?php

namespace LaunchCMS\Services\Exceptions;


class ContentTypeException extends CMSServiceException
{
    const DUPLICATED_CONTENT_TYPE = 200;
    const CONTENT_TYPE_NOT_FOUND = 201;
    const CONTENT_TYPE_EMPTY_ALIAS = 202;
    const CONTENT_TYPE_WITH_EMPTY_NAME = 203;
    const INVALID_INHERIT_CONTENT_TYPE = 204;
    const CANNOT_DELETE_PARENT_CONTENT_TYPE = 205;
    const CONTENT_TYPE_ALIAS_INVALID = 206;
    const CONTENT_TYPE_PRIMARY_LOCALE_REQUIRED = 207;
    const CANNOT_DELETE_BUILTIN_CONTENT_TYPE = 208;
    const CANNOT_CHANGE_ALIAS_OF_BUILTIN_CONTENT_TYPE = 209;

    protected static $errorMessageMapping = [
        self::CONTENT_TYPE_EMPTY_ALIAS                    => [ 'trans_key' => 'launchcms.cms_errors.content_type_empty_alias', 'fallback' => 'You cannot set empty alias for a content type' ],
        self::CONTENT_TYPE_WITH_EMPTY_NAME                => [ 'trans_key' => 'launchcms.cms_errors.content_type_empty_name', 'fallback' => 'You cannot set empty name for a content type' ],
        self::INVALID_INHERIT_CONTENT_TYPE                => [ 'trans_key' => 'launchcms.cms_errors.invalid_inherit_content_type', 'fallback' => 'The inherit content type is not valid. It may be removed' ],
        self::CANNOT_DELETE_PARENT_CONTENT_TYPE           => [ 'trans_key' => 'launchcms.cms_errors.delete_inherit_content_type', 'fallback' => 'You cannot delete a content type that is inherited by other content types. Please delete inherited content types firstly' ],
        self::CONTENT_TYPE_NOT_FOUND                      => [ 'trans_key' => 'launchcms.cms_errors.content_type_not_found', 'fallback' => 'Content type not found' ],
        self::CONTENT_TYPE_ALIAS_INVALID                  => [ 'trans_key' => 'launchcms.cms_errors.content_type_alias_invalid', 'fallback' => 'Content type alias is invalid. It must be named as variable name and lowercase' ],
        self::CONTENT_TYPE_PRIMARY_LOCALE_REQUIRED        => [ 'trans_key' => 'launchcms.cms_errors.primary_locale_required_when_localization_enabled', 'fallback' => 'You must select the primary locale if enable the localization for content type' ],
        self::CANNOT_DELETE_BUILTIN_CONTENT_TYPE          => [ 'trans_key' => 'launchcms.cms_errors.cannot_delete_builtin_content_type', 'fallback' => 'You cannot delete builtin content type' ],
        self::CANNOT_CHANGE_ALIAS_OF_BUILTIN_CONTENT_TYPE => [ 'trans_key' => 'launchcms.cms_errors.cannot_change_alias_of_builtin_content_type', 'fallback' => 'You cannot change alias of builtin content types' ],
    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function duplicatedContentTypeAlias()
    {
        return new ContentTypeException("Content type alias exists", self::DUPLICATED_CONTENT_TYPE);
    }

    public static function contentTypeNotFound()
    {
        return new ContentTypeException("Content type does not exist", self::CONTENT_TYPE_NOT_FOUND);
    }

    public static function cannotDeleteInheritedContentType()
    {
        return new ContentTypeException("Cannot delete content type which is inherited by other content types", self::CANNOT_DELETE_PARENT_CONTENT_TYPE);
    }

    public static function cannotSetEmptyNameForContentType()
    {
        return new ContentTypeException("Cannot set empty name for content type", self::CONTENT_TYPE_WITH_EMPTY_NAME);
    }

    public static function cannotSetEmptyAliasForContentType()
    {
        return new ContentTypeException("Cannot set empty alias for content type", self::CONTENT_TYPE_EMPTY_ALIAS);
    }

    public static function invalidContentTypeAlias()
    {
        return new ContentTypeException("Invalid content type alias", self::CONTENT_TYPE_ALIAS_INVALID);
    }

    public static function invalidInheritContentType()
    {
        return new ContentTypeException("Invalid inherit content type", self::INVALID_INHERIT_CONTENT_TYPE);
    }

    public static function requiredPrimaryLocale()
    {
        return new ContentTypeException("Primary locale is required when localization is enabled", self::CONTENT_TYPE_PRIMARY_LOCALE_REQUIRED);
    }

    public static function cannotDeleteBuiltinContentType()
    {
        return new ContentTypeException("Cannot delete builtin content type", self::CANNOT_DELETE_BUILTIN_CONTENT_TYPE);
    }

    public static function cannotChangeAliasOfBuiltinContentType()
    {
        return new ContentTypeException("Cannot change alias of builtin content type", self::CANNOT_CHANGE_ALIAS_OF_BUILTIN_CONTENT_TYPE);
    }
}