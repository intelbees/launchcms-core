<?php

namespace LaunchCMS\Services\Exceptions;

use Illuminate\Support\MessageBag;

abstract class CMSServiceException extends \Exception
{

    protected $_errors;

    protected abstract function getErrorMessageMapping();

    public function getTranslatedMessage()
    {
        $errorMessageMapping = $this->getErrorMessageMapping();
        if (isset( $errorMessageMapping[ $this->code ] )) {
            $message = trans($errorMessageMapping[ $this->code ][ 'trans_key' ]);
            if ($message === $errorMessageMapping[ $this->code ][ 'trans_key' ]) {
                return $errorMessageMapping[ $this->code ][ 'fallback' ];
            }

            return $message;
        }

        return $this->message;
    }

    public function __construct($message, $code, Exception $previous = null, $errors = null)
    {
        $this->_set_errors($errors);
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    protected function _set_errors($errors)
    {
        if (is_string($errors)) {
            $errors = [
                'error' => $errors,
            ];
        }

        if (is_array($errors)) {
            $errors = new MessageBag($errors);
        }

        $this->_errors = $errors;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

}