<?php

namespace LaunchCMS\Services\Exceptions;


class FieldGroupException extends CMSServiceException
{

    const DUPLICATED_FIELD_GROUP = 500;
    const FIELD_GROUP_ALIAS_EMPTY = 501;
    const FIELD_GROUP_NOT_FOUND = 502;
    const FIELD_GROUP_ALIAS_INVALID = 503;

    protected static $errorMessageMapping = [
        self::DUPLICATED_FIELD_GROUP    => [ 'trans_key' => 'launchcms.cms_errors.duplicated_group', 'fallback' => 'Duplicated field group alias' ],
        self::FIELD_GROUP_ALIAS_EMPTY   => [ 'trans_key' => 'launchcms.cms_errors.field_group_alias_empty', 'fallback' => 'You cannot set empty alias for field group' ],
        self::FIELD_GROUP_NOT_FOUND     => [ 'trans_key' => 'launchcms.cms_errors.field_group_not_found', 'fallback' => 'Field group not found' ],
        self::FIELD_GROUP_ALIAS_INVALID => [ 'trans_key' => 'launchcms.cms_errors.field_group_alias_invalid', 'fallback' => 'Field group alias is invalid. You must name it following variable name style and lower case' ],
    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function cannotSetEmptyFieldGroupAlias()
    {
        return new FieldGroupException("Cannot set empty alias for field group", self::FIELD_GROUP_ALIAS_EMPTY);
    }

    public static function fieldGroupNotFound()
    {
        return new FieldGroupException("Field group does not exist", self::FIELD_GROUP_NOT_FOUND);
    }

    public static function duplicatedFieldGroupAlias()
    {
        return new FieldGroupException("Field group alias exists", self::DUPLICATED_FIELD_GROUP);
    }

    public static function invalidFieldGroupAlias()
    {
        return new FieldGroupException("Invalid field group alias", self::FIELD_GROUP_ALIAS_INVALID);
    }


}