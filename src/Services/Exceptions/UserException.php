<?php

namespace LaunchCMS\Services\Exceptions;


class UserException extends CMSServiceException
{
    const RELATIONSHIP_NOT_FOUND = 601;
    const USER_CONNECTION_REQUEST_NOT_FOUND = 602;
    const USER_EMAIL_IS_INVALID = 603;
    const USER_PASSWORD_IS_INVALID = 604;
    const CANNOT_SET_EMPTY_ALIAS_FOR_RELATIONSHIP_DEF = 605;
    const INVALID_RELATIONSHIP_DEF_ALIAS = 606;
    const DUPLICATED_RELATIONSHIP_DEF_ALIAS = 607;

    protected static $errorMessageMapping = [
        self::RELATIONSHIP_NOT_FOUND            => [ 'trans_key' => 'launchcms.cms_errors.relationship_not_found', 'fallback' => 'Relationship not found' ],
        self::USER_CONNECTION_REQUEST_NOT_FOUND => [ 'trans_key' => 'launchcms.cms_errors.connection_request_not_found', 'fallback' => 'Connection request not found' ],
        self::USER_EMAIL_IS_INVALID             => [ 'trans_key' => 'launchcms.cms_errors.user_email_is_invalid', 'fallback' => 'User email is invalid' ],
        self::USER_PASSWORD_IS_INVALID          => [ 'trans_key' => 'launchcms.cms_errors.user_password_is_invalid', 'fallback' => 'User password is invalid' ],

        self::CANNOT_SET_EMPTY_ALIAS_FOR_RELATIONSHIP_DEF => [ 'trans_key' => 'launchcms.cms_errors.cannot_set_empty_alias_for_relationship_definition', 'fallback' => 'You cannot set empty alias for a relationship definition' ],
        self::INVALID_RELATIONSHIP_DEF_ALIAS              => [ 'trans_key' => 'launchcms.cms_errors.invalid_relationship_definition_alias', 'fallback' => 'Invalid relationship definition alias' ],
        self::DUPLICATED_RELATIONSHIP_DEF_ALIAS           => [ 'trans_key' => 'launchcms.cms_errors.duplicated_relationship_definition_alias', 'fallback' => 'Duplicated relationship definition alias' ],
    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function relationshipNotFound()
    {
        return new UserException("Relationship not found", self::RELATIONSHIP_NOT_FOUND);
    }

    public static function connectionRequestNotFound()
    {
        return new UserException("Connection request not found", self::USER_CONNECTION_REQUEST_NOT_FOUND);
    }

    public static function invalidEmail()
    {
        return new UserException("User email is invalid", self::USER_EMAIL_IS_INVALID);
    }

    public static function invalidPassword()
    {
        return new UserException("User password is invalid", self::USER_PASSWORD_IS_INVALID);
    }

    public static function cannotSetEmptyAliasForRelationshipDef()
    {
        return new UserException("Cannot set empty alias for relationship def", self::CANNOT_SET_EMPTY_ALIAS_FOR_RELATIONSHIP_DEF);
    }

    public static function invalidRelationshipDefAlias()
    {
        return new UserException("Invalid relationship definition alias", self::INVALID_RELATIONSHIP_DEF_ALIAS);
    }

    public static function duplicateRelationshipDefAlias()
    {
        return new UserException("Duplicated relationship definition alias", self::DUPLICATED_RELATIONSHIP_DEF_ALIAS);
    }
}