<?php

namespace LaunchCMS\Services\Exceptions;


class OrganizationException extends CMSServiceException
{

    const INVALID_SITE_ALIAS = 600;
    const INVALID_SITE_NAME = 601;
    const DUPLICATED_SITE_ALIAS = 602;
    const SITE_NOT_FOUND = 603;
    const INVALID_GROUP_ALIAS = 604;
    const INVALID_GROUP_NAME = 605;
    const DUPLICATED_GROUP_ALIAS = 606;
    const GROUP_NOT_FOUND = 607;
    protected static $errorMessageMapping = [
        self::INVALID_SITE_ALIAS     => [ 'trans_key' => 'launchcms.cms_errors.invalid_site_alias', 'fallback' => 'Invalid site alias. You must name it following variable naming style and lowercase.' ],
        self::INVALID_SITE_NAME      => [ 'trans_key' => 'launchcms.cms_errors.invalid_site_name', 'fallback' => 'You must set name for the site' ],
        self::DUPLICATED_SITE_ALIAS  => [ 'trans_key' => 'launchcms.cms_errors.duplicated_site_alias', 'fallback' => 'Duplicated site alias' ],
        self::SITE_NOT_FOUND         => [ 'trans_key' => 'launchcms.cms_errors.site_not_found', 'fallback' => 'Site not found' ],
        self::INVALID_GROUP_ALIAS    => [ 'trans_key' => 'launchcms.cms_errors.invalid_group_alias', 'fallback' => 'Invalid group alias. You must name it following variable naming style and lowercase.' ],
        self::INVALID_GROUP_NAME     => [ 'trans_key' => 'launchcms.cms_errors.invalid_group_name', 'fallback' => 'Invalid group name. You cannot set empty group name for a group' ],
        self::DUPLICATED_GROUP_ALIAS => [ 'trans_key' => 'launchcms.cms_errors.duplicated_group_alias', 'fallback' => 'Duplicated group alias' ],
        self::GROUP_NOT_FOUND        => [ 'trans_key' => 'launchcms.cms_errors.group_not_found', 'fallback' => 'Group not found' ],
    ];

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }

    public static function invalidSiteAlias()
    {
        return new OrganizationException("Invalid site alias", self::INVALID_SITE_ALIAS);
    }

    public static function invalidSiteName()
    {
        return new OrganizationException("Invalid site alias", self::INVALID_SITE_NAME);
    }

    public static function duplicatedSiteAlias()
    {
        return new OrganizationException("Duplicated site alias", self::DUPLICATED_SITE_ALIAS);
    }

    public static function siteNotFound()
    {
        return new OrganizationException("Site not found", self::SITE_NOT_FOUND);
    }

    public static function invalidGroupAlias()
    {
        return new OrganizationException("Invalid group alias", self::INVALID_GROUP_ALIAS);
    }

    public static function invalidGroupName()
    {
        return new OrganizationException("Invalid group name", self::INVALID_GROUP_NAME);
    }

    public static function duplicatedGroupAlias()
    {
        return new OrganizationException("Duplicated group alias", self::DUPLICATED_GROUP_ALIAS);
    }

    public static function groupNotFound()
    {
        return new OrganizationException("Group not found", self::GROUP_NOT_FOUND);
    }

}