<?php

namespace LaunchCMS\Services\Exceptions;


class ContentException extends CMSServiceException
{

    const CONTENT_EMPTY_NAME = 100;
    const INVALID_CONTENT = 101;
    const CONTENT_NOT_FOUND = 102;
    protected static $errorMessageMapping = [
        self::CONTENT_EMPTY_NAME => [ 'trans_key' => 'launchcms.cms_errors.content_empty_name', 'fallback' => 'You must set name for the content' ],
        self::INVALID_CONTENT    => [ 'trans_key' => 'launchcms.cms_errors.invalid_content', 'fallback' => 'Data of content is invalid. Please check errors for detail' ],
        self::CONTENT_NOT_FOUND  => [ 'trans_key' => 'launchcms.cms_errors.content_not_found', 'fallback' => 'Content not found' ],
    ];

    public static function cannotSetEmptyNameForContent()
    {
        return new ContentException("Cannot set empty name for content", self::CONTENT_EMPTY_NAME);
    }

    public static function contentNotFound()
    {
        return new ContentException("Content not found", self::CONTENT_NOT_FOUND);
    }

    public static function invalidContent($errors)
    {
        return new ContentException("Invalid content", self::INVALID_CONTENT, null, $errors);
    }

    protected function getErrorMessageMapping()
    {
        return self::$errorMessageMapping;
    }
}