<?php

namespace LaunchCMS\Services;


use Carbon\Carbon;
use Dflydev\ApacheMimeTypes\PhpRepository;

use Illuminate\Contracts\Filesystem\Filesystem;
use LaunchCMS\Services\Exceptions\MediaException;
use LaunchCMS\Services\Interfaces\MediaServiceInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class MediaServiceImp - Implement all features related to file uploading
 * @package LaunchCMS\Services
 */
class MediaServiceImp implements MediaServiceInterface
{
    /** @var  Filesystem */
    protected $disk;
    protected $mimeDetect;
    protected $webPath;

    public function setWebPath($webPath)
    {
        $this->webPath = rtrim($webPath, '/');
    }

    public function __construct(PhpRepository $mimeDetect)
    {
        $this->disk = Storage::disk();
        $this->mimeDetect = $mimeDetect;
    }


    /**
     * Return files and directories within a folder
     *
     * @param string $folder
     * @return array of [
     *    'folder' => 'path to current folder',
     *    'folderName' => 'name of just current folder',
     *    'breadCrumbs' => breadcrumb array of [ $path => $foldername ]
     *    'folders' => array of [ $path => $foldername] of each subfolder
     *    'files' => array of file details on each file in folder
     * ]
     */
    public function folderInfo($folder)
    {
        $folder = $this->cleanFolder($folder);

        $breadcrumbs = $this->breadcrumbs($folder);
        $slice = array_slice($breadcrumbs, -1);
        $folderName = current($slice)['name'];
        $breadcrumbs = array_slice($breadcrumbs, 0, -1);

        $subfolders = [ ];
        foreach (array_unique($this->disk->directories($folder)) as $subfolder) {
            $subfolders[] = ['path' => "/".$subfolder, 'name' => basename($subfolder)] ;
        }

        $files = [ ];
        foreach ($this->disk->files($folder) as $path) {
            $files[] = $this->fileDetails($path);
        }

        return [ 'folder'      => $folder,
                 'folderName'  => $folderName,
                 'breadcrumbs' => $breadcrumbs,
                 'subfolders'  => $subfolders,
                 'files'       => $files ];
    }

    /**
     * Sanitize the folder name
     * @param $folder
     * @return string
     */
    protected function cleanFolder($folder)
    {
        return '/' . trim(str_replace('..', '', $folder), '/');
    }


    /**
     * Return breadcrumbs to current folder
     */
    protected function breadcrumbs($folder)
    {
        $folder = trim($folder, '/');
        $crumbs = [ ];
        $crumbs [] = ['path' => '/', 'name' => 'root' ];
        if (empty( $folder )) {
            return $crumbs;
        }

        $folders = explode('/', $folder);
        $build = '';
        foreach ($folders as $folder) {
            $build .= '/' . $folder;
            $crumbs[] = ['path' => $build, 'name' => $folder];
        }

        return $crumbs;
    }

    /**
     * Return an array of file details for a file
     */
    protected function fileDetails($path)
    {
        $path = '/' . ltrim($path, '/');

        return [
            'name'     => basename($path),
            'fullPath' => $path,
            'webPath'  => $this->fileWebPath($path),
            'mimeType' => $this->fileMimeType($path),
            'size'     => $this->fileSize($path),
            'modified' => $this->fileModified($path),
        ];
    }

    /**
     * Return the full web path to a file
     */
    public function fileWebPath($path)
    {
        $path = $this->webPath . '/' .
            ltrim($path, '/');

        return url($path);
    }

    /**
     * Return the mime type
     */
    public function fileMimeType($path)
    {
        return $this->mimeDetect->findType(
            pathinfo($path, PATHINFO_EXTENSION)
        );
    }

    /**
     * Return the file size
     */
    public function fileSize($path)
    {
        return $this->disk->size($path);
    }

    /**
     * Return the last modified time
     */
    public function fileModified($path)
    {
        return Carbon::createFromTimestamp(
            $this->disk->lastModified($path)
        );
    }

    /**
     * Create a directory with the full folder path
     * @param $folder
     * @return bool
     * @throws MediaException
     */
    public function createDirectory($folder)
    {
        $folder = $this->cleanFolder($folder);

        if ($this->disk->exists($folder)) {
            throw MediaException::folderAlreadyExist();
        }

        return $this->disk->makeDirectory($folder);
    }



    /**
     * delete a directory
     * @param $folder
     * @return bool
     * @throws MediaException
     */
    public function deleteDirectory($folder)
    {
        $folder = $this->cleanFolder($folder);
        if ( !$this->disk->exists($folder)) {
            throw MediaException::folderDoesNotExist();
        }

        return $this->disk->deleteDirectory($folder);
    }

    /**
     * Delete a file
     * @param $path
     * @return bool
     * @throws MediaException
     */
    public function deleteFile($path)
    {
        $path = $this->cleanFolder($path);

        if ( !$this->disk->exists($path)) {
            throw MediaException::fileDoesNotExist();
        }

        return $this->disk->delete($path);
    }


    /**
     * Save content to a path
     * @param $path
     * @param $content
     * @return bool
     */
    public function saveFile($path, $content)
    {
        $path = $this->cleanFolder($path);
        return $this->disk->put($path, $content);
    }

    /**
     * Read file content from a path
     * @param $path
     * @return string
     */
    public function getFileContent($path) {
        $path = $this->cleanFolder($path);
        return $this->disk->get($path);
    }

}