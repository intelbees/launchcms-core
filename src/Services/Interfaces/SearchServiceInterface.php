<?php

namespace LaunchCMS\Services\Interfaces;


use LaunchCMS\Models\Content\DataObject\Content;
use ONGR\ElasticsearchDSL\BuilderInterface;
use ONGR\ElasticsearchDSL\Search;

interface SearchServiceInterface
{
    function createSimpleIndex($indexName);
    function deleteIndex($indexName);
    function indexContent(Content $content);
    function deleteDocument($contentTypeID, $contentID);
    function deleteDocumentWithContentTypeAlias($contentTypeAlias, $contentID);
    function search(Search $search);
    function searchInContentType($contentTypeAlias, BuilderInterface $searchQuery);
}