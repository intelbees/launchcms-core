<?php

namespace LaunchCMS\Services\Interfaces;


use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\FieldGroup;

interface StructureServiceInterface
{
    

    /**
     * Get all content types of the system
     * @param string $orderBy order field, default is 'name'
     * @param string $direction direction for ordering, default is 'asc'
     * @return mixed
     */
    function getAllContentTypes($orderBy = 'name', $direction='asc');


    /**
     * Create content type
     * @param array $info
     * @return mixed
     */
    function createContentType(array $info);

    /**
     * Update a content type
     * @param $alias
     * @param array $info
     * @return ContentType
     */
    function updateContentTypeInfo($alias, array $info);

    /**
     * Get content type by alias
     * @param $contentTypeAlias
     * @return ContentType
     */
    function getContentTypeByAlias($contentTypeAlias);

    /**
     * Get content type by ID
     * @param $id
     * @return ContentType
     */
    function getContentTypeByID($id);

    /**
     * Delete content type by id
     * @param $id
     * @param bool $deleteChildren true - if you want to delete inherited children content types. Default is false.
     * @return
     */
    function deleteContentTypeByID($id,  $deleteChildren = false);

    /**
     * Delete content type by alias
     * @param $alias
     * @param bool $deleteChildren true - if you want to delete inherited children content types. Default is false.
     * @return
     */
    function deleteContentTypeByAlias($alias, $deleteChildren = false);

    /**
     * Add field group to content type
     * @param $contentTypeAlias
     * @param FieldGroup $fieldGroup
     * @return mixed
     */
    function addFieldGroup($contentTypeAlias,FieldGroup $fieldGroup);
    function updateFieldGroup($contentTypeAlias, $fieldGroupAlias, FieldGroup $fieldGroup);
    function deleteFieldGroup($contentTypeAlias, $fieldGroupAlias);
    function addField($contentTypeAlias, Field $field);
    function updateField($contentTypeAlias, $fieldAlias, Field $field);
    function deleteField($contentTypeAlias, $fieldAlias, $removeInSchema = false);
    function getContentTypeFromCacheByID($contentTypeID);
    function getContentTypeFromCacheByAlias($alias);

    function getAllDataTypes($orderBy = 'name', $direction='asc');
    function getDataTypeByAlias($alias);
    function getDataTypeByID($id);
    function createDataType(array $info);
    function updateDataTypeInfo($alias, array $info);
    function deleteDataTypeByID($id);
    function deleteDataTypeByAlias($alias);
    function addFieldToDataType($dataTypeAlias, Field $field);
    function updateFieldOfDataType($dataTypeAlias, $fieldAlias, Field $field);
    function deleteFieldOfDataType($dataTypeAlias, $fieldAlias);
    function getDataTypeFromCacheByID($dataTypeID);
    function getDataTypeFromCacheByAlias($alias);
    function addWorkflow(array $workflowInfo) ;
    function deleteWorkflowByID($id);
    function deleteWorkflowByAlias($alias);
    function getWorkflowByID($id);
    function updateWorkflow($alias, array $workflowInfo);
    function getAllWorkflows($orderBy = 'name', $direction='asc');
    function getWorkflowEngineFromContentType(ContentType $contentType);
}