<?php

namespace LaunchCMS\Services\Interfaces;


interface MediaServiceInterface
{
    function folderInfo($folder);
    function fileWebPath($path);
    function fileMimeType($path);
    function fileSize($path);
    function fileModified($path);
    function createDirectory($folder);
    function deleteDirectory($folder);
    function deleteFile($path);
    function saveFile($path, $content);
    function getFileContent($path);

}