<?php

namespace LaunchCMS\Services\Interfaces;


interface UserServiceInterface
{
    function createRelationship($relationship, $requestUser, $targetUser);
    function removeRelationship($relationship,  $requestUser, $targetUser);
    function approveConnectionRequest($request);
    function rejectConnectionRequest($request);
    function findConnectionRequestById($connectionRequestID);
    function createRelationshipDef(array $info);
    function updateRelationshipDef($alias, array $info);
    function deleteRelationshipDefByAlias($alias);
    function deleteRelationshipDefByID($id);
    function getAllRelationshipDefinitions($orderBy = 'name', $direction='asc');
    function getRelationshipDefinitionByAlias($alias);
    function getRelationshipDefinitionByID($id);
}