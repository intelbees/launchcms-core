<?php

namespace LaunchCMS\Services\Interfaces;


interface WorkflowObjectInterface
{
    function getWorkflowObjectIdentifer();
    /**
     * Return a string value for the workflow status
     * @return mixed
     */
    function getWorkflowStatus();

    /**
     * Set the new state to the Workflow object
     * @param $state
     * @return mixed
     */
    function setWorkflowStatus($state);

    /**
     * Get the mapping system status of the current workflow state inside the Workflow object
     * @return mixed
     */
    function getMappedSystemStatus();

    /**
     * Set the mapping system state to the workflow object
     * @param $systemState
     * @return mixed
     */
    function setMappedSystemStatus($systemState);

    function getWorkflowData();

    function setWorkflowData($workflowData);

}