<?php

namespace LaunchCMS\Services\Interfaces;


interface PermissionServiceInterface
{
    function registerPermission($permission, $name, $description = '');
    function removePermission($permission);
    function checkPermissionExist($permission);
    function getAllPermissions();

}