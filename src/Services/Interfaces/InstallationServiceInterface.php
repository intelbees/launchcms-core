<?php

namespace LaunchCMS\Services\Interfaces;


interface InstallationServiceInterface
{
    function install();
}