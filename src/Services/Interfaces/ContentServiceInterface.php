<?php

namespace LaunchCMS\Services\Interfaces;

use Jenssegers\Mongodb\Query\Builder;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\User\User;

interface ContentServiceInterface
{

    /**
     * Delete a content by ID. Do nothing if content not found
     * @param $contentTypeAlias Content type alias
     * @param $contentID Content ID
     * @return mixed
     */
    function deleteContentByID($contentTypeAlias, $contentID);

    /**
     * Move a content to trash. Do nothing if content not found
     * @param $contentTypeAlias Content type alias
     * @param $contentID Content ID
     * @return mixed
     */
    function moveToTrash($contentTypeAlias, $contentID);

    /**
     * Get QueryBuilder object from the content type alias
     * @param $contentTypeAlias Content type alias
     * @return mixed
     */
    function getQueryBuilder($contentTypeAlias);

    /**
     * This function creates a draft content - which does not validate any field constraint inside.
     * The content status can be checked by workflow_status field
     * @param $contentTypeAlias
     * @param $name
     * @return mixed
     */
    function createDraftContent($contentTypeAlias, $name);

    /**
     * Save a content. Thrown CMSServiceException in below cases:
     *
     * - Content type not found
     * - Field values violates validation rules
     * - Field value does not match data type of the field
     * @param string $contentTypeAlias String value of content type alias
     * @param Content $content
     * @param bool $saveWithNewVersion
     * @return mixed
     */
    function saveContent($contentTypeAlias, Content $content, $saveWithNewVersion = false);

    function addVisibleCheckingToQuery(Builder $query, User $user = null);

    function queryVisibleContent(Builder $query, User $user = null);

    function changeContentWorkflowStatus($contentTypeAlias, $contentID, $nextState, array $workflowData = null);


}