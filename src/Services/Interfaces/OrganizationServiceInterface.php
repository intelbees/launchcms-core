<?php

namespace LaunchCMS\Services\Interfaces;


interface OrganizationServiceInterface
{
    function createSite(array $siteInfo);
    function updateSite($alias, array $siteInfo);
    function deleteSiteByAlias($alias);
    function deleteSiteByID($id);
}