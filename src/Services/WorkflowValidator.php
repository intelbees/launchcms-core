<?php

namespace LaunchCMS\Services;


use LaunchCMS\Models\Content\WorkflowSystemStatus;

class WorkflowValidator
{

    const INVALID_JSON_FORMAT = 1;
    const MISSING_STATES_SECTION = 2;
    const MISSING_TRANSITIONS_SECTION = 3;
    const INVALID_STATE_DATA = 4;
    const INVALID_MAPPED_SYSTEM_STATE = 6;
    const INVALID_TRANSITION_DATA = 7;
    const TRANSITION_POINT_TO_NON_EXIST_STATE = 8;
    const DUPLICATED_STATE = 9;
    protected static $reservedAlias = [ WorkflowEngine::NIL_STATE ];
    protected $workflowStates = [ ];
    protected $workflowTransition = [ ];
    protected $errorCode = 0;
    protected $errorMessageMapping = [
        self::INVALID_JSON_FORMAT                 => [ 'trans_key' => 'launchcms.workflow_errors.invalid_json_format', 'fallback' => 'Invalid JSON format' ],
        self::MISSING_STATES_SECTION              => [ 'trans_key' => 'launchcms.workflow_errors.missing_states_section', 'fallback' => 'Missing states section' ],
        self::MISSING_TRANSITIONS_SECTION         => [ 'trans_key' => 'launchcms.workflow_errors.missing_transitions_section', 'fallback' => 'Missing transitions section' ],
        self::INVALID_STATE_DATA                  => [ 'trans_key' => 'launchcms.workflow_errors.invalid_state_data', 'fallback' => 'Invalid state data' ],
        self::INVALID_MAPPED_SYSTEM_STATE         => [ 'trans_key' => 'launchcms.workflow_errors.invalid_mapped_system_state', 'fallback' => 'Invalid mapped system state' ],
        self::INVALID_TRANSITION_DATA             => [ 'trans_key' => 'launchcms.workflow_errors.invalid_transition_data', 'fallback' => 'Invalid transition data' ],
        self::TRANSITION_POINT_TO_NON_EXIST_STATE => [ 'trans_key' => 'launchcms.workflow_errors.transition_point_to_non_exist_state', 'fallback' => 'Transition points to non exist state' ],
        self::DUPLICATED_STATE                    => [ 'trans_key' => 'launchcms.workflow_errors.duplicated_state', 'fallback' => 'Duplicated state' ],

    ];

    public function getTranslatedErrorMessage()
    {
        $message = trans($this->errorMessageMapping[ $this->errorCode ][ 'trans_key' ]);
        if ($message === $this->errorMessageMapping[ $this->errorCode ][ 'trans_key' ]) {
            return $this->errorMessageMapping[ $this->errorCode ][ 'fallback' ];
        }

        return $message;
    }

    public function validateWorkflowJSON($jsonString)
    {
        $json = json_decode($jsonString, true);
        if ($json === null) {
            $this->errorCode = self::INVALID_JSON_FORMAT;

            return false;
        }

        if ( !isset( $json[ WorkflowEngine::SECTION_STATES ] ) || empty( $json[ WorkflowEngine::SECTION_STATES ] )) {
            $this->errorCode = self::MISSING_STATES_SECTION;

            return false;
        }

        $states = $json[ WorkflowEngine::SECTION_STATES ];
        $validated = $this->validateWorkflowStates($states);
        if ( !$validated) {
            return false;
        }

        if ( !isset( $json[ WorkflowEngine::SECTION_TRANSITIONS ] ) || empty( $json[ WorkflowEngine::SECTION_TRANSITIONS ] )) {
            $this->errorCode = self::MISSING_TRANSITIONS_SECTION;

            return false;
        }

        $transitions = $json[ WorkflowEngine::SECTION_TRANSITIONS ];
        $validated = $this->validateTransitions($transitions);
        if ( !$validated) {
            return false;
        }

        return true;
    }


    protected function validateTransitions(array $transitions)
    {
        foreach ($transitions as $transition) {
            if ( !isset( $transition[ WorkflowEngine::TRANSITION_FROM ] ) || !isset( $transition[ WorkflowEngine::TRANSITION_TO ] )) {
                $this->errorCode = self::INVALID_TRANSITION_DATA;

                return false;
            }
            if ( !isset( $transition[ 'name' ] ) && !isset( $transition[ 'trans_key' ] )) {
                $this->errorCode = self::INVALID_TRANSITION_DATA;

                return false;
            }
            $from = $transition[ WorkflowEngine::TRANSITION_FROM ];
            if ( !isset( $this->workflowStates[ $from ] ) && !in_array($from, self::$reservedAlias)) {
                $this->errorCode = self::TRANSITION_POINT_TO_NON_EXIST_STATE;

                return false;
            }

            $to = $transition[ WorkflowEngine::TRANSITION_TO ];
            if ( !isset( $this->workflowStates[ $to ] ) && !in_array($to, self::$reservedAlias)) {
                $this->errorCode = self::TRANSITION_POINT_TO_NON_EXIST_STATE;

                return false;
            }
        }

        return true;
    }

    protected function validateWorkflowStates(array $states)
    {
        foreach ($states as $state) {
            if ( !isset( $state[ 'alias' ] )) {
                $this->errorCode = self::INVALID_STATE_DATA;

                return false;
            }

            if ( !isset( $state[ WorkflowEngine::MAPPED_SYSTEM_STATE ] )) {
                $this->errorCode = self::INVALID_STATE_DATA;

                return false;
            }
            $mapSystemState = $state[ WorkflowEngine::MAPPED_SYSTEM_STATE ];
            if ( !WorkflowSystemStatus::isSystemStatus($mapSystemState)) {
                $this->errorCode = self::INVALID_MAPPED_SYSTEM_STATE;

                return false;
            }

            if ( !isset( $state[ 'name' ] ) && !isset( $state[ 'trans_key' ] )) {
                $this->errorCode = self::INVALID_STATE_DATA;

                return false;
            }
            $stateAlias = $state[ 'alias' ];
            if (empty( $stateAlias ) || in_array($stateAlias, self::$reservedAlias)) {
                $this->errorCode = self::INVALID_STATE_DATA;

                return false;
            }
            if (isset( $this->workflowStates[ $stateAlias ] )) {
                $this->errorCode = self::DUPLICATED_STATE;

                return false;
            }
            $this->workflowStates[ $stateAlias ] = $state;
        }

        return true;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }
}