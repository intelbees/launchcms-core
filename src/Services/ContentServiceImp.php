<?php

namespace LaunchCMS\Services;


use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Mongodb\Query\Builder;
use LaunchCMS\Events\ContentAfterDeleting;
use LaunchCMS\Events\ContentAfterMovingToTrash;
use LaunchCMS\Events\ContentAfterSaving;
use LaunchCMS\Events\ContentBeforeDeleting;
use LaunchCMS\Events\ContentBeforeMovingToTrash;
use LaunchCMS\Events\ContentBeforeSaving;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\ContentVersion;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\Helper\FieldDataTypeValidator;
use LaunchCMS\Models\Content\Helper\FieldFactory;
use LaunchCMS\Models\Content\WorkflowSystemStatus;
use LaunchCMS\Models\User\User;
use LaunchCMS\MongoDB\MongoHelper;
use LaunchCMS\Services\Exceptions\ContentException;
use LaunchCMS\Services\Exceptions\ContentTypeException;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Interfaces\ContentServiceInterface;
use LaunchCMS\Utils\DateUtil;


class ContentServiceImp implements ContentServiceInterface
{
    const DEV_FIELDS = 'dev_fields';

    private function createNewContentVersion($oldContent)
    {
        $contentVersion = ContentVersion::fromContentData($oldContent);
        $contentVersion->save();

        return $contentVersion->getVersionNumber();
    }

    protected function validateContent(ContentType $contentType, Content &$content)
    {
        if (empty( $content->name )) {
            throw ContentException::cannotSetEmptyNameForContent();
        }
        //validate through data type of field value
        foreach ($content->attributes as $fieldDataAlias => $fieldData) {
            $field = $contentType->getField($fieldDataAlias);
            $isNotUserDefinedField = empty( $field );
            $isNotBuiltInField = !Content::isBuiltInField($fieldDataAlias);
            $isInDevFields = isset( $contentType->getDevSettings()[ ContentType::DEV_FIELDS ] ) &&
                in_array($fieldDataAlias, $contentType->getDevSettings()[ ContentType::DEV_FIELDS ]);
            if ($isInDevFields) {
                continue;
            }

            if ($isNotUserDefinedField && $isNotBuiltInField) {
                throw FieldException::fieldNotFound();
            }

            $concreteField = FieldFactory::getConcreteField($field);
            $errors = [ ];
            $validated = $concreteField->validateValue($fieldData, $errors);
            if ( !$validated) {
                throw ContentException::invalidContent($errors);
            }
            $concreteField->processFieldDataBeforeSaving($content, $fieldDataAlias, $fieldData);
        }
        $validation = Validator::make($content->attributes, $contentType->getFieldValidationRules());
        if ($validation->fails()) {
            throw ContentException::invalidContent($validation->messages());
        }

        return true;
    }

    public function createDraftContent($contentTypeAlias, $name)
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $content = new Content();
        $content->name = $name;
        $content->setWorkflowStatus(WorkflowSystemStatus::DRAFT);
        $content->contentTypeID = $contentType->_id;
        $content->contentTypePath = $contentType->getInheritPath();
        $now = DateUtil::fromDateTime(new Carbon);
        $content->createdTime = $now;
        $content->lastUpdatedTime = $now;
        Event::fire(new ContentBeforeSaving($content));
        $data = $content->toMongoObjectArray();
        $collectionName = $contentType->rootContentType()->getAlias();
        $content->id = (string) MongoHelper::getMongoDB()->collection($collectionName)->insertGetId($data);
        Event::fire(new ContentAfterSaving($content));
        $id = $content->id;

        return (string) $id;
    }

    protected function processContentWorkflowState(Content &$content, ContentType $contentType, $oldState, $newState)
    {
        if ($oldState === $newState) {
            return;
        }
        /** @var WorkflowEngine $workflowEngine */
        $workflowEngine = StructureService::getWorkflowEngineFromContentType($contentType);
        $content->setWorkflowStatus($oldState);
        $workflowEngine->changeState($content, $newState);
    }

    public function saveContent($contentTypeAlias, Content $content, $saveWithNewVersion = false)
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }

        $collectionName = $contentType->rootContentType()->getAlias();
        $content->contentTypeID = $contentType->_id;
        $content->contentTypePath = $contentType->getInheritPath();
        $this->validateContent($contentType, $content);
        $now = DateUtil::fromDateTime(new Carbon);
        $oldContentState = null;
        $newContentState = null;
        if (empty( $content->id )) {
            $content->createdTime = $now;
            $content->lastUpdatedTime = $now;
        } else {
            $content->lastUpdatedTime = $now;
            $oldContentData = MongoHelper::getMongoDB()->collection($collectionName)->where('_id', $content->id)->first();
            $oldContent = Content::fromArray($oldContentData, $contentType);
            $oldContentState = $oldContent->getWorkflowStatus();
            $newContentState = $content->getWorkflowStatus();
            if ($saveWithNewVersion) {
                $newVersionNumber = $this->createNewContentVersion($oldContent);
                $content->lastVersionNumber = $newVersionNumber;
            }
        }
        $this->processContentWorkflowState($content, $contentType, $oldContentState, $newContentState);
        Event::fire(new ContentBeforeSaving($content));
        $data = $content->toMongoObjectArray();
        if (empty( $content->id )) {
            $content->id = (string) MongoHelper::getMongoDB()->collection($collectionName)->insertGetId($data);
        } else {
            MongoHelper::getMongoDB()->collection($collectionName)->where('_id', $content->id)->update($data);
        }
        Event::fire(new ContentAfterSaving($content));
        $id = $content->id;

        return (string) $id;
    }

    public function changeContentWorkflowStatus($contentTypeAlias, $contentID, $nextState, array $workflowData = null)
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        /** @var Content $content */
        $content = $this->getContentByID($contentTypeAlias, $contentID);
        if (empty( $content )) {
            throw ContentException::contentNotFound();
        }
        $now = DateUtil::fromDateTime(new Carbon);
        $content->lastUpdatedTime = $now;
        /** @var WorkflowEngine $workflowEngine */
        $workflowEngine = StructureService::getWorkflowEngineFromContentType($contentType);
        $workflowEngine->changeState($content, $nextState, $workflowData);
        $data = $content->toMongoObjectArray();
        $collectionName = $contentType->rootContentType()->getAlias();
        MongoHelper::getMongoDB()->collection($collectionName)->where('_id', $content->id)->update($data);
        Event::fire(new ContentAfterSaving($content));

        return $content;
    }

    public function deleteContentByID($contentTypeAlias, $contentID)
    {
        $queryBuilder = $this->getQueryBuilder($contentTypeAlias);
        $contentData = $queryBuilder->where('_id', $contentID)->first();
        $content = Content::fromArray($contentData);
        Event::fire(new ContentBeforeDeleting($content));
        $queryBuilder->where('_id', $contentID)->delete();
        MongoHelper::getMongoDB()->collection('cms_content_versions')->where('content_id', $contentID)->delete();
        Event::fire(new ContentAfterDeleting($content));
    }

    public function moveToTrash($contentTypeAlias, $contentID)
    {
        $queryBuilder = $this->getQueryBuilder($contentTypeAlias);
        $contentData = $queryBuilder->where('_id', $contentID)->first();
        $content = Content::fromArray($contentData);
        Event::fire(new ContentBeforeMovingToTrash($content));
        $queryBuilder = $this->getQueryBuilder($contentTypeAlias);
        $queryBuilder->where('_id', $contentID)->update([ 'trashed' => true ]);
        Event::fire(new ContentAfterMovingToTrash($content));
    }

    public function getQueryBuilder($contentTypeAlias)
    {
        $contentType = StructureService::getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $collectionName = $contentType->rootContentType()->getAlias();

        return MongoHelper::getMongoDB()->collection($collectionName);
    }

    public function addVisibleCheckingToQuery(Builder $query, User $user = null)
    {
        return $query->where(function(Builder $query) use ($user) {
            $query->where('visibility', Content::VISIBILITY_ANYONE);
            if ( !empty( $user )) {
                $query->orWhere(function(Builder $query) use ($user) {
                    $query->where('visibility', Content::VISIBILITY_SPECIFIC_USERS)
                        ->where('visible_users', $user->_id);
                });

                $query->orWhere(function(Builder $query) use ($user) {
                    $query->where('visibility', Content::VISIBILITY_SPECIFIC_GROUPS)
                        ->whereIn('visible_users', $user->getJoinedGroupIDs());
                });

                $query->orWhere(function(Builder $query) use ($user) {
                    $query->where('visibility', Content::VISIBILITY_SPECIFIC_ROLES)
                        ->whereIn('visible_roles', $user->getAssignedRoleIDs());
                });
            }
        });
    }

    public function queryVisibleContent(Builder $query, User $user = null)
    {
        return $this->addVisibleCheckingToQuery($query, $user)->get();
    }

    public function getContentByID($contentTypeAlias, $id)
    {
        $contentQueryBuilder = $this->getQueryBuilder($contentTypeAlias);
        $contentData = $contentQueryBuilder->where('_id', $id)->first();
        if (empty( $contentData )) {
            return null;
        }

        return Content::fromArray($contentData);
    }
}