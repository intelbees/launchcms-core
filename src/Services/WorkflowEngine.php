<?php
namespace LaunchCMS\Services;


use LaunchCMS\Models\Content\Workflow\WorkflowUserInterface;
use LaunchCMS\Models\Content\WorkflowSystemStatus;
use LaunchCMS\Services\Exceptions\WorkflowException;
use LaunchCMS\Services\Interfaces\WorkflowObjectInterface;

class WorkflowEngine
{
    const SCHEDULED_TIME = 'scheduled_time';
    const SECTION_STATES = 'states';
    const SECTION_TRANSITIONS = 'transitions';
    const MAPPED_SYSTEM_STATE = 'mapped_system_state';
    const TRANSITION_FROM = 'from';
    const TRANSITION_TO = 'to';
    const RESPONSIBLE_USERS = 'responsible_users';
    const RESPONSIBLE_ROLES = 'responsible_roles';
    const NIL_STATE = 'nil';
    public static $defaultWorkflowDefinition = [
        'states'      => [
            [ 'trans_key' => 'launchcms.default_workflow.states.draft', 'alias' => 'draft', 'mapped_system_state' => WorkflowSystemStatus::DRAFT ],
            [ 'trans_key' => 'launchcms.default_workflow.states.published', 'alias' => 'published', 'mapped_system_state' => WorkflowSystemStatus::PUBLISHED ],
            [ 'trans_key' => 'launchcms.default_workflow.states.scheduled_publish', 'alias' => 'scheduled_publish', 'mapped_system_state' => WorkflowSystemStatus::SCHEDULED_PUBLISH ],
        ],
        'transitions' => [
            [ 'trans_key' => 'launchcms.default_workflow.transitions.send_to_draft', 'from' => 'nil', 'to' => 'draft' ],
            [ 'trans_key' => 'launchcms.default_workflow.transitions.send_to_publish', 'from' => 'draft', 'to' => 'published' ],
            [ 'trans_key' => 'launchcms.default_workflow.transitions.schedule_to_publish', 'from' => 'draft', 'to' => 'scheduled_publish' ],
            [ 'trans_key' => 'launchcms.default_workflow.transitions.send_back_to_draft', 'from' => 'published', 'to' => 'draft' ],
            [ 'trans_key' => 'launchcms.default_workflow.transitions.send_back_to_draft', 'from' => 'scheduled_publish', 'to' => 'draft' ],
        ],
    ];

    protected $transitions;
    protected $states;

    protected function buildTransitionKey($from, $to)
    {
        return $from . '-' . $to;
    }

    public function loadWorkflow($workflowJSON = null)
    {
        if ($workflowJSON == null) {
            $workflowJSON = json_encode(self::$defaultWorkflowDefinition);
        }
        $workflowValidator = new WorkflowValidator();
        if ( !$workflowValidator->validateWorkflowJSON($workflowJSON)) {
            $detailMessage = $workflowValidator->getTranslatedErrorMessage();
            throw WorkflowException::invalidWorkflowJSON($detailMessage);
        }
        $workflowData = json_decode($workflowJSON, true);
        $stateData = $workflowData[ self::SECTION_STATES ];
        $this->states = [ ];
        foreach ($stateData as $state) {
            $stateAlias = $state[ 'alias' ];
            $this->states[ $stateAlias ] = $state;
        }

        $transitionData = $workflowData[ self::SECTION_TRANSITIONS ];
        $this->transitions = [ ];
        foreach ($transitionData as $transition) {
            //we create this mapping to do the faster and clean lookup for transition later.
            $key = $this->buildTransitionKey($transition[ self::TRANSITION_FROM ], $transition[ self::TRANSITION_TO ]);
            $this->transitions[ $key ] = $transition;
        }
    }

    public function changeState(WorkflowObjectInterface &$workflowObject, $newState, array $workflowData = null)
    {
        if (empty( $newState )) {
            throw WorkflowException::invalidWorkflowState();
        }
        if ( !isset( $this->states[ $newState ] )) {
            throw WorkflowException::invalidWorkflowState();
        }
        $currentState = $workflowObject->getWorkflowStatus();
        if (empty( $currentState )) {
            $currentState = self::NIL_STATE;
        }
        $keyToLookUpTransition = $this->buildTransitionKey($currentState, $newState);
        if ( !isset( $this->transitions[ $keyToLookUpTransition ] )) {
            throw WorkflowException::noTriggerRuleForTheNewState($newState);
        }
        $state = $this->states[ $newState ];
        $workflowObject->setMappedSystemStatus($state[ self::MAPPED_SYSTEM_STATE ]);
        $workflowObject->setWorkflowStatus($newState);
        if ($state[ self::MAPPED_SYSTEM_STATE ] === WorkflowSystemStatus::SCHEDULED_PUBLISH) {
            if (empty( $workflowData ) || !isset( $workflowData[ self::SCHEDULED_TIME ] )) {
                throw WorkflowException::scheduleTimeRequired();
            }

        }
        $workflowObject->setWorkflowData($workflowData);
    }

    public function getNextTransitions(WorkflowObjectInterface $workflowObject, WorkflowUserInterface $user = null)
    {
        $currentState = $workflowObject->getWorkflowStatus();

        return $this->getNextTransitionsFromState($currentState, $user);
    }

    public function getNextTransitionsFromState($currentState, WorkflowUserInterface $user = null)
    {
        if (empty( $currentState )) {
            $currentState = self::NIL_STATE;
        }
        $outputTransitions = [ ];
        foreach ($this->transitions as $transition) {
            if ($transition[ self::TRANSITION_FROM ] !== $currentState) {
                continue;
            }
            $allowTransitionToAdd = true;
            if(isset($transition[self::RESPONSIBLE_ROLES]) && !empty($transition[self::RESPONSIBLE_ROLES]) && !empty($user)) {
                $responsibleRoles = $transition[self::RESPONSIBLE_ROLES];
                $matchedRoles = array_intersect($responsibleRoles, $user->getRoleSlugs());
                $allowTransitionToAdd = count($matchedRoles) > 0;
            }
            if(isset($transition[self::RESPONSIBLE_USERS]) && !empty($transition[self::RESPONSIBLE_USERS]) && !empty($user)) {
                $responsibleUsers = $transition[self::RESPONSIBLE_USERS];
                $allowTransitionToAdd = in_array($user->getEmail(), $responsibleUsers);
            }
            if($allowTransitionToAdd) {
                $outputTransitions[] = $transition;
            }

        }

        return $outputTransitions;
    }

    public function getState($stateAlias)
    {
        return isset( $this->states[ $stateAlias ] ) ? $this->states[ $stateAlias ] : null;
    }

    public function getTransition($from, $to)
    {
        $keyToLookupTransition = $this->buildTransitionKey($from, $to);

        return isset( $this->transitions[ $keyToLookupTransition ] ) ? $this->transitions[ $keyToLookupTransition ] : null;
    }

}