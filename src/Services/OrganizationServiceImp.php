<?php

namespace LaunchCMS\Services;


use LaunchCMS\Models\Organization\Group;
use LaunchCMS\Models\Organization\Site;
use LaunchCMS\Services\Exceptions\OrganizationException;
use LaunchCMS\Services\Interfaces\OrganizationServiceInterface;
use LaunchCMS\Utils\StringUtil;

class OrganizationServiceImp implements OrganizationServiceInterface
{
    public function createSite(array $siteInfo)
    {
        if ( !isset( $siteInfo[ 'alias' ] ) || empty( $siteInfo[ 'alias' ] )) {
            throw OrganizationException::invalidSiteAlias();
        }
        if ( !StringUtil::isValidVariableName($siteInfo[ 'alias' ])) {
            throw OrganizationException::invalidSiteAlias();
        }
        if ( !isset( $siteInfo[ 'name' ] ) || empty( $siteInfo[ 'name' ] )) {
            throw OrganizationException::invalidSiteName();
        }
        $existingSiteWithSameAlias = Site::where('alias', $siteInfo[ 'alias' ])->first();

        if ( !empty( $existingSiteWithSameAlias )) {
            throw OrganizationException::duplicatedSiteAlias();
        }

        return Site::create($siteInfo);
    }

    public function updateSite($alias, array $siteInfo)
    {
        if (empty( $alias )) {
            throw OrganizationException::invalidSiteAlias();
        }
        /** @var Site $site */
        $site = Site::where('alias', $alias)->first();
        if (empty( $site )) {
            throw OrganizationException::siteNotFound();
        }
        if (isset( $siteInfo[ 'alias' ] ) && $siteInfo[ 'alias' ] !== $alias) {
            $siteWithSameAlias = Site::where('alias', $siteInfo[ 'alias' ])->first();
            if ( !empty( $siteWithSameAlias )) {
                throw OrganizationException::duplicatedSiteAlias();
            }
        }
        $site->updateFromArray($siteInfo);
        $site->save();
    }

    public function deleteSiteByAlias($alias)
    {
        /** @var Site $site */
        $site = Site::where('alias', $alias)->first();
        if (empty( $site )) {
            throw OrganizationException::siteNotFound();
        }
        $site->delete();
    }


    public function deleteSiteByID($id)
    {
        /** @var Site $site */
        $site = Site::where('_id', $id)->first();
        if (empty( $site )) {
            throw OrganizationException::siteNotFound();
        }
        $site->delete();
    }

    public function getSiteByID($id)
    {
        return Site::where('_id', $id)->first();
    }

    public function getSiteByAlias($alias)
    {
        return Site::where('alias', $alias)->first();
    }

    public function getAllSites()
    {
        return Site::all();
    }

    public function createGroup(array $groupInfo)
    {
        if ( !isset( $groupInfo[ 'alias' ] ) || empty( $groupInfo[ 'alias' ] )) {
            throw OrganizationException::invalidGroupAlias();
        }
        if ( !StringUtil::isValidVariableName($groupInfo[ 'alias' ])) {
            throw OrganizationException::invalidGroupAlias();
        }
        if ( !isset( $groupInfo[ 'name' ] ) || empty( $groupInfo[ 'name' ] )) {
            throw OrganizationException::invalidGroupName();
        }
        $existingSiteWithSameAlias = Site::where('alias', $groupInfo[ 'alias' ])->first();

        if ( !empty( $existingSiteWithSameAlias )) {
            throw OrganizationException::duplicatedGroupAlias();
        }

        return Group::create($groupInfo);
    }

    public function updateGroup($alias, array $groupInfo)
    {
        if (empty( $alias )) {
            throw OrganizationException::invalidGroupAlias();
        }
        /** @var Group $group */
        $group = Group::where('alias', $alias)->first();
        if (empty( $group )) {
            throw OrganizationException::groupNotFound();
        }
        if (isset( $groupInfo[ 'alias' ] ) && $groupInfo[ 'alias' ] !== $alias) {
            $groupWithSameAlias = Group::where('alias', $groupInfo[ 'alias' ])->first();
            if ( !empty( $groupWithSameAlias )) {
                throw OrganizationException::duplicatedGroupAlias();
            }
        }
        $group->updateFromArray($groupInfo);
        $group->save();
    }

    public function deleteGroupByAlias($alias)
    {
        $group = Group::where('alias', $alias)->first();
        if (empty( $group )) {
            throw OrganizationException::groupNotFound();
        }
        $group->delete();
    }

    public function getGroupByAlias($alias)
    {
        return Group::where('alias', $alias)->first();
    }

    public function getGroupByID($id)
    {
        return Group::where('_id', $id)->first();
    }

    public function getAllGroups()
    {
        return Group::all();
    }


}