<?php
namespace LaunchCMS\Services;

use LaunchCMS\Services\Interfaces\PermissionServiceInterface;

class PermissionServiceImp implements PermissionServiceInterface
{
    /**
     * This is the array contains all permissions of development module, but not related to content
     * @var array
     */
    protected static $PERMISSIONS = [ ];
    /**
     * This is the array contains all permissions for creating the ACL
     * @var array
     */
    protected static $CONTENT_PERMISSIONS = [ ];

    public function registerPermission($permission, $name, $description = '')
    {
        if (isset( self::$PERMISSIONS[ $permission ] )) {
            return;
        }
        self::$PERMISSIONS[ $permission ] = [ 'alias' => $permission, 'name' => $name, 'description' => $description ];
    }

    public function registerContentPermission($permission, $name, $description = '')
    {
        if (isset( self::$CONTENT_PERMISSIONS[ $permission ] )) {
            return;
        }
        self::$CONTENT_PERMISSIONS[ $permission ] = [ 'alias' => $permission, 'name' => $name, 'description' => $description ];
    }

    public function removePermission($permission)
    {
        if ( !isset( self::$PERMISSIONS[ $permission ] )) {
            return;
        }
        unset( self::$PERMISSIONS[ $permission ] );
    }

    public function removeContentPermission($permission)
    {
        if ( !isset( self::$CONTENT_PERMISSIONS[ $permission ] )) {
            return;
        }
        unset( self::$CONTENT_PERMISSIONS[ $permission ] );
    }

    public function checkPermissionExist($permission)
    {
        return isset( self::$PERMISSIONS[ $permission ] ) && !empty( self::$PERMISSIONS[ $permission ] );
    }

    public function checkContentPermissionExist($permission)
    {
        return isset( self::$CONTENT_PERMISSIONS[ $permission ] ) && !empty( self::$CONTENT_PERMISSIONS[ $permission ] );
    }

    public function getAllPermissions()
    {
        return self::$PERMISSIONS;
    }

    public function getAllContentPermissions()
    {
        return self::$CONTENT_PERMISSIONS;
    }


}