<?php

namespace LaunchCMS\Services;


use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Event;
use LaunchCMS\Events\UserWasAdded;
use LaunchCMS\Events\UserWasDeleted;
use LaunchCMS\Events\UserWasUpdated;
use LaunchCMS\Models\User\RelationshipDefinition;
use LaunchCMS\Models\User\Role;
use LaunchCMS\Models\User\User;
use LaunchCMS\Models\User\UserConnectionRequest;
use LaunchCMS\MongoDB\MongoHelper;
use LaunchCMS\Services\Exceptions\UserException;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Interfaces\UserServiceInterface;
use LaunchCMS\Utils\StringUtil;

class UserServiceImp implements UserServiceInterface
{
    protected function getUserObjectFromValue($value)
    {
        $result = null;

        if ($value instanceof User) {
            $result = $value;
        } else {
            $result = User::where('_id', $value)->first();
        }

        return $result;
    }

    protected function getRelationshipDefinitionFromValue($value)
    {
        $relationshipObj = null;
        if ($value instanceof RelationshipDefinition) {
            $relationshipObj = $value;
        } else {
            $relationshipObj = RelationshipDefinition::where('alias', $value)->first();
        }

        return $relationshipObj;
    }

    public function createRelationship($relationship, $requestUser, $targetUser)
    {
        /** @var RelationshipDefinition $relationshipObj */
        $relationshipObj = $this->getRelationshipDefinitionFromValue($relationship);
        if (empty( $relationship )) {
            throw UserException::relationshipNotFound();
        }
        $requestUserObj = $this->getUserObjectFromValue($requestUser);
        $targetUserObj = $this->getUserObjectFromValue($targetUser);

        //in case relationship need approval, we simply just create the connection request
        if ($relationshipObj->needApproval()) {

            $existingConnectionRequest = UserConnectionRequest::where('request_user_id', $requestUserObj->_id)
                ->where('target_user_id', $targetUserObj->_id)
                ->where('relationship_id', $relationshipObj->_id)->first();
            if (empty( $existingConnectionRequest )) {
                $connectionRequest = new UserConnectionRequest();
                $connectionRequest->request_user_id = $requestUserObj->_id;
                $connectionRequest->target_user_id = $targetUserObj->_id;
                $connectionRequest->relationship_id = $relationshipObj->_id;
                $connectionRequest->save();
            }

            return;
        }

        $requestUserObj->addToRelationship($relationshipObj->getAlias(), $targetUserObj);
        if ($relationshipObj->inverse()) {
            $targetUserObj->addToRelationship($relationshipObj->getAlias(), $requestUserObj);
        }

    }

    public function removeRelationship($relationship, $requestUser, $targetUser)
    {
        /** @var RelationshipDefinition $relationshipObj */
        $relationshipObj = $this->getRelationshipDefinitionFromValue($relationship);
        if (empty( $relationship )) {
            throw UserException::relationshipNotFound();
        }
        $requestUserObj = $this->getUserObjectFromValue($requestUser);
        $targetUserObj = $this->getUserObjectFromValue($targetUser);
        $requestUserObj->removeFromRelationship($relationshipObj->getAlias(), $targetUserObj);
        if ($relationshipObj->inverse()) {
            $targetUserObj->removeFromRelationship($relationshipObj->getAlias(), $requestUserObj);
        }
    }

    public function approveConnectionRequest($request)
    {
        /** @var UserConnectionRequest $connectionRequest */
        $connectionRequest = null;
        if ($request instanceof UserConnectionRequest) {
            $connectionRequest = $request;
        } else {
            $connectionRequest = UserConnectionRequest::where('_id', $request)->first();
        }
        if (empty( $connectionRequest )) {
            throw UserException::connectionRequestNotFound();
        }
        /** @var User $targetUser */
        $targetUser = $connectionRequest->targetUser;
        /** @var User $requestUser */
        $requestUser = $connectionRequest->requestUser;
        /** @var RelationshipDefinition $relationship */
        $relationship = $connectionRequest->relationshipDefinition;
        $requestUser->addToRelationship($relationship->getAlias(), $targetUser);
        if ($relationship->inverse()) {
            $targetUser->addToRelationship($relationship->getAlias(), $requestUser);
        }
        $connectionRequest->delete();
    }

    public function rejectConnectionRequest($request)
    {
        /** @var UserConnectionRequest $connectionRequest */
        $connectionRequest = null;
        if ($request instanceof UserConnectionRequest) {
            $connectionRequest = $request;
        } else {
            $connectionRequest = UserConnectionRequest::where('_id', $request)->first();
        }
        $connectionRequest->delete();
    }

    public function findConnectionRequestById($connectionRequestID)
    {
        return UserConnectionRequest::where('_id', $connectionRequestID)->first();
    }

    public function getUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function setUserPassword($userID, $password)
    {
        $user = User::where('_id', $userID)->first();
        if (empty( $user )) {
            return;
        }
        $credentials = [
            'login'    => $user->email,
            'password' => $password,
        ];
        Sentinel::getUserRepository()->fill($user, $credentials);
        $user->save();
        Event::fire(new UserWasUpdated($user));
    }

    public function updateUser(User $user)
    {
        if (empty( $user )) {
            return;
        }
        $user->save();
        Event::fire(new UserWasUpdated($user));
    }

    public function createUser(User $user)
    {
        if (empty( $user )) {
            return;
        }
        if (empty( $user->email )) {
            throw UserException::invalidEmail();
        }
        $userContentType = StructureService::getContentTypeFromCacheByAlias('cms_users');
        $user->content_type_id = $userContentType->_id;
        $user->save();
        Event::fire(new UserWasAdded($user));

        return $user;
    }

    public function deleteUser(User $user)
    {
        $user->delete();
        Event::fire(new UserWasDeleted($user));
    }

    public function registerAndActivate(User $user)
    {
        if (empty( $user->email )) {
            throw UserException::invalidEmail();
        }
        if (empty( $user->password )) {
            throw UserException::invalidPassword();
        }
        $sentinelUser = Sentinel::registerAndActivate([
            'email'    => $user->email,
            'password' => $user->password,
        ]);
        User::copy($user, $sentinelUser, [ '_id', 'password' ]);
        $userContentType = StructureService::getContentTypeFromCacheByAlias('cms_users');
        if ( !empty( $userContentType )) {
            $sentinelUser->content_type_id = (string) $userContentType->_id;
        }
        $sentinelUser->save();

        return $sentinelUser;
    }

    public function createRole($roleName, $slug)
    {
        $role = Sentinel::getRoleRepository()->createModel()->create([
            'name' => $roleName,
            'slug' => $slug,
        ]);
        $roleContentType = StructureService::getContentTypeFromCacheByAlias('cms_roles');
        if ( !empty( $roleContentType )) {
            $role->content_type_id = (string) $roleContentType->_id;
            $role->save();
        }

        return $role;
    }

    public function getAllRoles()
    {
        return Role::all();
    }

    public function getRoleBySlug($slug)
    {
        return Role::where('slug', $slug)->first();
    }

    public function deleteRoleBySlug($slug)
    {
        $role = $this->getRoleBySlug($slug);
        if (empty( $role )) {
            return;
        }
        User::where('roles', $role->id)->pull($role->id);
        $allContentTypes = StructureService::getAllContentTypes();
        foreach ($allContentTypes as $contentType) {
            $collection = $contentType->rootContentType()->getAlias();
            MongoHelper::getMongoDB()->collection($collection)->where('visible_roles', $role->id)->pull($role->id);
        }
    }

    public function activateUser($user)
    {
        $activation = Activation::completed($user);
        if ($activation !== false) {
            return;
        }
        $activation = Activation::create($user);
        $activationCode = $activation->code;
        Activation::complete($user, $activationCode);
    }

    public function deactivateUser($user)
    {
        $activation = Activation::completed($user);
        if ($activation === false) {
            return;
        }
        Activation::remove($user);
    }

    public function isUserActivated($user)
    {
        $activation = Activation::completed($user);

        return ( $activation !== false );
    }

    public function createRelationshipDef(array $info)
    {
        if ( !isset( $info[ 'alias' ] )) {
            throw UserException::cannotSetEmptyAliasForRelationshipDef();
        }
        $alias = $info[ 'alias' ];

        if ( !empty( $alias )) {
            $alias = strtolower($alias);
            $alias = StringUtil::sanitize($alias, '_');
        }
        $relationshipDefWithSameAlias = RelationshipDefinition::where('alias', $alias)->first();
        if ( !empty( $relationshipDefWithSameAlias )) {
            throw UserException::duplicateRelationshipDefAlias();
        }
        $relationship = new RelationshipDefinition();
        $relationship->updateFromArray($info);
        $relationship->save();

        return $relationship;
    }

    public function updateRelationshipDef($alias, array $info)
    {
        /** @var RelationshipDefinition $existingRelationshipDef */
        $existingRelationshipDef = RelationshipDefinition::where('alias', $alias)->first();
        if (empty( $existingRelationshipDef )) {
            throw UserException::relationshipNotFound();
        }
        if (isset( $info[ 'alias' ] )) {
            $newAlias = $info[ 'alias' ];
            $relationshipDefWithSameNewAlias = RelationshipDefinition::where('alias', $newAlias)->first();
            if ( !empty( $relationshipDefWithSameNewAlias ) &&
                (string) $relationshipDefWithSameNewAlias->_id !== (string) $relationshipDefWithSameNewAlias->_id
            ) {
                throw UserException::duplicateRelationshipDefAlias();
            }
        }
        $existingRelationshipDef->updateFromArray($info);
        $existingRelationshipDef->save();
    }

    public function deleteRelationshipDefByAlias($alias)
    {
        /** @var RelationshipDefinition $existingRelationshipDef */
        $existingRelationshipDef = RelationshipDefinition::where('alias', $alias)->first();
        if (empty( $existingRelationshipDef )) {
            return;
        }
        $existingRelationshipDef->delete();
    }

    public function deleteRelationshipDefByID($id)
    {
        /** @var RelationshipDefinition $existingRelationshipDef */
        $existingRelationshipDef = RelationshipDefinition::where('_id', $id)->first();
        if (empty( $existingRelationshipDef )) {
            return;
        }
        $existingRelationshipDef->delete();
    }

    public function getRelationshipDefinitionByAlias($alias)
    {
        return RelationshipDefinition::where('alias', $alias)->first();
    }

    public function getRelationshipDefinitionByID($id)
    {
        return RelationshipDefinition::where('_id', $id)->first();
    }

    public function getAllRelationshipDefinitions($orderBy = 'name', $direction = 'asc')
    {
        return RelationshipDefinition::orderBy($orderBy, $direction)->get();
    }

}