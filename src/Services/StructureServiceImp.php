<?php

namespace LaunchCMS\Services;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use LaunchCMS\Events\ContentTypeWasCreated;
use LaunchCMS\Events\ContentTypeWasDeleted;
use LaunchCMS\Events\ContentTypeWasUpdated;
use LaunchCMS\Events\DataTypeWasCreated;
use LaunchCMS\Events\DataTypeWasDeleted;
use LaunchCMS\Events\DataTypeWasUpdated;
use LaunchCMS\Events\FieldGroupWasAdded;
use LaunchCMS\Events\FieldGroupWasDeleted;
use LaunchCMS\Events\FieldGroupWasUpdated;
use LaunchCMS\Events\FieldWasAdded;
use LaunchCMS\Events\FieldWasDeleted;
use LaunchCMS\Events\FieldWasUpdated;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\FieldGroup;
use LaunchCMS\Models\Content\Workflow;
use LaunchCMS\MongoDB\MongoHelper;
use LaunchCMS\RuntimeSetting;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Exceptions\ContentTypeException;
use LaunchCMS\Services\Exceptions\DataTypeException;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Services\Exceptions\FieldGroupException;
use LaunchCMS\Services\Exceptions\WorkflowException;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;
use LaunchCMS\Utils\StringUtil;

/**
 * Class StructureServiceImp - Implement all features related to content type
 * @package LaunchCMS\Services
 */
class StructureServiceImp implements StructureServiceInterface
{
    const DATA_TYPE_ID_LOOKUP_CACHE_KEY = 'data_type_id_lookup';
    const DATA_TYPE_ALIAS_LOOKUP_CACHE_KEY = 'data_type_alias_lookup';
    const CONTENT_TYPE_ID_LOOKUP_CACHE_KEY = 'content_type_id_lookup';
    const CONTENT_TYPE_ALIAS_LOOKUP_CACHE_KEY = 'content_type_alias_lookup';

    /**
     * Get data type by alias
     * @param $alias
     * @return mixed
     */
    public function getDataTypeByAlias($alias)
    {
        return DataType::where('alias', $alias)->first();
    }

    /**
     * Get data type by id
     * @param $id
     * @return mixed
     */
    public function getDataTypeByID($id)
    {
        return DataType::where('_id', $id)->first();
    }

    /**
     * Create a data type
     * @param array $info
     * @return DataType
     * @throws CMSServiceException
     */
    public function createDataType(array $info)
    {
        if ( !isset( $info[ 'alias' ] )) {
            throw DataTypeException::cannotSetEmptyAliasForDataType();
        }
        if (empty( $info[ 'alias' ] )) {
            throw DataTypeException::cannotSetEmptyAliasForDataType();
        }
        if ( !StringUtil::isValidVariableName($info[ 'alias' ])) {
            throw DataTypeException::invalidDataTypeAlias();
        }

        $dataType = DataType::fromArray($info);

        $existDataType = DataType::where('alias', $dataType->getAlias())->first();
        if ( !empty( $existDataType )) {
            throw DataTypeException::duplicatedDataTypeAlias();
        }
        $dataType->save();
        $this->buildDataTypeCache();
        Event::fire(new DataTypeWasCreated($dataType));

        return $dataType;
    }

    /**
     * Get all data types in the system
     * @param string $orderBy field for ordering, default is 'name'
     * @param string $direction direction of the order, default is 'asc'
     * @return mixed
     */
    public function getAllDataTypes($orderBy = 'name', $direction = 'asc')
    {
        return DataType::orderBy($orderBy, $direction)->get();
    }

    /**
     * Update content type information with array of data. Allowed fields to be updated: alias, name, description
     * @param $alias
     * @param array $info
     * @throws CMSServiceException
     */
    public function updateDataTypeInfo($alias, array $info)
    {

        if (isset( $info[ 'alias' ] )) {
            if (empty( $info[ 'alias' ] )) {
                throw DataTypeException::cannotSetEmptyAliasForDataType();
            }
            if ( !StringUtil::isValidVariableName($info[ 'alias' ])) {
                throw DataTypeException::invalidDataTypeAlias();
            }
        }
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeByAlias($alias);
        if (empty( $dataType )) {
            throw DataTypeException::dataTypeNotFound();
        }
        $dataType->updateFromArray($info);
        $newAlias = $dataType->getAlias();
        $name = $dataType->getName();
        if (empty( $name )) {
            throw DataTypeException::cannotSetEmptyNameForDataType();
        }

        if ($newAlias !== $alias) {
            $existDataType = DataType::where('alias', $newAlias)->first();
            if ( !empty( $existDataType ) && $existDataType->_id !== $dataType->_id) {
                throw DataTypeException::duplicatedDataTypeAlias();
            }
        }
        $dataType->save();
        $this->buildDataTypeCache();
        Event::fire(new DataTypeWasUpdated($dataType));

        return $dataType;
    }

    protected function dataTypeStillHaveReference($id)
    {
        $contentTypeRefs = $this->getAllContentTypeRelatedToDataType($id);

        return count($contentTypeRefs) > 0;
    }

    public function deleteDataTypeByID($id)
    {
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeByID($id);
        if (empty( $dataType )) {
            return;
        }
        if ($dataType->isBuiltInType()) {
            throw DataTypeException::cannotDeleteBuiltinDataType();
        }
        if ($this->dataTypeStillHaveReference($id)) {
            throw DataTypeException::dataTypeStillHaveReference();
        }
        $dataType->delete();
        $this->buildDataTypeCache();
        Event::fire(new DataTypeWasDeleted($dataType));
    }

    public function deleteDataTypeByAlias($alias)
    {
        $dataType = $this->getDataTypeByAlias($alias);
        if (empty( $dataType )) {
            return;
        }
        if ($dataType->isBuiltInType()) {
            throw DataTypeException::cannotDeleteBuiltinDataType();
        }
        $dataType->delete();
        $this->buildDataTypeCache();
        Event::fire(new DataTypeWasDeleted($dataType));
    }

    public function getAllContentTypeRelatedToDataType($dataTypeID)
    {
        return $contentTypeRefs = ContentType::where('fields.extra_data_type_info.data_type_id', $dataTypeID)->get();
    }

    protected function updateIndexingOfRelatedContentType(ContentType $contentType, Field $addedField, DataType $dataType)
    {
        $collectionName = $contentType->rootContentType()->getAlias();
        $relatedEmbeddedFields = $dataType->getAllRelatedEmbeddedFieldsInContentType($contentType);
        /** @var Field $embeddedField */
        foreach ($relatedEmbeddedFields as $embeddedField) {
            $nestedFieldAlias = $embeddedField->getAlias();
            $addedField->makeIndexing($collectionName, $nestedFieldAlias);
        }
    }

    /**
     * Add field to data type
     * @param $dataTypeAlias
     * @param Field $field
     * @throws CMSServiceException
     */
    public function addFieldToDataType($dataTypeAlias, Field $field)
    {
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeByAlias($dataTypeAlias);
        if (empty( $dataType )) {
            throw DataTypeException::dataTypeNotFound();
        }
        $dataType->addField($field);
        if ($field->index()) {
            $relatedContentTypes = $this->getAllContentTypeRelatedToDataType($dataType->_id);
            if ( !empty( $relatedContentTypes )) {
                /** @var ContentType $contentType */
                foreach ($relatedContentTypes as $contentType) {
                    $this->updateIndexingOfRelatedContentType($contentType, $field, $dataType);
                }
            }
        }

        $this->buildDataTypeCache();
        Event::fire(new DataTypeWasUpdated($dataType));
        Event::fire(new FieldWasAdded($dataType, $field));
    }


    public function updateFieldOfDataType($dataTypeAlias, $fieldAlias, Field $field)
    {
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeByAlias($dataTypeAlias);
        if (empty( $dataType )) {
            throw DataTypeException::dataTypeNotFound();
        }
        $existingField = $dataType->updateField($fieldAlias, $field);
        $this->buildDataTypeCache();
        Event::fire(new DataTypeWasUpdated($dataType));
        Event::fire(new FieldWasUpdated($dataType, $existingField));
    }


    public function deleteFieldOfDataType($dataTypeAlias, $fieldAlias)
    {
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeByAlias($dataTypeAlias);
        if (empty( $dataType )) {
            throw DataTypeException::dataTypeNotFound();
        }
        $foundField = $dataType->getField($fieldAlias);
        if (empty( $foundField )) {
            return;
        }
        $allRelatedContentTypes = $this->getAllContentTypeRelatedToDataType($dataType->_id);
        foreach ($allRelatedContentTypes as $contentType) {
            $allRelatedEmbeddedFields = $dataType->getAllRelatedEmbeddedFieldsInContentType($contentType);
            $collectionName = $contentType->rootContentType()->getAlias();
            foreach ($allRelatedEmbeddedFields as $embeddedField) {
                $nestedFieldAlias = $embeddedField->getAlias() . '.' . $foundField->getAlias();
                MongoHelper::getMongoDB()->collection($collectionName)->unset($nestedFieldAlias);
            }
        }
        $foundField->delete();

        $this->buildDataTypeCache();
        Event::fire(new DataTypeWasUpdated($dataType));
        Event::fire(new FieldWasDeleted($dataType, $foundField));
    }

    /**
     * Create content type and setup mongo collection
     * @param array $info
     * @return ContentType
     * @throws ContentTypeException
     */
    public function createContentType(array $info)
    {
        if (isset( $info[ 'alias' ] ) && !StringUtil::isValidVariableName($info[ 'alias' ])) {
            throw ContentTypeException::invalidContentTypeAlias();
        }
        $contentType = ContentType::fromArray($info);
        $inherit = null;
        if (isset( $info[ 'inherit' ] )) {
            $inherit = $info[ 'inherit' ];
        }
        $collectionName = $contentType->getAlias();
        if (RuntimeSetting::$runningMode === RuntimeSetting::LIVE &&
            ContentType::isBuiltInCollection($collectionName)
        ) {
            throw ContentTypeException::invalidContentTypeAlias();
        }
        $inheritContentType = null;
        if ( !empty( $inherit )) {
            $inheritContentType = $this->getContentTypeByID($inherit);
            if (empty( $inheritContentType )) {
                throw ContentTypeException::invalidInheritContentType();
            }
            $contentType->setParentID($inheritContentType->_id);
            $rootContentType = $inheritContentType->rootContentType();
            if ( !empty( $rootContentType )) {
                $collectionName = $rootContentType->getAlias();
            }
        }
        $existContentType = ContentType::where('alias', $contentType->getAlias())->first();
        if ( !empty( $existContentType )) {
            throw ContentTypeException::duplicatedContentTypeAlias();
        }
        $contentType->save();
        $path = $contentType->_id;
        if ( !empty( $inheritContentType )) {
            $inheritPathFromParent = $inheritContentType->getInheritPath();
            $path = sprintf("%s,%s", $inheritPathFromParent, $path);
        }
        $contentType->setInheritPath($path);
        $contentType->save();
        if (empty( $inheritContentType )) {
            MongoHelper::getMongoDBSchema()->create($collectionName, function($collection) {

            });
        }
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasCreated($contentType));

        return $contentType;
    }

    /**
     * Update content type information with array of data. Allowed fields to be updated: alias, name, description
     * @param $alias
     * @param array $info
     * @param bool $ghostMode
     * @return ContentType
     * @throws ContentTypeException
     */
    public function updateContentTypeInfo($alias, array $info)
    {

        if (isset( $info[ 'alias' ] )) {
            if (empty( $info[ 'alias' ] )) {
                throw ContentTypeException::cannotSetEmptyAliasForContentType();
            }
            if ( !StringUtil::isValidVariableName($info[ 'alias' ])) {
                throw ContentTypeException::invalidContentTypeAlias();
            }
        }
        /** @var ContentType $contentType */
        $contentType = $this->getContentTypeByAlias($alias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $contentType->updateFromArray($info);
        $newAlias = $contentType->getAlias();
        if ( !$contentType->isBuiltInType() && ContentType::isBuiltInCollection($newAlias)) {
            throw ContentTypeException::invalidContentTypeAlias();
        }
        $name = $contentType->getName();
        if (empty( $name )) {
            throw ContentTypeException::cannotSetEmptyNameForContentType();
        }
        $primaryLocale = $contentType->getPrimaryLocale();
        if ($contentType->getEnableLocalization() && empty( $primaryLocale )) {
            throw ContentTypeException::requiredPrimaryLocale();
        }
        if ($newAlias !== $alias) {
            $existContentType = ContentType::where('alias', $newAlias)->first();
            if ( !empty( $existContentType ) && $existContentType->_id !== $contentType->_id) {
                throw ContentTypeException::duplicatedContentTypeAlias();
            }
            if ($contentType->isRootContentType()) {
                MongoHelper::renameCollection($alias, $newAlias);
            }
        }
        $contentType->save();
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasUpdated($contentType));

        return $contentType;
    }

    /**
     * Get content type by alias
     * @param $contentTypeAlias
     * @return mixed
     */
    public function getContentTypeByAlias($contentTypeAlias)
    {
        return ContentType::where('alias', $contentTypeAlias)->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getContentTypeByID($id)
    {
        return ContentType::where('_id', $id)->first();
    }

    /**
     * Delete content type by ID
     * @param $id
     * @param bool $deleteChildren
     * @throws CMSServiceException
     */
    public function deleteContentTypeByID($id, $deleteChildren = false)
    {
        /** @var ContentType $contentType */
        $contentType = $this->getContentTypeByID($id);
        if (empty( $contentType )) {
            return;
        }
        if ($contentType->isBuiltInType()) {
            throw ContentTypeException::cannotDeleteBuiltinContentType();
        }
        $inheritChildren = $contentType->children()->get();

        if (count($inheritChildren) > 0 && !$deleteChildren) {
            throw ContentTypeException::cannotDeleteInheritedContentType();
        }
        if (count($inheritChildren) > 0 && $deleteChildren) {
            foreach ($inheritChildren as $childContent) {
                $this->deleteContentTypeByAlias($childContent->getAlias(), $deleteChildren);
            }
        }
        $contentType->delete();

        MongoHelper::getMongoDBSchema()->drop($contentType->getAlias(), function($collection) {

        });
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasDeleted($contentType));
    }

    /**
     * Delete content type by alias
     * @param $alias
     * @param bool $deleteChildren
     * @throws CMSServiceException
     */
    public function deleteContentTypeByAlias($alias, $deleteChildren = false)
    {
        $contentType = $this->getContentTypeByAlias($alias);
        if (empty( $contentType )) {
            return;
        }
        if ($contentType->isBuiltInType()) {
            throw ContentTypeException::cannotDeleteBuiltinContentType();
        }
        $inheritChildren = $contentType->children()->get();

        if (count($inheritChildren) > 0 && !$deleteChildren) {
            throw ContentTypeException::cannotDeleteInheritedContentType();
        }
        if (count($inheritChildren) > 0 && $deleteChildren) {
            foreach ($inheritChildren as $childContent) {
                $this->deleteContentTypeByAlias($childContent->getAlias(), $deleteChildren);
            }
        }
        $contentType->delete();
        $this->buildContentTypeCache();
        MongoHelper::getMongoDBSchema()->drop($contentType->getAlias(), function($collection) {

        });
        Event::fire(new ContentTypeWasDeleted($contentType));
    }

    /**
     * Add field group to content type
     * @param $contentTypeAlias
     * @param FieldGroup $fieldGroup
     * @throws CMSServiceException
     */
    public function addFieldGroup($contentTypeAlias, FieldGroup $fieldGroup)
    {
        $contentType = $this->getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $fieldGroupAlias = $fieldGroup->getAlias();
        if (empty( $fieldGroupAlias )) {
            throw FieldGroupException::cannotSetEmptyFieldGroupAlias();
        }
        if ($fieldGroup->getPosition() == -1) {
            $fieldGroup->setPosition($contentType->fieldGroups()->count());
        }
        if ($contentType->isGroupExist($fieldGroup->getAlias())) {
            throw FieldGroupException::duplicatedFieldGroupAlias();
        }
        $contentType->fieldGroups()->save($fieldGroup);
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasUpdated($contentType));
        Event::fire(new FieldGroupWasAdded($contentType, $fieldGroup));
    }

    public function updateFieldGroup($contentTypeAlias, $fieldGroupAlias, FieldGroup $fieldGroup)
    {
        $contentType = $this->getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }

        if (empty( $fieldGroupAlias )) {
            throw FieldGroupException::cannotSetEmptyFieldGroupAlias();
        }
        /** @var FieldGroup $existingFieldGroup */
        $existingFieldGroup = $contentType->getOwnFieldGroup($fieldGroupAlias);
        if (empty( $existingFieldGroup )) {
            throw FieldGroupException::fieldGroupNotFound();
        }
        $existingFieldGroup->updateFromFieldGroup($fieldGroup);
        if ( !StringUtil::isValidVariableName($existingFieldGroup->getAlias())) {
            throw FieldGroupException::invalidFieldGroupAlias();
        }
        if ($existingFieldGroup->getPosition() == -1) {
            $existingFieldGroup->setPosition($contentType->fieldGroups()->count());
        }
        $contentType->fieldGroups()->save($existingFieldGroup);
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasUpdated($contentType));
        Event::fire(new FieldGroupWasUpdated($contentType, $existingFieldGroup));
    }


    /**
     * Remove a field group from a content type
     * @param $contentTypeAlias
     * @param $fieldGroupAlias
     * @throws CMSServiceException
     */
    public function deleteFieldGroup($contentTypeAlias, $fieldGroupAlias)
    {
        $contentType = $this->getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $fieldGroup = $contentType->getFieldGroupByAlias($fieldGroupAlias);
        if (empty( ( $fieldGroup ) )) {
            return;
        }
        $contentType->fieldGroups()->destroy($fieldGroup);
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasUpdated($contentType));
        Event::fire(new FieldGroupWasDeleted($contentType, $fieldGroup));
    }


    /**
     * Add field to content type
     * @param $contentTypeAlias
     * @param Field $field
     * @throws ContentTypeException
     * @throws FieldGroupException
     */
    public function addField($contentTypeAlias, Field $field)
    {
        /** @var ContentType $contentType */
        $contentType = $this->getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $contentType->addField($field);
        $collectionName = $contentType->rootContentType()->getAlias();
        $field->makeIndexing($collectionName);

        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasUpdated($contentType));
        Event::fire(new FieldWasAdded($contentType, $field));
    }

    public function updateField($contentTypeAlias, $fieldAlias, Field $field)
    {
        /** @var ContentType $contentType */
        $contentType = $this->getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $existingField = $contentType->updateField($fieldAlias, $field);
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasUpdated($contentType));
        Event::fire(new FieldWasUpdated($contentType, $existingField));
    }


    /**
     * Remove a field from a content type
     * @param $contentTypeAlias
     * @param $fieldAlias
     * @param bool $removeInSchema (default = false) - if true, this will remove the column in the collection
     * @throws CMSServiceException
     */
    public function deleteField($contentTypeAlias, $fieldAlias, $removeInSchema = false)
    {
        $contentType = $this->getContentTypeByAlias($contentTypeAlias);
        if (empty( $contentType )) {
            throw ContentTypeException::contentTypeNotFound();
        }
        $foundField = $contentType->getField($fieldAlias);
        if (empty( $foundField )) {
            throw FieldException::fieldNotFound();
        }

        $foundField->delete();
        $collectionName = $contentType->getAlias();

        $inheritContentType = $contentType->parent;
        if ( !empty( $inheritContentType )) {
            $rootContentType = $inheritContentType->rootContentType();
            if ( !empty( $rootContentType )) {
                $collectionName = $rootContentType->getAlias();
            }
        }

        if ($removeInSchema) {
            MongoHelper::getMongoDB()->collection($collectionName)->unset($foundField->getAlias());
        }
        $this->buildContentTypeCache();
        Event::fire(new ContentTypeWasUpdated($contentType));
        Event::fire(new FieldWasDeleted($contentType, $foundField));
    }

    public function getAllContentTypes($orderBy = 'name', $direction = 'asc')
    {
        return ContentType::orderBy($orderBy, $direction)->get();
    }


    protected function buildContentTypeCache()
    {
        $allContentTypes = ContentType::all();
        $contentTypeIDLookup = [ ];
        $contentTypeAliasLookup = [ ];
        foreach ($allContentTypes as $contentType) {
            $contentTypeIDLookup[ (string) $contentType->_id ] = $contentType;
            $contentTypeAliasLookup[ (string) $contentType->getAlias() ] = $contentType;
        }

        Cache::forever(self::CONTENT_TYPE_ALIAS_LOOKUP_CACHE_KEY, $contentTypeAliasLookup);
        Cache::forever(self::CONTENT_TYPE_ID_LOOKUP_CACHE_KEY, $contentTypeIDLookup);

    }

    protected function buildDataTypeCache()
    {
        $allDataTypes = DataType::all();
        $dataTypeIDLookup = [ ];
        $dataTypeAliasLookup = [ ];
        foreach ($allDataTypes as $dataType) {
            $dataTypeIDLookup[ (string) $dataType->_id ] = $dataType;
            $dataTypeAliasLookup[ (string) $dataType->getAlias() ] = $dataType;
        }

        Cache::forever(self::DATA_TYPE_ALIAS_LOOKUP_CACHE_KEY, $dataTypeAliasLookup);
        Cache::forever(self::DATA_TYPE_ID_LOOKUP_CACHE_KEY, $dataTypeIDLookup);
    }

    public function getDataTypeFromCacheByID($dataTypeID)
    {
        if ( !Cache::has(self::DATA_TYPE_ID_LOOKUP_CACHE_KEY)) {
            $this->buildDataTypeCache();
        }
        $dataTypeIDLookup = Cache::get(self::DATA_TYPE_ID_LOOKUP_CACHE_KEY);
        if ( !isset( $dataTypeIDLookup[ $dataTypeID ] )) {
            $this->buildDataTypeCache();
            if ( !isset( $dataTypeIDLookup[ $dataTypeID ] )) {
                return null;
            }
        }

        return $dataTypeIDLookup[ $dataTypeID ];
    }

    public function getDataTypeFromCacheByAlias($alias)
    {
        if ( !Cache::has(self::DATA_TYPE_ALIAS_LOOKUP_CACHE_KEY)) {
            $this->buildDataTypeCache();
        }
        $dataTypeAliasLookup = Cache::get(self::DATA_TYPE_ALIAS_LOOKUP_CACHE_KEY);
        if ( !isset( $dataTypeAliasLookup[ $alias ] )) {
            $this->buildDataTypeCache();
            if ( !isset( $dataTypeAliasLookup[ $alias ] )) {
                return null;
            }
        }

        return $dataTypeAliasLookup[ $alias ];
    }

    public function getContentTypeFromCacheByID($contentTypeID)
    {
        if ( !Cache::has(self::CONTENT_TYPE_ID_LOOKUP_CACHE_KEY)) {
            $this->buildContentTypeCache();
        }
        $contentTypeIDLookup = Cache::get(self::CONTENT_TYPE_ID_LOOKUP_CACHE_KEY);
        if ( !isset( $contentTypeIDLookup[ $contentTypeID ] )) {
            $this->buildContentTypeCache();
            if ( !isset( $contentTypeIDLookup[ $contentTypeID ] )) {
                return null;
            }
        }

        return $contentTypeIDLookup[ $contentTypeID ];
    }

    public function getContentTypeFromCacheByAlias($alias)
    {
        if ( !Cache::has(self::CONTENT_TYPE_ALIAS_LOOKUP_CACHE_KEY)) {
            $this->buildContentTypeCache();
        }
        $contentTypeAliasLookup = Cache::get(self::CONTENT_TYPE_ALIAS_LOOKUP_CACHE_KEY);

        if ( !isset( $contentTypeAliasLookup[ $alias ] )) {
            $this->buildContentTypeCache();
            if ( !isset( $contentTypeAliasLookup[ $alias ] )) {
                return null;
            }
        }

        return $contentTypeAliasLookup[ $alias ];
    }

    public function addWorkflow(array $info)
    {
        if ( !isset( $info[ 'alias' ] )) {
            throw WorkflowException::cannotSetEmptyAliasForWorkflow();
        }
        if (empty( $info[ 'alias' ] )) {
            throw WorkflowException::cannotSetEmptyAliasForWorkflow();
        }
        if ( !StringUtil::isValidVariableName($info[ 'alias' ])) {
            throw WorkflowException::invalidWorkflowAlias();
        }
        if ( !isset( $info[ 'json' ] )) {
            $info[ 'json' ] = '';
        }
        $workflow = Workflow::fromArray($info);
        $workflowValidator = new WorkflowValidator();
        $validated = $workflowValidator->validateWorkflowJSON($workflow->getJSON());
        if ( !$validated) {
            throw WorkflowException::invalidWorkflowJSON($workflowValidator->getTranslatedErrorMessage());
        }
        $workflow->save();

        return $workflow;

    }

    public function getAllContentTypeRelatedToWorkflow($workflowID)
    {
        return $contentTypeRefs = ContentType::where('workflow_id', $workflowID)->get();
    }

    public function deleteWorkflowByID($id)
    {
        /** @var Workflow $workflow */
        $workflow = Workflow::where('_id', $id)->first();
        $this->deleteWorkflow($workflow);
    }

    public function deleteWorkflowByAlias($alias)
    {
        /** @var Workflow $workflow */
        $workflow = Workflow::where('alias', $alias)->first();
        $this->deleteWorkflow($workflow);
    }

    protected function deleteWorkflow(Workflow $workflow)
    {
        if ( !empty( $workflow )) {
            $refContentTypes = $this->getAllContentTypeRelatedToWorkflow((string) $workflow->_id);
            if (count($refContentTypes) > 0) {
                throw WorkflowException::workflowStillHaveReferences();
            }
            $workflow->delete();
        }
    }

    public function updateWorkflow($alias, array $info)
    {
        /** @var Workflow $workflow */
        $workflow = Workflow::where('alias', $alias)->first();
        if (empty( $workflow )) {
            throw WorkflowException::workflowNotFound();
        }

        if ( !empty( $info[ 'alias' ] )) {
            if ( !StringUtil::isValidVariableName($info[ 'alias' ])) {
                throw WorkflowException::invalidWorkflowAlias();
            }
        }

        if ( !isset( $info[ 'json' ] )) {
            $info[ 'json' ] = '';
        }
        $workflow->updateFromArray($info);
        $workflowValidator = new WorkflowValidator();
        $validated = $workflowValidator->validateWorkflowJSON($workflow->getJSON());
        if ( !$validated) {
            throw WorkflowException::invalidWorkflowJSON($workflowValidator->getTranslatedErrorMessage());
        }
        $workflow->save();

        return $workflow;
    }

    public function getAllWorkflows($orderBy = 'name', $direction = 'asc')
    {
        return Workflow::orderBy($orderBy, $direction)->get();
    }

    public function getWorkflowByID($id)
    {
        return Workflow::where('_id', $id)->first();
    }

    public function getWorkflowEngineFromContentType(ContentType $contentType)
    {
        $workflowEngine = new WorkflowEngine();
        $workflowJSON = null;
        if ( !empty( $contentType )) {
            $workflowID = $contentType->getWorkflowID();
            if ( !empty( $workflowID )) {
                /** @var Workflow $workflow */
                $workflow = $this->getWorkflowByID($workflowID);
                if ( !empty( $workflow )) {
                    $workflowJSON = $workflow->getJSON();
                }
            }
        }
        $workflowEngine->loadWorkflow($workflowJSON);

        return $workflowEngine;
    }
}