<?php

namespace LaunchCMS\Plugin;

use Illuminate\Support\ServiceProvider;

abstract class PluginServiceProvider extends ServiceProvider
{
    /**
     * @param $keyPath is the dot notation syntax to define where the menu is put in. Example:
     * content.stats => this means the menu data will be put in the real menu data $menu['content']['children']['stats']
     *
     * @param array $menu
     */
    public function registerAdminMenu($keyPath, array $menu)
    {

        if ( !isset( $this->app[ 'config' ][ 'admin-menu' ] )) {
            $this->app[ 'config' ]->set('admin-menu', [ ]);
        }
        $menuArray = $this->app[ 'config' ][ 'admin-menu' ];

        $browsingEl = &$menuArray;

        $paths = explode('.', $keyPath);
        $numberOfPath = count($paths);

        for ($index = 0; $index < $numberOfPath; $index++) {
            $path = $paths[ $index ];
            if ( !isset( $browsingEl[ $path ] )) {
                $browsingEl[ $path ] = [ ];
                $browsingEl[ $path ][ 'children' ] = [ ];
            }
            if ($index === $numberOfPath - 1) {
                $browsingEl[ $path ] = $menu;
                break;
            }

            $browsingEl = &$browsingEl[ $path ][ 'children' ];

        }
        $this->app[ 'config' ]->set('admin-menu', $menuArray);
    }


}