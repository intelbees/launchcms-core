<?php

namespace LaunchCMS\MongoDB;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DynamicModel extends Eloquent
{
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }
}