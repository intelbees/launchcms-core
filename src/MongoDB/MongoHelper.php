<?php


namespace LaunchCMS\MongoDB;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Query\Builder;

class MongoHelper
{
    public static function getMongoDB()
    {
        return DB::connection('mongodb');
    }

    public static function getMongoDBSchema()
    {
        return Schema::connection('mongodb');
    }

    public static function renameCollection($oldCollection, $newCollection)
    {
        $dbName = (string) DB::getMongoDB();
        DB::connection('mongodb_admin')->command([
            "renameCollection" => $dbName . '.' . $oldCollection,
            "to"               => $dbName . '.' . $newCollection ]);

    }

    public static function renameField($collectionName, $oldField, $newField)
    {

        $updateOption = (object) [ "\$rename" => [
            $oldField => $newField,
        ] ];
        $command = [ "update"  => $collectionName,
                     "updates" => [
                         [
                             "q" => (object) [ ],
                             "u" => $updateOption,
                         ],
                     ] ];
        DB::getMongoDB()->command($command);
    }

    public static function buildQueryBuilderFromArray(Builder &$builder, array $queryArray)
    {

    }
}