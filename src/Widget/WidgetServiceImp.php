<?php

namespace LaunchCMS\Widget;


class WidgetServiceImp
{
    protected $widgets = [ ];
    protected $pages = [ ];

    public function setPageConfigs($pages)
    {
        $this->pages = $pages;
    }

    public function loadWidgetConfig(array $config, $app)
    {

        foreach ($config as $widgetKey => $widgetConfig) {
            $widget = $this->loadWidget($widgetConfig, $app);
            if ( !empty( $widget )) {
                $this->widgets[ $widgetKey ] = $widget;
            }
        }
    }

    private function loadWidget($widgetConfig, $app)
    {
        $widgetClass = $widgetConfig[ 'class' ];
        $class = '\\' . ltrim($widgetClass, '\\');
        $widget = new $class();

        if ($widget instanceof WidgetInterface) {
            $widget->setApp($app);
        } else {
            $widget = null;
        }

        return $widget;
    }

    public function get($widgetAlias)
    {
        if (isset( $this->widgets[ $widgetAlias ] )) {
            return $this->widgets[ $widgetAlias ];
        }

        return null;
    }

    public function widgetsInBlock($pageAlias, $blockAlias)
    {
        if (empty( $this->pages ) ||
            !isset( $this->pages[ $pageAlias ] ) ||
            !isset( $this->pages[ $pageAlias ][ 'blocks' ] ) ||
            !isset( $this->pages[ $pageAlias ][ 'blocks' ][ $blockAlias ] )
        ) {
            return [ ];
        }
        $result = [ ];
        $widgetAliasInBlock = $this->pages[ $pageAlias ][ 'blocks' ][ $blockAlias ];

        foreach ($widgetAliasInBlock as $widgetAlias) {
            $widget = $this->get($widgetAlias);
            if ( !empty( $widget )) {
                $result[] = $widget;
            }
        }

        return $result;
    }
}