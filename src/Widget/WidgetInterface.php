<?php
namespace LaunchCMS\Widget;


interface WidgetInterface
{
    public function render ( $params = null );

    public function setApp ( $app );
}