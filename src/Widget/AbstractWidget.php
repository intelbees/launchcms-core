<?php

namespace LaunchCMS\Widget;


abstract class AbstractWidget implements WidgetInterface
{
    protected $app;

    public function setApp ($app)
    {
        $this->app = $app;
    }

}