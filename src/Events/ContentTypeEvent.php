<?php

namespace LaunchCMS\Events;

use Illuminate\Queue\SerializesModels;
use LaunchCMS\Models\Content\ContentType;

class ContentTypeEvent extends CMSEvent
{
    public $contentType;

    public function __construct(ContentType $contentType)
    {
        $this->contentType = $contentType;
    }

}