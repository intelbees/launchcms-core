<?php

namespace LaunchCMS\Events;

use Illuminate\Queue\SerializesModels;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\User\User;

class UserWasDeleted extends UserEvent
{
    
}