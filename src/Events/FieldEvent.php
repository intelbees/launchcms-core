<?php

namespace LaunchCMS\Events;

use LaunchCMS\Models\Content\Field;

class FieldEvent extends CMSEvent
{
    

    public $structureObject;
    public $field;

    public function __construct($structureObject, Field $field)
    {
        $this->structureObject = $structureObject;
        $this->field = $field;
    }

}