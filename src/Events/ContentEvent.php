<?php

namespace LaunchCMS\Events;

use Illuminate\Queue\SerializesModels;
use LaunchCMS\Models\Content\DataObject\Content;

/**
 * Event class is thrown after a content was deleted
 * @package LaunchCMS\Events
 *
 */
class ContentEvent extends CMSEvent
{


    public $content;

    public function __construct(Content $content)
    {
        $this->content = $content;
    }

}