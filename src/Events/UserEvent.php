<?php

namespace LaunchCMS\Events;

use LaunchCMS\Models\User\User;

class UserEvent extends CMSEvent
{
    public $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

}