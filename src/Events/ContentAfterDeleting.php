<?php

namespace LaunchCMS\Events;

/**
 * Event class is thrown after a content was deleted
 * @package LaunchCMS\Events
 *
 */
class ContentAfterDeleting extends ContentEvent
{
    
}