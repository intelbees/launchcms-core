<?php

namespace LaunchCMS\Events;

/**
 * Event class is thrown after a content was moved to trash.
 * @package LaunchCMS\Events
 */
class ContentAfterMovingToTrash extends ContentEvent
{

}