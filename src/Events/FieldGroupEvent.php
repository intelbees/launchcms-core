<?php

namespace LaunchCMS\Events;

use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\FieldGroup;

class FieldGroupEvent extends CMSEvent
{

    public $contentType;
    public $fieldGroup;

    public function __construct(ContentType $contentType, FieldGroup $fieldGroup)
    {
        $this->contentType = $contentType;
        $this->fieldGroup = $fieldGroup;
    }

}