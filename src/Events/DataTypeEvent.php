<?php

namespace LaunchCMS\Events;

use LaunchCMS\Models\Content\DataType;

class DataTypeEvent extends CMSEvent
{
    public $dataType;

    public function __construct(DataType $dataType)
    {
        $this->dataType = $dataType;
    }

}