<?php

namespace LaunchCMS;

use Dflydev\ApacheMimeTypes\PhpRepository;
use Illuminate\Support\ServiceProvider;
use LaunchCMS\Services\ContentServiceImp;
use LaunchCMS\Services\Interfaces\ContentServiceInterface;
use LaunchCMS\Services\Interfaces\InstallationServiceInterface;
use LaunchCMS\Services\Interfaces\MediaServiceInterface;
use LaunchCMS\Services\Interfaces\OrganizationServiceInterface;
use LaunchCMS\Services\Interfaces\PermissionServiceInterface;
use LaunchCMS\Services\Interfaces\SearchServiceInterface;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;
use LaunchCMS\Services\Interfaces\UserServiceInterface;
use LaunchCMS\Services\MediaServiceImp;
use LaunchCMS\Services\StructureServiceImp;
use LaunchCMS\Services\Facades\PermissionService;
use LaunchCMS\Services\InstallationServiceImp;
use LaunchCMS\Services\OrganizationServiceImp;
use LaunchCMS\Services\PermissionServiceImp;
use LaunchCMS\Services\SearchServiceImp;
use LaunchCMS\Services\UserServiceImp;
use LaunchCMS\Widget\WidgetInterface;
use LaunchCMS\Widget\WidgetServiceImp;

class LaunchCoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // use this if your package has views
        $this->loadViewsFrom(realpath(__DIR__ . '/resources/views'), 'launchcms');


        // use this if your package needs a config file
        $this->publishes([
            __DIR__ . '/config/config.php' => config_path('launchcms.php'),
        ]);

        // use the vendor configuration file as fallback
        $this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'launchcms'
        );
        PermissionService::registerPermission('cms_access_content_type', 'Access content type section', 'Able to access content type section');
        PermissionService::registerPermission('cms_create_content_type', 'Create content type', 'Able to create content type');
        PermissionService::registerPermission('cms_delete_content_type', 'Delete content type', 'Able to create content type');

        PermissionService::registerPermission('cms_access_data_type', 'Access data type section', 'Able to access data type section');
        PermissionService::registerPermission('cms_create_data_type', 'Create data type', 'Able to create data type');
        PermissionService::registerPermission('cms_delete_data_type', 'Delete data type', 'Able to delete data type');

        PermissionService::registerPermission('cms_access_user', 'Access user section', 'Able to access user section in the CMS');
        PermissionService::registerPermission('cms_create_user', 'Create user', 'Able to create user');
        PermissionService::registerPermission('cms_delete_user', 'Delete user', 'Able to delete user');
        PermissionService::registerPermission('cms_ban_user', 'Ban/activate user', 'Able to ban/activate user');


        PermissionService::registerPermission('cms_access_role', 'Access role section', 'Able to access role section in the CMS');
        PermissionService::registerPermission('cms_create_role', 'Create role', 'Able to create role');
        PermissionService::registerPermission('cms_delete_role', 'Delete role', 'Able to delete role');

        PermissionService::registerPermission('cms_access_relationship_definition', 'Access relationship definition section', 'Able to access relationship definition section in the CMS');
        PermissionService::registerPermission('cms_create_relationship_definition', 'Create relationship definition', 'Able to create relationship definition');
        PermissionService::registerPermission('cms_delete_relationship_definition', 'Delete relationship definition', 'Able to delete relationship definition');

        PermissionService::registerPermission('cms_access_workflow_definition', 'Access workflow definition section', 'Able to access workflow definition section in the CMS');
        PermissionService::registerPermission('cms_create_workflow_definition', 'Create workflow definition', 'Able to create workflow definition');
        PermissionService::registerPermission('cms_delete_workflow_definition', 'Delete workflow definition', 'Able to delete workflow definition');

        PermissionService::registerPermission('cms_access_media', 'Access media section', 'Able to access media section in the CMS');
        PermissionService::registerPermission('cms_create_media', 'Create media', 'Able to create media items (file, folder)');
        PermissionService::registerPermission('cms_delete_media', 'Delete media', 'Able to delete media items (file, folder)');


        PermissionService::registerPermission('cms_access_content', 'Access content section', 'Able to access content section in the CMS');
        PermissionService::registerContentPermission('content.delete', 'Delete content', 'Able to delete content');
        PermissionService::registerContentPermission('content.modify', 'Create and modify content', 'Able to create and modify content');
        PermissionService::registerContentPermission('content.change_status', 'Change content status', 'Able to change content status (except publish status)');
    }


    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSkeleton();
    }

    private function registerSkeleton()
    {
        $this->app->bind('InstallationService', function($app) {
            return new InstallationServiceImp($app);
        });

        $this->app->bind('ContentService', function($app) {
            return new ContentServiceImp($app);
        });

        $this->app->bind('StructureService', function($app) {
            return new StructureServiceImp($app);
        });

        $this->app->bind('UserService', function($app) {
            return new UserServiceImp($app);
        });

        $this->app->bind('SearchService', function($app) {
            return new SearchServiceImp($app);
        });

        $this->app->bind('PermissionService', function($app) {
            return new PermissionServiceImp($app);
        });

        $this->app->bind('OrganizationService', function($app) {
            return new OrganizationServiceImp($app);
        });

        $this->app->bind('PhpRepository', function($app) {
            return new PhpRepository();
        });

        $this->app->bind('MediaService', function($app) {
            $mediaService = new MediaServiceImp($app['PhpRepository']);
            $configWebPath = config('launchcms.media.webpath');
            $mediaService->setWebPath($configWebPath);
            return $mediaService;
        });

        $this->app->bind('WidgetService', function($app) {

            $widgetLocator = new WidgetServiceImp($app);
            $launchCMSConfig = $this->app[ 'config' ][ 'launchcms' ];
            if (isset( $launchCMSConfig[ 'widgets' ] )) {
                $widgetConfig = $launchCMSConfig[ 'widgets' ];
                if ( !empty( $widgetConfig )) {
                    $widgetLocator->loadWidgetConfig($widgetConfig, $this->app);
                }
            }
            if (isset( $launchCMSConfig[ 'pages' ] )) {
                $widgetLocator->setPageConfigs($launchCMSConfig[ 'pages' ]);
            }

            return $widgetLocator;
        });
        $this->app->bind(MediaServiceInterface::class, function($app) {
            $mediaService = new MediaServiceImp($app['PhpRepository']);
            $configWebPath = config('launchcms.media.webpath');
            $mediaService->setWebPath($configWebPath);
            return $mediaService;
        });
        $this->app->bind(ContentServiceInterface::class, ContentServiceImp::class);
        $this->app->bind(WidgetInterface::class, WidgetServiceImp::class);
        $this->app->bind(InstallationServiceInterface::class, InstallationServiceImp::class);
        $this->app->bind(StructureServiceInterface::class, StructureServiceImp::class);
        $this->app->bind(PermissionServiceInterface::class, PermissionServiceImp::class);
        $this->app->bind(UserServiceInterface::class, UserServiceImp::class);
        $this->app->bind(SearchServiceInterface::class, SearchServiceImp::class);
        $this->app->bind(OrganizationServiceInterface::class, OrganizationServiceImp::class);

    }


}