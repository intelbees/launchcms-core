<?php

namespace LaunchCMS\Models\Organization;


use LaunchCMS\Models\IdentityModel;

abstract class Organization extends IdentityModel
{
    public function owner()
    {
        return $this->belongsTo('LaunchCMS\Models\User\User', 'owner_id');
    }

}