<?php

namespace LaunchCMS\Models\Organization;



class Group extends Organization
{
    protected $collection = 'cms_groups';
    protected static $unguarded = true;
    protected $guarded = ['name', 'alias', 'description', 'site_id', 'need_approval_to_join'];

    public function site()
    {
        return $this->belongsTo('LaunchCMS\Models\Organization\Site', 'site_id');
    }

    public function needApprovalToJoin() {
        return $this->need_approval_to_join;
    }

    public function setNeedApprovalFlag($needApprovalFlag) {
        $this->need_approval_to_join = $needApprovalFlag;
    }

    public function updateFromArray(array $info)
    {
        parent::updateFromArray($info);
        if(isset($info['site_id'])) {
            $this->site_id = $info['site_id'];
        }

        if(isset($info['need_approval_to_join'])) {
            $this->need_approval_to_join = $info['need_approval_to_join'];
        }
    }


}