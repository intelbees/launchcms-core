<?php

namespace LaunchCMS\Models\Organization;


class Site extends Organization
{
    protected $collection = 'cms_sites';
    protected static $unguarded = true;
    protected $guarded = [ 'name', 'alias', 'description', 'domain_regex' ];

    public function updateFromArray(array $info)
    {
        parent::updateFromArray($info);
        if (isset( $info[ 'domain_regex' ] )) {
            $this->setDomainRegExp($info[ 'domain_regex' ]);
        }
    }

    public function setDomainRegExp($domain)
    {
        $this->domain_regex = $domain;
    }

    public function getDomainRegExp()
    {
        return $this->domain_regex;
    }


}