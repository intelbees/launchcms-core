<?php

namespace LaunchCMS\Models\User;

use LaunchCMS\Models\LaunchEloquentModel;

class Reminder extends LaunchEloquentModel
{
    protected $collection = 'cms_reminders';
    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'code',
        'completed',
        'completed_at',
    ];

    /**
     * Get mutator for the "completed" attribute.
     *
     * @param  mixed $completed
     * @return bool
     */
    public function getCompletedAttribute()
    {
        return (bool) $this->attributes[ 'completed' ];
    }

    /**
     * Set mutator for the "completed" attribute.
     *
     * @param  mixed $completed
     * @return void
     */
    public function setCompletedAttribute($completed)
    {
        $this->attributes[ 'completed' ] = (int) (bool) $completed;
    }

    public function __construct($attributes = [ ])
    {
        parent::__construct($attributes);
        if ( !isset( $this->attributes[ 'completed' ] )) {
            $this->attributes[ 'completed' ] = false;
        }
    }
}