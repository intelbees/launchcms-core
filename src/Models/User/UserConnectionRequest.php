<?php

namespace LaunchCMS\Models\User;

use LaunchCMS\Models\LaunchEloquentModel;

class UserConnectionRequest extends LaunchEloquentModel
{

    protected $collection = 'cms_user_connection_requests';

    public function relationshipDefinition()
    {
        return $this->belongsTo('LaunchCMS\Models\User\RelationshipDefinition', 'relationship_id');
    }

    public function requestUser()
    {
        return $this->belongsTo('LaunchCMS\Models\User\User', 'request_user_id');
    }

    public function targetUser()
    {
        return $this->belongsTo('LaunchCMS\Models\User\User', 'target_user_id');
    }

}