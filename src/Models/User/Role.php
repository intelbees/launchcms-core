<?php
namespace LaunchCMS\Models\User;

use Cartalyst\Sentinel\Permissions\PermissibleInterface;
use Cartalyst\Sentinel\Permissions\PermissibleTrait;
use Cartalyst\Sentinel\Roles\RoleInterface;
use LaunchCMS\Models\LaunchEloquentModel;

class Role extends LaunchEloquentModel implements RoleInterface, PermissibleInterface
{
    use PermissibleTrait;
    protected static $usersModel = 'LaunchCMS\Models\User\User';
    protected $collection = 'cms_roles';
    protected $fillable = [
        'name',
        'slug',
        'permissions',
    ];

    /**
     * Get mutator for the "permissions" attribute.
     *
     * @param  mixed $permissions
     * @return array
     */
    public function getPermissionsAttribute()
    {
        if ( !isset( $this->attributes[ 'permissions' ] )) {
            $this->attributes[ 'permissions' ] = '';

            return [ ];
        }

        $result = json_decode($this->attributes[ 'permissions' ], true);
        if ($result == null) {
            $result = [ ];
        }

        return $result;
    }

    /**
     * Set mutator for the "permissions" attribute.
     *
     * @param  mixed $permissions
     * @return void
     */
    public function setPermissionsAttribute(array $permissions)
    {
        $this->attributes[ 'permissions' ] = $permissions ? json_encode($permissions) : '';
    }

    public function users()
    {
        return User::where('roles', $this->getRoleId())->get();
    }

    public function getRoleId()
    {
        return $this->getKey();
    }

    public function getRoleSlug()
    {
        return $this->slug;
    }

    public function getUsers()
    {
        return $this->users();
    }

    public static function getUsersModel()
    {
        return static::$usersModel;
    }

    public static function setUsersModel($usersModel)
    {
        static::$usersModel = $usersModel;
    }


    public function __call($method, $parameters)
    {
        $methods = [ 'hasAccess', 'hasAnyAccess' ];

        if (in_array($method, $methods)) {
            $permissions = $this->getPermissionsInstance();

            return call_user_func_array([ $permissions, $method ], $parameters);
        }

        return parent::__call($method, $parameters);
    }

    /**
     * {@inheritDoc}
     */
    protected function createPermissions()
    {
        return new static::$permissionsClass($this->permissions);
    }

    public function getContentPermissionMatrix()
    {
        return isset( $this->content_permission_matrix ) ? $this->content_permission_matrix : [ ];
    }

    public function setContentPermissionMatrix(array $permissionMatrix)
    {
        $this->content_permission_matrix = $permissionMatrix;
    }

    public function setContentPermission($contentTypeID, array $contentPermission)
    {
        if ( !isset( $this->content_permission_matrix )) {
            $this->content_permission_matrix = [ ];
        }
        $this->content_permission_matrix[ $contentTypeID ] = $contentPermission;
    }

    public function hasContentPermission($contentPermissionSlug, $contentTypeID)
    {
        if ( !isset( $this->content_permission_matrix )) {
            return false;
        }
        if ( !isset( $this->content_permission_matrix[ $contentTypeID ] )) {
            return false;
        }

        return in_array($contentPermissionSlug, $this->content_permission_matrix[ $contentTypeID ]);
    }
}