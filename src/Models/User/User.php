<?php

namespace LaunchCMS\Models\User;

use Carbon\Carbon;
use Cartalyst\Sentinel\Roles\RoleInterface;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Roles\RoleableInterface;
use Cartalyst\Sentinel\Persistences\PersistableInterface;
use LaunchCMS\Models\Content\Workflow\WorkflowUserInterface;
use LaunchCMS\Models\LaunchEloquentModel;
use LaunchCMS\Utils\ArrayUtil;
use Cartalyst\Sentinel\Permissions\PermissibleTrait;
use Cartalyst\Sentinel\Permissions\PermissibleInterface;
use LaunchCMS\Utils\DateUtil;

class User extends LaunchEloquentModel implements WorkflowUserInterface, UserInterface, RoleableInterface, PersistableInterface, PermissibleInterface
{
    use PermissibleTrait;
    protected $collection = 'cms_users';
    const FRIEND_RELATIONSHIP = 'friends';
    const FOLLOW_RELATIONSHIP = 'follows';
    const RELATIONSHIPS = 'relationships';
    protected $cachedRoles = null;
    protected $cachedContentPermissionMatrix = null;

    protected function getSelfCacheRoles()
    {
        if ($this->cachedRoles == null) {
            $this->cachedRoles = $this->getRoles();
        }

        return $this->cachedRoles;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getRoleSlugs()
    {
        $roles = $this->getRoles();
        $roleSlugs = [];
        /** @var Role $role */
        foreach ($roles as $role) {
            $roleSlugs []= $role->getRoleSlug();
        }
        return $roleSlugs;
    }

    protected function getSelfCachedContentPermissionMatrix()
    {
        if ($this->cachedContentPermissionMatrix == null) {
            $this->cachedContentPermissionMatrix = [ ];
            $roles = $this->getSelfCacheRoles();
            /** @var Role $role */
            foreach ($roles as $role) {
                $permissionMatrix = $role->getContentPermissionMatrix();
                foreach ($permissionMatrix as $contentID => $permissions) {
                    if ( !isset( $this->cachedContentPermissionMatrix[ $contentID ] )) {
                        $this->cachedContentPermissionMatrix[ $contentID ] = [ ];
                    }
                    $this->cachedContentPermissionMatrix[ $contentID ] = array_merge($this->cachedContentPermissionMatrix[ $contentID ], $permissions);
                }
            }
        }

        return $this->cachedContentPermissionMatrix;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'reset_password_code',
        'activation_code',
        'persist_code',
    ];
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'reset_password_code',
        'activation_code',
        'persist_code',
    ];
    /**
     * Attributes that should be hashed.
     *
     * @var array
     */
    protected $hashableAttributes = [
        'password',
        'persist_code',
    ];

    /**
     * The login attribute.
     *
     * @var string
     */
    protected static $loginAttribute = 'email';

    public function groups()
    {
        return isset( $this->attributes[ 'groups' ] ) ? $this->attributes[ 'groups' ] : [ ];
    }

    public function getJoinedGroupIDs()
    {
        if ( !isset( $this->attributes[ 'groups' ] )) {
            $this->attributes[ 'groups' ] = [ ];
        }
        $result = [ ];
        foreach ($this->attributes[ 'groups' ] as $group) {
            $result[] = $group[ 'id' ];
        }

        return $result;
    }

    public function hasJoinedGroup($groupID)
    {
        if ( !isset( $this->attributes[ 'groups' ] )) {
            $this->attributes[ 'groups' ] = [ ];
        }
        foreach ($this->attributes[ 'groups' ] as $group) {
            if ($groupID === $group[ 'id' ]) {
                return true;
            }
        }

        return false;
    }

    public function addGroup($groupID)
    {
        if ( !$this->hasJoinedGroup($groupID)) {
            $this->attributes[ 'groups' ] [] = [ 'id' => $groupID, 'time' => new Carbon() ];
        }
        $this->save();
    }

    public function leaveGroup($groupID)
    {
        if ( !$this->hasJoinedGroup($groupID)) {
            return;
        }
        $countOfGroup = count($this->attributes[ 'groups' ]);
        $foundIndex = -1;
        for ($index = 0; $index < $countOfGroup; $index++) {
            if ($groupID === $this->attributes[ 'groups' ][ $index ][ 'id' ]) {
                $foundIndex = $index;
                break;
            }
        }
        if ($foundIndex !== -1) {
            ArrayUtil::removeElementAtIndex($foundIndex, $this->attributes[ 'groups' ]);
        }

    }

    /**
     * Get mutator for the "permissions" attribute.
     *
     * @param  mixed $permissions
     * @return array
     */
    public function getPermissionsAttribute()
    {
        if ( !isset( $this->attributes[ 'permissions' ] )) {
            $this->attributes[ 'permissions' ] = '';

            return [ ];
        }

        $result = json_decode($this->attributes[ 'permissions' ], true);
        if ($result == null) {
            $result = [ ];
        }

        return $result;
    }

    /**
     * Set mutator for the "permissions" attribute.
     *
     * @param  mixed $permissions
     * @return void
     */
    public function setPermissionsAttribute(array $permissions)
    {
        $this->attributes[ 'permissions' ] = $permissions ? json_encode($permissions) : '';
    }

    public function getUserId()
    {
        return $this->getKey();
    }

    public function getUserLogin()
    {
        return $this->login;
    }

    public function getUserLoginName()
    {
        return $this->loginName;
    }

    public function getUserPassword()
    {
        return $this->password;
    }

    /**
     * Returns an array of login column names.
     *
     * @return array
     */
    public function getLoginNames()
    {
        return [ 'email' ];
    }


    public function getRoles()
    {
        if ( !isset( $this->attributes[ 'roles' ] )) {
            $this->attributes[ 'roles' ] = [ ];
        }
        $roles = [ ];
        if ( !empty( $this->attributes[ 'roles' ] )) {
            $roles = Role::whereIn('_id', $this->attributes[ 'roles' ])->get();
        }

        return $roles;
    }

    public function inRole($role)
    {
        $roleID = $this->getRoleIDFromVariable($role);
        if (empty( $roleID )) {
            return false;
        }
        if ( !isset( $this->attributes[ 'roles' ] )) {
            $this->attributes[ 'roles' ] = [ ];
        }
        if (in_array($roleID, $this->attributes[ 'roles' ])) {
            return true;
        }

        return false;
    }

    public function getAssignedRoleIDs()
    {
        if ( !isset( $this->attributes[ 'roles' ] )) {
            return [ ];
        }

        return $this->attributes[ 'roles' ];
    }

    public function getPersistableKey()
    {
        return 'user_id';
    }

    public function getPersistableId()
    {
        return $this->getKey();
    }

    /**
     * {@inheritDoc}
     */
    public function setPersistableRelationship($persistableRelationship)
    {
        $this->persistableRelationship = $persistableRelationship;
    }

    /**
     * {@inheritDoc}
     */
    public function getPersistableRelationship()
    {
        return $this->persistableRelationship;
    }

    public function generatePersistenceCode()
    {
        return str_random(32);
    }

    private function getRoleIDFromVariable($role)
    {
        $roleID = null;
        if ($role instanceof RoleInterface) {
            $roleID = $role->getRoleId();
        } else {
            $roleObj = Role::where('slug', $role)->first();
            if (empty( $roleObj )) {
                return null;
            }
            $roleID = $roleObj->_id;
        }

        return $roleID;
    }

    public function addRole($role)
    {
        $roleID = $this->getRoleIDFromVariable($role);
        if (empty( $roleID )) {
            return;
        }

        if ( !isset( $this->attributes[ 'roles' ] )) {
            $this->attributes[ 'roles' ] = [ ];
        }
        if ( !in_array($roleID, $this->attributes[ 'roles' ])) {
            $this->attributes[ 'roles' ][] = $roleID;
        }
        $this->save();
    }

    public function removeRole($role)
    {
        $roleID = $this->getRoleIDFromVariable($role);
        if (empty( $roleID )) {
            return;
        }

        if ( !isset( $this->attributes[ 'roles' ] )) {
            $this->attributes[ 'roles' ] = [ ];
        }
        if (in_array($roleID, $this->attributes[ 'roles' ])) {
            ArrayUtil::removeElement($roleID, $this->attributes[ 'roles' ]);
        }
        $this->save();
    }

    /**
     * Creates a permissions object.
     *
     * @return \Cartalyst\Sentinel\Permissions\PermissionsInterface
     */
    protected function createPermissions()
    {
        $userPermissions = $this->permissions;
        $rolePermissions = [ ];
        $roles = $this->getRoles();
        foreach ($roles as $role) {
            $rolePermissions[] = $role->permissions;
        }

        return new static::$permissionsClass($userPermissions, $rolePermissions);
    }

    public function __call($method, $parameters)
    {
        $methods = [ 'hasAccess', 'hasAnyAccess' ];

        if (in_array($method, $methods)) {
            $permissions = $this->getPermissionsInstance();

            return call_user_func_array([ $permissions, $method ], $parameters);
        }

        return parent::__call($method, $parameters);
    }

    protected function checkAndInitRelationshipArray($relationshipAlias)
    {
        if ( !isset( $this->attributes[ self::RELATIONSHIPS ] )) {
            $this->attributes[ self::RELATIONSHIPS ] = [ ];
        }
        if ( !isset( $this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ] )) {
            $this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ] = [ ];
        }
    }

    public function addToRelationship($relationshipAlias = self::FRIEND_RELATIONSHIP, User $user)
    {
        $this->checkAndInitRelationshipArray($relationshipAlias);
        $now = $now = DateUtil::fromDateTime(new Carbon);
        $this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ] [] = [ 'user_id' => $user->_id, 'created_at' => $now ];
        $this->save();
    }

    public function removeFromRelationship($relationshipAlias = self::FRIEND_RELATIONSHIP, User $user)
    {
        $this->checkAndInitRelationshipArray($relationshipAlias);
        $sizeOfRelationshipData = count($this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ]);
        $foundIndex = -1;
        for ($index = 0; $index < $sizeOfRelationshipData; $index++) {
            $relationshipData = $this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ][ $index ];
            if ($relationshipData[ 'user_id' ] == $user->_id) {
                $foundIndex = $index;
                break;
            }
        }
        if ($foundIndex >= 0) {
            ArrayUtil::removeElementAtIndex($foundIndex, $this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ]);
            $this->save();
        }
    }

    public function relationshipData($relationshipAlias)
    {
        $this->checkAndInitRelationshipArray($relationshipAlias);

        return $this->attributes[ $relationshipAlias ];
    }

    public function inRelationship($relationshipAlias, $userID)
    {
        $this->checkAndInitRelationshipArray($relationshipAlias);
        $sizeOfRelationshipData = count($this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ]);
        $foundIndex = -1;

        for ($index = 0; $index < $sizeOfRelationshipData; $index++) {
            $relationshipData = $this->attributes[ self::RELATIONSHIPS ][ $relationshipAlias ][ $index ];
            if ($relationshipData[ 'user_id' ] === $userID) {
                $foundIndex = $index;
                break;
            }
        }

        return $foundIndex >= 0;
    }

    public static function copy(User $sourceUser, User &$targetUser, array $excludeAttributeKeys = [ ])
    {
        foreach ($sourceUser->attributes as $key => $value) {
            if ( !empty( $excludeAttributeKeys ) && in_array($key, $excludeAttributeKeys)) {
                continue;
            }
            $targetUser->attributes[ $key ] = $value;
        }
    }

    public function canDo($contentPermission, $contentTypeID)
    {
        $permissionMatrix = $this->getSelfCachedContentPermissionMatrix();

        return isset( $permissionMatrix[ $contentTypeID ] ) &&
        in_array($contentPermission, $permissionMatrix[ $contentTypeID ]);
    }

    public function canDoActions(array $contentPermissions, $contentTypeID)
    {
        $permissionMatrix = $this->getSelfCachedContentPermissionMatrix();
        foreach ($contentPermissions as $contentPermission) {
            if (isset( $permissionMatrix[ $contentTypeID ] ) &&
                in_array($contentPermission, $permissionMatrix[ $contentTypeID ])
            ) {
                return true;
            }
        }

        return false;
    }
}