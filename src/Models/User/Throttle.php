<?php

namespace LaunchCMS\Models\User;

use LaunchCMS\Models\LaunchEloquentModel;

class Throttle extends LaunchEloquentModel
{
    protected $collection = 'cms_throttles';

    protected $fillable = [
        'ip',
        'type',
    ];
}