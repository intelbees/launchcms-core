<?php


namespace LaunchCMS\Models\User;

use Cartalyst\Sentinel\Persistences\PersistenceInterface;
use LaunchCMS\Models\LaunchEloquentModel;

class Persistence extends LaunchEloquentModel implements PersistenceInterface
{
    protected $collection = 'cms_persistences';

    public function user()
    {
        return $this->belongsTo('LaunchCMS\Models\User\User', 'user_id');
    }

}