<?php


namespace LaunchCMS\Models\User;

use Cartalyst\Sentinel\Activations\ActivationInterface;
use LaunchCMS\Models\LaunchEloquentModel;

class Activation extends LaunchEloquentModel implements ActivationInterface
{
    protected $collection = 'cms_activations';
    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'code',
        'completed',
        'completed_at',
    ];

    public function getCode()
    {
        return $this->code;
    }


    /**
     * Get mutator for the "completed" attribute.
     *
     * @param  mixed $completed
     * @return bool
     */
    public function getCompletedAttribute()
    {
        if ( !isset( $this->attributes[ 'completed' ] )) {
            $this->attributes[ 'completed' ] = false;
        }

        return (bool) $this->attributes[ 'completed' ];
    }

    /**
     * Set mutator for the "completed" attribute.
     *
     * @param  mixed $completed
     * @return void
     */
    public function setCompletedAttribute($completed)
    {
        $this->attributes[ 'completed' ] = (bool) $completed;
    }

    public function __construct($attributes = [ ])
    {
        parent::__construct($attributes);
        if ( !isset( $this->attributes[ 'completed' ] )) {
            $this->attributes[ 'completed' ] = false;
        }
    }
}