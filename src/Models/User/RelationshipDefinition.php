<?php

namespace LaunchCMS\Models\User;

use LaunchCMS\Models\LaunchEloquentModel;

/**
 * Class RelationshipDefinition
 * To describe the relationship definition, which include: name, alias, inverse (2 ways or 1 way relationship direction),
 * needApproval (allow creating the relationship directly or need to wait for approval of the requested user)
 * @package LaunchCMS\Models\User
 */
class RelationshipDefinition extends LaunchEloquentModel
{
    protected static $unguarded = true;
    protected $guarded = [ 'name', 'alias', 'inverse', 'need_approval' ];
    protected $collection = 'cms_relationship_definitions';


    public function getName()
    {
        return $this->attributes[ 'name' ];
    }

    public function setName($name)
    {
        $this->attributes[ 'name' ] = $name;
    }

    public function getAlias()
    {
        return $this->attributes[ 'alias' ];
    }

    public function setAlias($alias)
    {
        $this->attributes[ 'alias' ] = $alias;
    }

    public function inverse()
    {
        return $this->attributes[ 'inverse' ];
    }

    public function setInverse($inverse)
    {
        $this->attributes[ 'inverse' ] = $inverse;
    }

    public function needApproval()
    {
        return (bool) $this->attributes[ 'need_approval' ];
    }

    public function setNeedApproval($approval)
    {
        $this->attributes[ 'need_approval' ] = $approval;
    }

    public function __construct($attributes = [ ])
    {
        parent::__construct($attributes);
        if ( !isset( $this->attributes[ 'need_approval' ] )) {
            $this->attributes[ 'need_approval' ] = true;
        }
        if ( !isset( $this->attributes[ 'inverse' ] )) {
            $this->attributes[ 'inverse' ] = true;
        }
    }

    public function updateFromArray(array $info)
    {
        if (isset( $info[ 'name' ] )) {
            $this->setName($info[ 'name' ]);
        }
        if (isset( $info[ 'alias' ] )) {
            $this->setAlias($info[ 'alias' ]);
        }

        if (isset( $info[ 'inverse' ] )) {
            $this->setInverse($info[ 'inverse' ]);
        }

        if (isset( $info[ 'need_approval' ] )) {
            $this->setNeedApproval($info[ 'need_approval' ]);
        }

    }
}