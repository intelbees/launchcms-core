<?php

namespace LaunchCMS\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use LaunchCMS\Utils\SerializeUtil;

abstract class LaunchEloquentModel extends Eloquent implements \Serializable
{
    protected $connection = 'mongodb';

    public function serialize()
    {
        return SerializeUtil::serialize($this->attributes);
    }

    public function unserialize($serialized)
    {

        $data = SerializeUtil::unserialize($serialized);

        foreach ($data as $key => $value) {
            $this->setAttribute($key, $value);
        }
        $this->exists = isset( $this->attributes[ '_id' ] ) && !empty( $this->attributes[ '_id' ] );
    }
}