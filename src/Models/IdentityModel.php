<?php

namespace LaunchCMS\Models;

use LaunchCMS\Utils\SerializeUtil;
use LaunchCMS\Utils\StringUtil;

abstract class IdentityModel extends LaunchEloquentModel implements \Serializable
{
    protected $connection = 'mongodb';

    public function serialize()
    {
        return SerializeUtil::serialize($this->attributes);
    }

    public function unserialize($serialized)
    {

        $data = SerializeUtil::unserialize($serialized);

        foreach ($data as $key => $value) {
            $this->setAttribute($key, $value);
        }
        $this->exists = isset( $this->attributes[ '_id' ] ) && !empty( $this->attributes[ '_id' ] );
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    public function getDevSettings()
    {
        return isset( $this->attributes[ 'dev_settings' ] ) ? $this->attributes[ 'dev_settings' ] : [ ];
    }

    public function setDevSettings(array $devSettings)
    {
        $this->attributes[ 'dev_settings' ] = $devSettings;
    }

    public function isBuiltInType()
    {
        return isset( $this->attributes[ 'dev_settings' ] ) &&
        isset( $this->attributes[ 'dev_settings' ][ 'builtin' ] ) &&
        $this->attributes[ 'dev_settings' ][ 'builtin' ] === true;
    }

    public function validateAlias()
    {
        if (empty( $this->alias )) {
            return false;
        }

        return true;
    }

    protected static function updateGenericInfoFromArray(array $info, IdentityModel &$structure)
    {
        if (isset( $info[ 'name' ] )) {
            $structure->setName($info[ 'name' ]);
        }
        if (isset( $info[ 'description' ] )) {
            $structure->setDescription($info[ 'description' ]);
        }
        $alias = null;
        if (isset( $info[ 'alias' ] )) {
            $alias = $info[ 'alias' ];
        } else if (empty( $structure->alias )) {
            $alias = $structure->name;
        }

        if ( !empty( $alias )) {
            $alias = strtolower($alias);
            $alias = StringUtil::sanitize($alias, '_');
            $structure->setAlias($alias);
        }

        if (isset( $info[ 'settings' ] )) {
            $structure->setSettings($info[ 'settings' ]);
        }

        if (isset( $info[ 'dev_settings' ] )) {
            $structure->setDevSettings($info[ 'dev_settings' ]);
        }

    }


}