<?php


namespace LaunchCMS\Models\Content;


use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;

class ReferenceListField extends ReferenceField
{
    public function __construct($contentTypeID)
    {
        $this->setDataType(self::REFERENCE_LIST);
        $this->setExtraDataTypeInfo([ self::CONTENT_TYPE_ID => $contentTypeID ]);
    }

    public function validateValue($value, array &$errors)
    {
        if($value == null) {
            return true;
        }
        if ( !is_array($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }
        if(!empty($value)) {
            /** @var ContentType $contentType */
            $contentType = StructureService::getContentTypeFromCacheByID($this->getReferenceContentTypeID());
            $queryBuilder = ContentService::getQueryBuilder($contentType->getAlias());
            $countContent = $queryBuilder->whereIn('_id', $value)->count();

            if($countContent < count($value)) {
                $errors[] = trans(self::MESSAGE_INVALID_REFERENCE_CONTENT, [ 'type' => $this->getDataType() ]);
                return false;
            }
        }
        return true;
    }


}