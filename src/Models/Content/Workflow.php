<?php

namespace LaunchCMS\Models\Content;


use LaunchCMS\Models\Content\DataObject\ContentTypeDefinition;
use LaunchCMS\Models\IdentityModel;

/**
 * Workflow is the concept that hold the whole workflow schema to control state transition of content
 * @package LaunchCMS\Models\Content
 */
class Workflow extends IdentityModel
{
    protected $collection = 'cms_workflows';
    public function getJSON() {
        return $this->json;
    }

    public function setJSON($json) {
        $this->json = $json;
    }

    public static function fromArray(array $info)
    {
        $workflow = new Workflow();
        self::updateGenericInfoFromArray($info, $workflow);
        if(isset($info['json'])) {
            $workflow->setJSON($info['json']);
        }
        return $workflow;
    }

    public function updateFromArray(array $info)
    {
        self::updateGenericInfoFromArray($info, $this);
        if(isset($info['json'])) {
            $this->setJSON($info['json']);
        }
    }
}