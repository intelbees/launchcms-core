<?php

namespace LaunchCMS\Models\Content;

use LaunchCMS\MongoDB\MongoHelper;
use LaunchCMS\Services\Exceptions\FieldGroupException;
use LaunchCMS\Services\Facades\StructureService;

/**
 * Class ContentType
 * @package LaunchCMS\Models\Content
 */
class ContentType extends Structure
{
    const DEV_FIELDS = 'dev_fields';
    const BUILTIN = 'builtin';
    const DEV_SETTINGS = 'dev_settings';
    const ENABLE_VERSIONING = 'enable_versioning';
    protected $collection = 'cms_content_types';
    protected $fieldGroupHash = [ ];

    protected $allFieldGroups = [ ];
    protected $allAncestors = [ ];


    public function isRootContentType()
    {
        $inheritPath = $this->getInheritPath();
        $ancestorIDs = explode(",", $inheritPath);

        return $this->_id == $ancestorIDs[ 0 ];
    }

    public function rootContentType()
    {
        $ancestors = $this->getAllAncestors();
        if ( !empty( $ancestors )) {
            return $ancestors[ 0 ];
        }

        return null;
    }

    public function parent()
    {
        return $this->belongsTo('LaunchCMS\Models\Content\ContentType', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('LaunchCMS\Models\Content\ContentType', 'children');
    }

    public function fieldGroups()
    {
        return $this->embedsMany('LaunchCMS\Models\Content\FieldGroup', 'field_groups');
    }


    public function setParentID($parentID)
    {
        $this->parent_id = $parentID;
    }


    public function getInheritPath()
    {
        return $this->inherit_path;
    }

    public function getWorkflowID() {
        return $this->workflow_id;
    }

    public function setWorkflowID($workflowID) {
        $this->workflow_id = $workflowID;
    }

    public function setInheritPath($inheritPath)
    {
        $this->inherit_path = $inheritPath;
    }

    public function setEnableVersioning($enable)
    {
        $this->enable_versioning = $enable;
    }

    public function getEnableVersioning()
    {
        return isset( $this->enable_versioning ) ? $this->enable_versioning : false;
    }

    public function setEnableLocalization($enable)
    {
        $this->enable_localization = $enable;
    }

    public function getEnableLocalization()
    {
        return isset( $this->enable_localization ) ? $this->enable_localization : false;
    }

    public function setPrimaryLocale($locale)
    {
        $this->primary_locale = $locale;
    }

    public function getPrimaryLocale()
    {
        return $this->primary_locale;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    

    /**
     * Check a group alias exist
     * @param $groupAlias
     * @return bool
     */
    public function isGroupExist($groupAlias)
    {
        $fieldGroups = $this->getFieldGroups();
        foreach ($fieldGroups as $fieldGroup) {
            if ($fieldGroup->alias == $groupAlias) {
                return true;
            }
        }

        return false;
    }

    public function getAllAncestors($forceRefresh = false)
    {
        if (empty( $this->allAncestors ) || $forceRefresh) {
            $inheritPath = $this->getInheritPath();
            $ancestorIDs = explode(",", $inheritPath);
            $this->allAncestors = ContentType::whereIn('_id', $ancestorIDs)->get();
        }

        return $this->allAncestors;
    }

    public function getFieldGroups($forceRefresh = false)
    {
        if (empty( $this->allFieldGroups ) || $forceRefresh) {
            $ancestors = $this->getAllAncestors();
            $result = [ ];
            foreach ($ancestors as $ancestor) {
                $fieldGroups = $ancestor->fieldGroups()->all();
                $result = array_merge($result, $fieldGroups);
            }
            usort($result, [ 'LaunchCMS\Models\Content\ContentType', 'positionSort' ]);
            $this->allFieldGroups = $result;

        }

        return $this->allFieldGroups;
    }

    public function getOwnFieldGroup($groupAlias)
    {
        $fieldGroups = $this->fieldGroups()->all();
        foreach ($fieldGroups as $fieldGroup) {
            if ($fieldGroup->getAlias() === $groupAlias) {
                return $fieldGroup;
            }
        }

        return null;
    }

    public function getAllFields($forceRefresh = false)
    {
        if (empty( $this->allFields ) || $forceRefresh) {
            $ancestors = $this->getAllAncestors();
            $result = [ ];
            foreach ($ancestors as $ancestor) {
                $fieldGroups = $ancestor->fields()->all();
                $result = array_merge($result, $fieldGroups);
            }
            $this->allFields = $result;
        }

        return $this->allFields;
    }


    private function buildFieldGroupHash()
    {
        $fieldGroups = $this->getFieldGroups(true);
        $this->fieldGroupHash = [ ];
        foreach ($fieldGroups as $fieldGroup) {
            $this->fieldGroupHash[ $fieldGroup->alias ] = $fieldGroup;
        }
    }


    public function getFieldGroupByAlias($alias, $forceRefreshHash = false)
    {
        if (empty( $this->fieldGroupHash ) || $forceRefreshHash) {
            $this->buildFieldGroupHash();
        }
        if (isset( $this->fieldGroupHash[ $alias ] )) {
            return $this->fieldGroupHash[ $alias ];
        }

        return null;
    }

    public static function positionSort($a, $b)
    {
        if ($a->position == $b->position) {
            return 0;
        }

        return ( $a->position < $b->position ) ? -1 : 1;
    }

    public function getFieldsOfGroup($fieldGroupAlias)
    {
        $fieldCollection = $this->getAllFields();
        $result = [ ];
        foreach ($fieldCollection as $field) {
            if ($field->getFieldGroup() === $fieldGroupAlias) {
                $result[] = $field;
            }
        }
        usort($result, [ 'LaunchCMS\Models\Content\ContentType', 'positionSort' ]);

        return $result;
    }


    public static function fromArray(array $info)
    {
        $contentType = new ContentType();
        self::updateGenericInfoFromArray($info, $contentType);
        if(!isset($contentType->attributes['dev_settings'])) {
            //this dev setting builtin field is the key field to recognize a content type is a built-in type or not
            //We will limit modification actions or will have different other logic processing for builtin content type
            $contentType->attributes['dev_settings'] = ['builtin' => false, 'allow_modify_meta_data' => true];
        }
        $workflowAlias = null;
        if (isset( $info[ 'workflow_id' ] )) {
            $workflowAlias = $info[ 'workflow_id' ];
        }

        $enableVersioning = true;
        if (isset( $info[ 'enable_versioning' ] )) {
            $enableVersioning = $info[ 'enable_versioning' ];
        }

        $enableLocalization = false;
        if (isset( $info[ 'enable_localization' ] )) {
            $enableLocalization = $info[ 'enable_localization' ];
        }

        $primaryLocale = false;
        if (isset( $info[ 'primary_locale' ] )) {
            $primaryLocale = $info[ 'primary_locale' ];
        }

        $template = null;
        if (isset( $info[ 'template' ] )) {
            $template = $info[ 'template' ];
        }
        $contentType->setEnableVersioning($enableVersioning);
        $contentType->setEnableLocalization($enableLocalization);
        $contentType->setTemplate($template);
        $contentType->setPrimaryLocale($primaryLocale);
        $contentType->setWorkflowID($workflowAlias);
        return $contentType;
    }

    public function updateFromArray(array $info)
    {
        self::updateGenericInfoFromArray($info, $this);
        if(!isset($this->attributes['dev_settings'])) {
            //this dev setting builtin field is the key field to recognize a content type is a built-in type or not
            //We will limit modification actions or will have different other logic processing for builtin content type
            $this->attributes['dev_settings'] = ['builtin' => false, 'allow_modify_meta_data' => true];
        }
        if (isset( $info[ 'enable_versioning' ] )) {
            $enableVersioning = $info[ 'enable_versioning' ];
            $this->setEnableVersioning($enableVersioning);
        }

        if (isset( $info[ 'enable_localization' ] )) {
            $enableLocalization = $info[ 'enable_localization' ];
            $this->setEnableLocalization($enableLocalization);
        }

        if (isset( $info[ 'primary_locale' ] )) {
            $primaryLocale = $info[ 'primary_locale' ];
            $this->setPrimaryLocale($primaryLocale);
        }


        if (isset( $info[ 'template' ] )) {
            $template = $info[ 'template' ];
            $this->setTemplate($template);
        }


        if (isset( $info[ 'workflow_id' ] )) {
            $this->setWorkflowID($info[ 'workflow_id' ]);
        } else {
            $this->setWorkflowID(null);
        }
    }

    public function addField(Field $field)
    {
        if (empty( $field )) {
            return;
        }
        $groupAlias = $field->getFieldGroup();
        if ( !empty( $groupAlias )) {
            $fieldGroup = $this->getFieldGroupByAlias($groupAlias);
            if (empty( $fieldGroup )) {
                throw FieldGroupException::fieldGroupNotFound();
            }
        }
        parent::addField($field);
    }

    public function updateField($fieldAlias, Field $field)
    {
        $updateResult = parent::updateField($fieldAlias, $field);
        $oldIndexValue = $updateResult[ 'oldIndexValue' ];
        $oldUniqueValue = $updateResult[ 'oldUniqueValue' ];
        /** @var Field $existingField */
        $existingField = $updateResult[ 'field' ];

        if ($existingField->getAlias() !== $fieldAlias) {
            $rootContentType = $this->rootContentType();
            $collectionName = $rootContentType->getAlias();
            MongoHelper::renameField($collectionName, $fieldAlias, $existingField->getAlias());

        }
        $collectionName = $this->rootContentType()->getAlias();
        if ($oldIndexValue !== $existingField->index()) {
            $existingField->updateIndexing($collectionName);
        }
        if ($oldUniqueValue != $existingField->isUnique()) {
            $existingField->updateUniqueIndexing($collectionName);
        }
        $this->fields()->save($existingField);
        return $existingField;
    }



    public function getDataTypesRelatedToEditableFields() {
        $editableFields = $this->getEditableFields();
        $dataTypeToPrepare = [];
        /** @var Field $editableField */
        foreach ($editableFields as $editableField) {
            if($editableField->getDataType() !== Field::EMBEDDED_LIST) {
                continue;
            }
            $dataTypeID = $editableField->getExtraDataTypeInfo()[Field::DATA_TYPE_ID];
            $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);
            if(!empty($dataType)) {
                $dataTypeToPrepare[] = $dataType;
            }
        }
        return $dataTypeToPrepare;
    }



}