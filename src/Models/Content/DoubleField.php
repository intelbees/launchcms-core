<?php


namespace LaunchCMS\Models\Content;


class DoubleField extends Field
{
    public function __construct()
    {
        $this->setDataType(self::DOUBLE);
    }

    public function validateValue($value, array &$errors)
    {
        if(empty($value)) {
            return true;
        }
        if ( !is_double($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);

            return false;
        }

        return true;
    }
}