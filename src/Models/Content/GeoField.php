<?php


namespace LaunchCMS\Models\Content;

use LaunchCMS\MongoDB\MongoHelper;

class GeoField extends Field
{
    const LNG = 'lng';
    const LAT = 'lat';

    public function __construct()
    {
        $this->setDataType(self::GEO);
    }

    public static function createLocationData($lat, $lng) {
        return [
            'type' => 'Point',
            'coordinates' => [self::LNG => floatval($lng), self::LAT => floatval($lat)]
        ];
    }

    public function validateValue($value, array &$errors)
    {
        if (empty( $value )) {
            return true;
        }
        if ( !is_array($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }
        if(!isset($value['type']) || $value['type'] !== 'Point') {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }
        if(!isset($value['coordinates']) || empty($value['coordinates']) || count($value['coordinates']) !== 2) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }
        //Following MongoDB spec: Coordinate-axis order is longitude, latitude. Longitude is the first value
        $isValidLngData = (isset($value['coordinates'][self::LNG]) && is_float($value['coordinates'][self::LNG])) ||
            is_float($value['coordinates'][0]);

        $isValidLatData = (isset($value['coordinates'][self::LAT]) && is_float($value['coordinates'][self::LAT])) ||
            is_float($value['coordinates'][1]);


        if(!$isValidLatData || !$isValidLngData) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }

        return true;
    }


    public function formatToIndexedData($value)
    {
        if ($value == null) {
            return null;
        }

        return [ "location" => [
            "lat" => $value[ self::LAT ],
            "lon" => $value[ self::LNG ],
            ]
        ];
    }

    public function makeIndexing($collectionName, $embeddedFieldAlias = null) {
        $field = $this;
        MongoHelper::getMongoDBSchema()->collection($collectionName, function ($collection) use ($field, $embeddedFieldAlias) {
            $fieldAlias = $field->getAlias();
            if(isset($embeddedFieldAlias)) {
                $fieldAlias = $embeddedFieldAlias.'.'.$fieldAlias;
            }
            $collection->index([$fieldAlias => '2dsphere']);
        });
    }
}