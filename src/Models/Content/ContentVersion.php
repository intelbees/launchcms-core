<?php


namespace LaunchCMS\Models\Content;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\DataObject\ContentTypeDefinition;


class ContentVersion extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'cms_content_versions';

    public function contentType()
    {
        return $this->belongsTo('LaunchCMS\Models\Content\ContentType', 'content_type_id');
    }

    public function setContentTypeID($contentTypeID)
    {
        $this->content_type_id = $contentTypeID;
    }

    public function getContentID()
    {
        return $this->content_id;
    }

    public function setContentID($contentID)
    {
        $this->content_id = $contentID;
    }


    public function getVersionNumber()
    {
        return $this->versionNumber;
    }

    public function setVersionNumber($versionNumber)
    {
        $this->versionNumber = $versionNumber;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public static function fromContentData(Content $content)
    {
        $contentVersion = new ContentVersion();
        $data = $content->toMongoObjectArray();
        $dataJSON = json_encode($data);
        $contentVersion->setData($dataJSON);
        $contentVersion->setContentID((string) $content->id);
        $contentVersion->contentTypeID = $content->contentTypeID;
        $contentVersion->setVersionNumber($content->lastVersionNumber + 1);

        return $contentVersion;
    }

}