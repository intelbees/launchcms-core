<?php


namespace LaunchCMS\Models\Content;


class IntegerField extends Field
{
    public function __construct()
    {
        $this->setDataType(self::INTEGER);
        $this->addValidationRule([ self::INTEGER ]);
    }

    public function validateValue($value, array &$errors)
    {
        if(empty($value)) {
            return true;
        }
        if ( !is_int($value)) {
            $errors[] = trans('launchcms.validation.invalid_data_type', [ 'type' => $this->getDataType() ]);

            return false;
        }

        return true;
    }
    
    
}