<?php

namespace LaunchCMS\Models\Content\DataObject;


use Carbon\Carbon;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\Helper\FieldFactory;
use LaunchCMS\Services\Facades\StructureService;
use LaunchCMS\Services\Interfaces\WorkflowObjectInterface;
use LaunchCMS\Utils\DateUtil;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;

class Content extends BaseContentModel implements \Serializable, WorkflowObjectInterface
{
    const VISIBILITY_ANYONE = 'anyone';
    const VISIBILITY_SPECIFIC_USERS = 'users';
    const VISIBILITY_SPECIFIC_GROUPS = 'groups';
    const VISIBILITY_SPECIFIC_ROLES = 'roles';
    public static $builtinField = [ '_id', 'name', 'last_version_number', 'workflow_status', 'mapped_system_status',
        'workflow_data', 'content_type_path', 'content_type_id', 'created_user_id', 'created_at', 'updated_at', 'trashed',
        'last_version_number', 'visibility', 'visible_users', 'visible_groups', 'visible_roles' ];


    public static function isBuiltInField($fieldAlias)
    {
        return in_array($fieldAlias, self::$builtinField);
    }


    public $id;
    public $name;


    public $contentTypePath;
    public $contentTypeID;
    public $createdUserID;
    public $createdTime;
    public $lastUpdatedTime;
    public $lastVersionNumber = 0;
    public $trashed = false;
    public $visibility = self::VISIBILITY_ANYONE;
    public $visibleUsers = [ ];
    public $visibleGroups = [ ];
    public $visibleRoles = [ ];
    public $fieldData = [ ];
    protected $workflowStatus;
    protected $mappedSystemStatus;
    protected $workflowData;

    public function toMongoObjectArray()
    {
        $result = [ ];

        $result[ 'name' ] = $this->name;
        $result[ 'last_version_number' ] = $this->lastVersionNumber;
        $result[ 'workflow_status' ] = $this->workflowStatus;
        $result[ 'mapped_system_status' ] = $this->mappedSystemStatus;

        $result[ 'content_type_path' ] = $this->contentTypePath;
        $result[ 'content_type_id' ] = $this->contentTypeID;
        $result[ 'created_user_id' ] = $this->createdUserID;
        $result[ 'created_at' ] = DateUtil::fromDateTime($this->createdTime);
        $result[ 'updated_at' ] = DateUtil::fromDateTime($this->lastUpdatedTime);
        $result[ 'trashed' ] = $this->trashed;
        $result[ 'visible_users' ] = $this->visibleUsers;
        $result[ 'visible_groups' ] = $this->visibleGroups;
        $result[ 'visible_roles' ] = $this->visibleRoles;
        $result[ 'visibility' ] = $this->visibility;
        foreach ($this->attributes as $fieldKey => $fieldValue) {
            if ($fieldValue instanceof Carbon) {
                $fieldValue = DateUtil::fromDateTime($fieldValue);
            }
            $result[ $fieldKey ] = $fieldValue;
        }
        if($this->workflowData != null) {
            foreach ($this->workflowData as $key => $value) {
                $persistValue = $value;
                if ($value instanceof Carbon) {
                    $persistValue = DateUtil::fromDateTime($value);
                }
                $this->workflowData[ $key ] = $persistValue;
            }
            $result[ 'workflow_data' ] = $this->workflowData;
        } else {
            $result[ 'workflow_data' ] = null;
        }


        return $result;
    }

    public static function fromArray(array $info)
    {
        $result = new Content();
        if (isset( $info[ '_id' ] )) {
            $result->id = $info[ '_id' ];
        }
        if (isset( $info[ 'name' ] )) {
            $result->name = $info[ 'name' ];
        }

        if (isset( $info[ 'last_version_number' ] )) {
            $result->lastVersionNumber = $info[ 'last_version_number' ];
        }
        if (isset( $info[ 'workflow_status' ] )) {
            $result->workflowStatus = $info[ 'workflow_status' ];
        }

        if (isset( $info[ 'mapped_system_status' ] )) {
            $result->mappedSystemStatus = $info[ 'mapped_system_status' ];
        }

        if (isset( $info[ 'workflow_data' ] )) {
            $result->workflowData = $info[ 'workflow_data' ];
            foreach ($result->workflowData as $key => $value) {
                $persistValue = $value;
                if ($value instanceof UTCDatetime) {
                    $persistValue = DateUtil::asDateTime($value);
                }
                $result->workflowData[ $key ] = $persistValue;
            }
        }
        if (isset( $info[ 'content_type_path' ] )) {
            $result->contentTypePath = $info[ 'content_type_path' ];
        }
        if (isset( $info[ 'content_type_id' ] )) {
            $result->contentTypeID = $info[ 'content_type_id' ];
        }
        if (isset( $info[ 'created_user_id' ] )) {
            $result->createdUserID = $info[ 'created_user_id' ];
        }
        if (isset( $info[ 'created_at' ] )) {
            $result->createdTime = DateUtil::asDateTime($info[ 'created_at' ]);
        }
        if (isset( $info[ 'updated_at' ] )) {
            $result->lastUpdatedTime = DateUtil::asDateTime($info[ 'updated_at' ]);
        }
        if (isset( $info[ 'trashed' ] )) {
            $result->trashed = $info[ 'trashed' ];
        }

        if (isset( $info[ 'visibility' ] )) {
            $result->visibility = $info[ 'visibility' ];
        }

        if (isset( $info[ 'visible_users' ] )) {
            $result->visibleUsers = $info[ 'visible_users' ];
        }

        if (isset( $info[ 'visible_groups' ] )) {
            $result->visibleGroups = $info[ 'visible_groups' ];
        }

        if (isset( $info[ 'visible_roles' ] )) {
            $result->visibleRoles = $info[ 'visible_roles' ];
        }
        $result->attributes = [ ];
        $contentType = StructureService::getContentTypeFromCacheByID($result->contentTypeID);
        foreach ($info as $key => $value) {
            if ( !in_array($key, self::$builtinField)) {
                $field = $contentType->getField($key);
                $concreteField = null;
                if ( !empty( $field )) {
                    $concreteField = FieldFactory::getConcreteField($field);
                }
                if ( !empty( $concreteField )) {
                    $result->attributes[ $key ] = $concreteField->loadDataToContent($value);
                } else {
                    $result->attributes[ $key ] = $value;
                }
            }
        }

        return $result;
    }

    public function serialize()
    {

        $dataToSerialize = [ 'name'                 => $this->name,
                             '_id'                  => (string) $this->id,
                             'last_version_number'  => $this->lastVersionNumber,
                             'workflow_status'      => $this->workflowStatus,
                             'mapped_system_status' => $this->mappedSystemStatus,
                             'workflow_data'        => $this->workflowData,
                             'content_type_path'    => $this->contentTypePath,
                             'content_type_id'      => $this->contentTypeID,
                             'created_user_id'      => $this->createdUserID,
                             'created_at'           => $this->createdTime,
                             'updated_at'           => $this->lastUpdatedTime,
                             'trashed'              => $this->trashed,
                             'visibility'           => $this->visibility,
                             'visible_users'        => $this->visibleUsers,
                             'visible_groups'       => $this->visibleGroups,
                             'visible_roles'        => $this->visibleRoles,

        ];
        foreach ($this->attributes as $fieldKey => $fieldValue) {

            $dataToSerialize[ $fieldKey ] = $fieldValue;
        }

        return serialize($dataToSerialize);
    }

    public function unserialize($serialized)
    {
        $info = unserialize($serialized);

        if (isset( $info[ '_id' ] )) {
            $this->attributes[ '_id' ] = new ObjectID($info[ '_id' ]);
            $this->id = new ObjectID($info[ '_id' ]);
        }
        if (isset( $info[ 'name' ] )) {
            $this->name = $info[ 'name' ];
        }

        if (isset( $info[ 'last_version_number' ] )) {
            $this->lastVersionNumber = $info[ 'last_version_number' ];
        }
        if (isset( $info[ 'workflow_status' ] )) {
            $this->workflowStatus = $info[ 'workflow_status' ];
        }

        if (isset( $info[ 'mapped_system_status' ] )) {
            $this->mappedSystemStatus = $info[ 'mapped_system_status' ];
        }
        if (isset( $info[ 'workflow_data' ] )) {
            $this->workflowData = $info[ 'workflow_data' ];
            if (isset( $info[ 'workflow_data' ] )) {
                $this->workflowData = $info[ 'workflow_data' ];
                foreach ($this->workflowData as $key => $value) {
                    $persistValue = $value;
                    if ($value instanceof UTCDatetime) {
                        $persistValue = DateUtil::asDateTime($value);
                    }
                    $this->workflowData[ $key ] = $persistValue;
                }
            }
        }
        if (isset( $info[ 'content_type_path' ] )) {
            $this->contentTypePath = $info[ 'content_type_path' ];
        }
        if (isset( $info[ 'content_type_id' ] )) {
            $this->contentTypeID = $info[ 'content_type_id' ];
        }
        if (isset( $info[ 'created_user_id' ] )) {
            $this->createdUserID = $info[ 'created_user_id' ];
        }
        if (isset( $info[ 'created_at' ] )) {
            $this->createdTime = DateUtil::asDateTime($info[ 'created_at' ]);
        }
        if (isset( $info[ 'updated_at' ] )) {
            $this->lastUpdatedTime = DateUtil::asDateTime($info[ 'updated_at' ]);
        }
        if (isset( $info[ 'trashed' ] )) {
            $this->trashed = $info[ 'trashed' ];
        }
        if (isset( $info[ 'visibility' ] )) {
            $this->visibility = $info[ 'visibility' ];
        }

        if (isset( $info[ 'visible_users' ] )) {
            $this->visibleUsers = $info[ 'visible_users' ];
        }

        if (isset( $info[ 'visible_groups' ] )) {
            $this->visibleGroups = $info[ 'visible_groups' ];
        }

        if (isset( $info[ 'visible_roles' ] )) {
            $this->visibleRoles = $info[ 'visible_roles' ];
        }

        $this->attributes = [ ];
        $contentType = StructureService::getContentTypeFromCacheByID($this->contentTypeID);
        foreach ($info as $key => $value) {

            if ( !in_array($key, self::$builtinField)) {
                $field = $contentType->getField($key);
                $concreteField = FieldFactory::getConcreteField($field);
                if ( !empty( $concreteField )) {
                    $this->attributes[ $key ] = $concreteField->loadDataToContent($value);
                } else {
                    $this->attributes[ $key ] = $value;
                }
            }
        }
    }

    public function getIndexData()
    {
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByID($this->contentTypeID);
        $allFields = $contentType->getAllFields();
        $createdAt = DateUtil::asDateTime($this->createdTime);
        $updatedAt = DateUtil::asDateTime($this->lastUpdatedTime);
        $data = [
            'name'                 => $this->name,
            'last_version_number'  => $this->lastVersionNumber,
            'workflow_status'      => $this->workflowStatus,
            'mapped_system_status' => $this->mappedSystemStatus,
            'content_type_path'    => $this->contentTypePath,
            'content_type_id'      => $this->contentTypeID,
            'created_user_id'      => $this->createdUserID,
            'created_at'           => DateUtil::formatDateTimeToIndexedFormat($createdAt),
            'updated_at'           => DateUtil::formatDateTimeToIndexedFormat($updatedAt),
            'trashed'              => $this->trashed,
            'visibility'           => $this->visibility,
            'visible_users'        => $this->visibleUsers,
            'visible_groups'       => $this->visibleGroups,
            'visible_roles'        => $this->visibleRoles,
        ];
        if(!empty($this->attributes)) {
            foreach ($allFields as $field) {
                if ($field->allowFullTextSearch()) {
                    $fieldValue = $this->attributes[ $field->getAlias() ];
                    $concreteField = FieldFactory::getConcreteField($field);
                    $data[ $field->getAlias() ] = $concreteField->formatToIndexedData($fieldValue);
                }
            }
        }
        return $data;
    }

    public function getWorkflowStatus()
    {
        return $this->workflowStatus;
    }

    public function setWorkflowStatus($state)
    {
        $this->workflowStatus = $state;
    }

    public function getMappedSystemStatus()
    {
        return $this->mappedSystemStatus;
    }

    public function setMappedSystemStatus($systemState)
    {
        $this->mappedSystemStatus = $systemState;
    }

    public function getWorkflowData() {
        return $this->workflowData;
    }

    public function setWorkflowData($workflowData) {
        $this->workflowData  = $workflowData;
    }

    public function getWorkflowObjectIdentifer() {
        return $this->contentTypeID.'_'.$this->id;
    }

    public function getWorkflowDataAttribute($attributeKey) {
        $workflowData = $this->getWorkflowData();
        return isset( $workflowData[$attributeKey] ) ? $workflowData[$attributeKey]  : [];
    }

    public function setWorkflowDataAttribute($attributeKey, $value) {
        if(!isset($this->attributes[ 'workflow_data' ])) {
            $this->attributes[ 'workflow_data' ] = [];
        }
        $this->attributes[ 'workflow_data' ][$attributeKey] = $value;
    }


}