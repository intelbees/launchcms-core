<?php

namespace LaunchCMS\Models\Content\DataObject;


use LaunchCMS\Utils\StringUtil;

class BaseContentModel
{

    /**
     * The model's attributes.
     *
     * @var array
     */
    public $attributes = [];

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        if(!isset($this->attributes[$key])) {
            return null;
        }
        return $this->attributes[$key];
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function attributeValue($key) {
        if(!StringUtil::contains($key, '.')) {
            return isset($this->attributes[$key]) ? $this->attributes[$key] : null;
        }
        $subKeys = explode('.', $key);
        $count = count($subKeys);
        $value = null;
        $subKey = $subKeys[0];
        $value = isset($this->attributes[$subKey]) ? $this->attributes[$subKey]: null;
        if($value == null || !is_array($value)) {
            return $value;
        }
        for($keyIndex = 1; $keyIndex < $count; $keyIndex++) {
            $subKey = $subKeys[$keyIndex];
            $value = isset($value[$subKey]) ? $value[$subKey] : null;
            $shouldStop = ($value === null) || !is_array($value);

            if($shouldStop) {
                return $value;
            }
        }

        return null;
    }


}