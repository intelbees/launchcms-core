<?php


namespace LaunchCMS\Models\Content;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Utils\DateUtil;
use InvalidArgumentException;

class DateTimeField extends Field
{
    public function __construct()
    {
        $this->setDataType(self::DATETIME);
    }

    public function loadDataToContent($value)
    {
        if($value == null) {
            return null;
        }
        return DateUtil::asDateTime($value);
    }

    public function validateValue($value, array &$errors)
    {
        if(empty($value)) {
            return true;
        }
        try {
            DateUtil::asDateTime($value);
        } catch (InvalidArgumentException $ex) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);

            return false;
        }

        return true;
    }

    public function convertToStorableData($value) {
        if($value != null) {
            return  DateUtil::fromDateTime($value);
        }
        return null;
    }

    public function processFieldDataBeforeSaving(Content &$content, $fieldDataAlias, $fieldData)
    {
        $content->attributes[ $fieldDataAlias ] = $this->convertToStorableData($fieldData);
    }

    public function formatToIndexedData($value) {
        if($value == null) {
            return null;
        }
        $date = $value;
        if($value instanceof UTCDatetime) {
            $date = DateUtil::asDateTime($value);
        }
        return DateUtil::formatDateTimeToIndexedFormat($date);
    }
}