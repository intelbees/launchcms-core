<?php

namespace LaunchCMS\Models\Content\Workflow;


interface WorkflowUserInterface
{
    function getEmail();
    function getRoleSlugs();
}