<?php


namespace LaunchCMS\Models\Content;

use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\IdentityModel;
use LaunchCMS\MongoDB\MongoHelper;
use LaunchCMS\RuntimeSetting;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Utils\StringUtil;

class Field extends IdentityModel
{

    protected static $unguarded = true;
    const STRING = 'string';
    const BOOLEAN = 'boolean';
    const DATE = 'date';
    const GEO = 'geo';
    const MEDIA = 'media';
    const DATETIME = 'datetime';
    const DOUBLE = 'double';
    const INTEGER = 'integer';
    const EMBEDDED = 'embedded';
    const ARRAY_FIELD = 'array';
    const ARRAY_ELEMENT_TYPE = 'element_type';
    const EMBEDDED_LIST = 'embedded_list';
    const REFERENCE = 'reference';
    const REFERENCE_LIST = 'reference_list';

    const DATA_TYPE_ID = 'data_type_id';
    const CONTENT_TYPE_ID = 'content_type_id';

    const MESSAGE_VALUE_NOT_MATCH_DATA_TYPE = 'launchcms.validation.invalid_data_type';
    protected static $supported_validation_rules = [
        'boolean',
        'string',
        'digits',
        'digits_between',
        'email',
        'integer',
        'json',
        'ip',
        'regex',
        'max',
        'min',
        'required',
        'required_if',
        'required_unless',
        'required_with_all',
        'required_without',
        'required_without_all',
        'same',
        'size',
        'url',
        'not_in',
        'different' ];

    public function isUnique()
    {
        return $this->unique;
    }

    public function setUnique($unique)
    {
        $this->unique = $unique;
    }

    public function index()
    {
        // here we need to use another name instead of "index",
        // because index seems a reserved field of Laravel MongoDB library
        return $this->indexable;
    }

    public function setIndex($index)
    {
        // here we need to use another name instead of "index",
        // because index seems a reserved field of Laravel MongoDB library
        return $this->indexable = $index;
    }

    public function allowFullTextSearch() {
        return isset($this->attributes['allow_full_text_search']) ? $this->attributes['allow_full_text_search']: false;
    }

    public function setAllowFullTextSearch($allow) {
        $this->attributes['allow_full_text_search'] = $allow;
    }

    public function getFieldGroup()
    {
        return $this->field_group;
    }

    public function setFieldGroup($fieldGroup)
    {
        $this->field_group = $fieldGroup;
    }



    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function isRequired()
    {
        return $this->required;
    }

    public function setRequired($required)
    {
        $this->required = $required;
    }

    public function getDataType()
    {
        return $this->data_type;
    }

    public function setDataType($dataType)
    {
        $this->data_type = $dataType;
    }

    public function getExtraDataTypeInfo()
    {
        return $this->extra_data_type_info;
    }

    public function setExtraDataTypeInfo($extraDataTypeInfo)
    {
        $this->extra_data_type_info = $extraDataTypeInfo;
    }

    public function setValidationRules(array $validationRules)
    {
        foreach ($validationRules as $rule) {
            $ruleParts = explode(':', $rule);
            $rule = trim($rule);
            if (count($ruleParts) == 0) {
                throw FieldException::invalidValidationRule($rule);
            }
            $rulePrefix = $ruleParts[ 0 ];
            if ( !in_array($rulePrefix, self::$supported_validation_rules)) {
                throw FieldException::invalidValidationRule($rule);
            }
        }
        $this->validation_rules = json_encode($validationRules);
    }

    public function getValidationRules()
    {
        if ( !empty( $this->validation_rules )) {
            return json_decode($this->validation_rules, true);
        }

        return [ ];
    }


    public function updateFromField(Field $fieldInfo)
    {
        if (isset( $fieldInfo->attributes[ 'name' ] )) {
            $this->setName($fieldInfo->getName());
        }
        if (isset( $fieldInfo->attributes[ 'alias' ] )) {
            $this->setAlias($fieldInfo->getAlias());
        }
        if (isset( $fieldInfo->attributes[ 'description' ] )) {
            $this->setDescription($fieldInfo->getDescription());
        }
        if (isset( $fieldInfo->attributes[ 'position' ] )) {
            $this->setPosition($fieldInfo->getPosition());
        }
        if (isset( $fieldInfo->attributes[ 'indexable' ] )) {
            $this->setIndex($fieldInfo->index());
        }
        if(isset($fieldInfo->attributes['allow_full_text_search'])) {
            $this->setAllowFullTextSearch($fieldInfo->attributes['allow_full_text_search']);
        }
        if (isset( $fieldInfo->attributes[ 'unique' ] )) {
            $this->setUnique($fieldInfo->isUnique());
        }

        if (isset( $fieldInfo->attributes[ 'required' ] )) {
            $this->setRequired($fieldInfo->isRequired());
        }

        if (isset( $fieldInfo->attributes[ 'data_type' ] )) {
            $this->setDataType($fieldInfo->getDataType());
        }

        if (isset( $fieldInfo->attributes[ 'extra_data_type_info' ] )) {
            $this->setExtraDataTypeInfo($fieldInfo->getExtraDataTypeInfo());
        }

        if (isset( $fieldInfo->attributes[ 'field_group' ] )) {
            $this->setFieldGroup($fieldInfo->getFieldGroup());
        }

        if (isset( $fieldInfo->attributes[ 'validation_rules' ] )) {
            $this->setValidationRules($fieldInfo->getValidationRules());
        }

        if (isset( $fieldInfo->attributes[ 'dev_settings' ] )) {
            $this->setDevSettings($fieldInfo->attributes[ 'dev_settings' ] );
        }

    }

    public function addValidationRule(array $validationRules)
    {
        $existValidationRules = $this->getValidationRules();
        $mergeValidationRules = array_merge($existValidationRules, $validationRules);
        $this->attributes[ 'validation_rules' ] = json_encode($mergeValidationRules);
    }

    public function loadDataToContent($value)
    {
        return $value;
    }

    public function convertToStorableData($value) {
        return $value;
    }
    
    public function processFieldDataBeforeSaving(Content &$content, $fieldDataAlias, $fieldData)
    {

    }

    public function validateValue($value, array &$errors)
    {
        return true;
    }

    public function formatToIndexedData($value) {
        return $value;
    }

    public function validateFieldMetaData() {
        $alias = $this->getAlias();
        if (empty( $alias )) {
            throw FieldException::cannotSetEmptyFieldAlias();
        }
        if(RuntimeSetting::$runningMode === RuntimeSetting::LIVE) {
            if (Content::isBuiltInField($alias) || !StringUtil::isValidVariableName($alias)) {
                throw FieldException::invalidFieldAlias();
            }
        }

        return true;
    }

    public function makeIndexing($collectionName, $embeddedFieldAlias = null) {
        $field = $this;
        MongoHelper::getMongoDBSchema()->collection($collectionName, function ($collection) use ($field, $embeddedFieldAlias) {
            $fieldAlias = $field->getAlias();
            if(isset($embeddedFieldAlias)) {
                $fieldAlias = $embeddedFieldAlias.'.'.$fieldAlias;
            }
            if($field->isUnique()) {
                $collection->unique($fieldAlias);
            }
            else if($field->index()) {
                $collection->index($fieldAlias);
            }
        });
    }

    public function updateIndexing($collectionName, $embeddedFieldAlias = null) {
        $existingField = $this;
        MongoHelper::getMongoDBSchema()->collection($collectionName, function($collection) use ($existingField, $embeddedFieldAlias) {
            $fieldAlias = $existingField->getAlias();
            if(isset($embeddedFieldAlias)) {
                $fieldAlias = $embeddedFieldAlias.'.'.$fieldAlias;
            }
            if ($existingField->index()) {
                try {
                    $collection->index($fieldAlias);
                } catch (\RuntimeException $rte) {
                    throw FieldException::cannotMakeIndex();
                }
            } else {
                try {
                    $collection->dropIndex($fieldAlias);
                } catch (\RuntimeException $rte) {
                    throw FieldException::cannotDropIndex();
                }
            }
        });
    }

    public function updateUniqueIndexing($collectionName, $embeddedFieldAlias = null) {
        $existingField = $this;
        MongoHelper::getMongoDBSchema()->collection($collectionName, function($collection) use ($existingField, $embeddedFieldAlias) {
            $fieldAlias = $existingField->getAlias();
            if(isset($embeddedFieldAlias)) {
                $fieldAlias = $embeddedFieldAlias.'.'.$fieldAlias;
            }
            if ($existingField->isUnique()) {
                try {
                    $collection->unique($fieldAlias);
                } catch (\RuntimeException $rte) {
                    throw  FieldException::cannotMakeUniqueIndex();
                }
            } else {
                try {
                    $collection->dropIndex($fieldAlias);
                } catch (\RuntimeException $rte) {
                    throw FieldException::cannotDropIndex();
                }
            }
        });
    }

}