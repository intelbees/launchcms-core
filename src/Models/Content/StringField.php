<?php


namespace LaunchCMS\Models\Content;


class StringField extends Field
{
    public function __construct()
    {
        $this->setDataType(self::STRING);
        $this->addValidationRule([ self::STRING ]);
    }

    public function validateValue($value, array &$errors)
    {
        if($value === null) {
            return true;
        }
        if ( !is_string($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);

            return false;
        }

        return true;
    }
}