<?php

namespace LaunchCMS\Models\Content;



use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\IdentityModel;
use LaunchCMS\RuntimeSetting;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Utils\StringUtil;


abstract class Structure extends IdentityModel
{
    const EDITABLE_FIELDS = 'editable_fields';
    const TABLE_DISPLAY_FIELDS = 'table_display_fields';
    const FILTER_FIELDS = 'filter_fields';
    protected static $builtinCollection = [ 'cms_users', 'cms_content_types', 'cms_content_versions', 'cms_persistences',
        'cms_roles', 'cms_throttles', 'cms_activations', 'cms_sites', 'cms_groups', 'cms_comments', 'cms_workflows' ];

    protected $allFields = [ ];

    public static function isBuiltInCollection($alias)
    {
        return in_array($alias, self::$builtinCollection);
    }

    protected $connection = 'mongodb';

    public function fields()
    {
        return $this->embedsMany('LaunchCMS\Models\Content\Field', 'fields');
    }

    public function validateAlias()
    {
        return parent::validateAlias() && self::isBuiltInCollection($this->alias);
    }

    public function getAllFields($forceRefresh = false)
    {
        if (empty( $this->allFields ) || $forceRefresh) {
            $this->allFields = $this->fields()->all();
        }

        return $this->allFields;
    }

    public function getField($fieldAlias)
    {
        $fields = $this->getAllFields();
        foreach ($fields as $field) {
            if ($field->getAlias() === $fieldAlias) {
                return $field;
            }
        }

        return null;
    }

    public function getOwnField($fieldAlias)
    {
        $fields = $this->fields()->all();
        foreach ($fields as $field) {
            if ($field->getAlias() === $fieldAlias) {
                return $field;
            }
        }

        return null;
    }

    public function isFieldExist($fieldAlias)
    {
        $fieldCollection = $this->getAllFields();
        foreach ($fieldCollection as $field) {
            if ($field->getAlias() === $fieldAlias) {
                return true;
            }
        }

        return false;
    }

    protected static function fieldSort($a, $b)
    {
        if ($a->position == $b->position) {
            return 0;
        }

        return ( $a->position < $b->position ) ? -1 : 1;
    }

    public function getSettings() {
        return isset($this->attributes['settings']) ? $this->attributes['settings']:[];
    }

    public function setSettings($settings) {
        $this->settings = $settings;
    }

    public function getFieldValidationRules()
    {
        $allFields = $this->getAllFields();
        $ruleArray = [ ];
        foreach ($allFields as $field) {
            $rules = $field->getValidationRules();
            $ruleArray[ $field->getAlias() ] = $rules;
        }

        return $ruleArray;
    }

    public function addField(Field $field)
    {
        $field->validateFieldMetaData();
        $alias = $field->getAlias();
        if(RuntimeSetting::$runningMode === RuntimeSetting::LIVE) {
            if (isset( $this->getDevSettings()[ ContentType::DEV_FIELDS ] )) {
                if (in_array($alias, $this->getDevSettings()[ ContentType::DEV_FIELDS ])) {
                    throw FieldException::invalidFieldAlias();
                }
            }
        }
        $existingField = $this->getField($alias);
        if ( !empty( $existingField )) {
            throw FieldException::duplicatedField();
        }

        $dataType = $field->getDataType();
        if (empty( $dataType )) {
            throw FieldException::invalidFieldDataType();
        }
        if(!isset($field->attributes['dev_settings'])) {
            $field->attributes['dev_settings'] = ['builtin' => false];
        }
        $this->fields()->save($field);
    }

    public function updateField($fieldAlias, Field $field)
    {
        if (empty( $fieldAlias )) {
            throw FieldException::cannotSetEmptyFieldAlias();
        }
        /** @var Field $existingField */
        $existingField = $this->getOwnField($fieldAlias);

        if (empty( $existingField )) {
            throw FieldException::fieldNotFound();
        }
        $oldIndexValue = $existingField->index();
        $oldUniqueValue = $existingField->isUnique();
        $existingField->updateFromField($field);
        $newAlias = $existingField->getAlias();
        if (empty( $newAlias )) {
            throw FieldException::cannotSetEmptyFieldAlias();
        }
        $violateBuiltInFieldRule = !$existingField->isBuiltInType() && Content::isBuiltInField($newAlias);
        $violateInvalidAliasName = !StringUtil::isValidVariableName($newAlias);
        if ($violateInvalidAliasName || $violateBuiltInFieldRule) {
            throw FieldException::invalidFieldAlias();
        }
        $foundFieldWithNewAlias = $this->getField($newAlias);
        if ( !empty( $foundFieldWithNewAlias ) && $foundFieldWithNewAlias->_id !== $existingField->_id) {
            throw FieldException::duplicatedField();
        }

        return [ 'oldIndexValue' => $oldIndexValue, 'oldUniqueValue' => $oldUniqueValue, 'field' => $existingField ];
    }

    
    public function getEditableFields() {
        $settings = $this->getSettings();
        $editableFieldAliases = isset( $settings[ self::EDITABLE_FIELDS ] ) ? $settings[ self::EDITABLE_FIELDS ] : [ ];
        $editableFields = $this->getAllFields();
        if(!empty($editableFieldAliases)) {
            $editableFields = [];
            foreach ($editableFieldAliases as $editableFieldAlias){
                $editableFields []= $this->getField($editableFieldAlias);
            }
        }
        return $editableFields;
    }
}