<?php

namespace LaunchCMS\Models\Content\Helper;
use LaunchCMS\Models\Content\ArrayField;
use LaunchCMS\Models\Content\BooleanField;
use LaunchCMS\Models\Content\DateField;
use LaunchCMS\Models\Content\DateTimeField;
use LaunchCMS\Models\Content\DoubleField;
use LaunchCMS\Models\Content\EmbeddedField;
use LaunchCMS\Models\Content\EmbeddedListField;
use LaunchCMS\Models\Content\Field;
use LaunchCMS\Models\Content\GeoField;
use LaunchCMS\Models\Content\IntegerField;
use LaunchCMS\Models\Content\ReferenceField;
use LaunchCMS\Models\Content\ReferenceListField;
use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Exceptions\DataTypeException;
use LaunchCMS\Services\Exceptions\FieldException;

class FieldFactory
{

    public static function getConcreteField(Field $fieldInfo) {
        $dataType = $fieldInfo->getDataType();
        if(empty($dataType)) {
            throw CMSServiceException::invalidFieldType();
        }

        $extraDataTypeInfo = $fieldInfo->getExtraDataTypeInfo();
        $field = null;
        if($dataType === Field::STRING) {
            $field = new StringField();
        }
        else if($dataType === Field::BOOLEAN) {
            $field = new BooleanField();
        }
        else if($dataType === Field::DATE) {
            $field = new DateField();
        }
        else if($dataType === Field::DATETIME) {
            $field = new DateTimeField();
        }
        else if($dataType === Field::INTEGER) {
            $field = new IntegerField();
        }
        else if($dataType === Field::DOUBLE) {
            $field = new DoubleField();
        }
        else if($dataType === Field::REFERENCE) {
            if(empty($extraDataTypeInfo) || !isset($extraDataTypeInfo[Field::CONTENT_TYPE_ID])) {
                throw FieldException::invalidReferenceContentType();
            }
            $field = new ReferenceField($extraDataTypeInfo[Field::CONTENT_TYPE_ID]);
        }
        else if($dataType === Field::REFERENCE_LIST) {
            if(empty($extraDataTypeInfo) || !isset($extraDataTypeInfo[Field::CONTENT_TYPE_ID])) {
                throw FieldException::invalidReferenceContentType();
            }
            $field = new ReferenceListField($extraDataTypeInfo[Field::CONTENT_TYPE_ID]);
        }
        else if($dataType === Field::EMBEDDED) {
            if(empty($extraDataTypeInfo) || !isset($extraDataTypeInfo[Field::DATA_TYPE_ID])) {
                throw DataTypeException::invalidFieldType();
            }
            $field = new EmbeddedField($extraDataTypeInfo[Field::DATA_TYPE_ID]);
        }
        else if($dataType === Field::EMBEDDED_LIST) {
            if(empty($extraDataTypeInfo) || !isset($extraDataTypeInfo[Field::DATA_TYPE_ID])) {
                throw DataTypeException::invalidFieldType();
            }
            $field = new EmbeddedListField($extraDataTypeInfo[Field::DATA_TYPE_ID]);
        }
        else if($dataType === Field::ARRAY_FIELD) {
            if(empty($extraDataTypeInfo) || !isset($extraDataTypeInfo[Field::ARRAY_ELEMENT_TYPE])) {
                throw FieldException::invalidArrayElementType();
            }
            $field = new ArrayField($extraDataTypeInfo[Field::ARRAY_ELEMENT_TYPE]);
        }
        else if($dataType === Field::GEO) {
            $field = new GeoField();
        }

        if(!empty($field)) {
            $field->updateFromField($fieldInfo);
        }
        return $field;
    }
}