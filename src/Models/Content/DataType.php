<?php

namespace LaunchCMS\Models\Content;


use LaunchCMS\Models\Content\DataObject\ContentTypeDefinition;
use LaunchCMS\Models\Content\Helper\FieldFactory;
use LaunchCMS\Services\Exceptions\DataTypeException;
use LaunchCMS\Services\Facades\StructureService;
use MongoDB\BSON\ObjectID;

/**
 * DataType is a kind of structure which can be used to embed to a field of content type.
 * DataType is similar to ContentType, just limit in 1 level of structure
 * @package LaunchCMS\Models\Content
 */
class DataType extends Structure
{
    protected $collection = 'cms_data_types';
    protected $array_fields = ['fields'];
    protected static $allowFieldTypes = [ Field::BOOLEAN, Field::INTEGER, Field::STRING,
        Field::DOUBLE, Field::DATE, Field::DATETIME,
        Field::REFERENCE, Field::REFERENCE_LIST,
        Field::ARRAY_FIELD, Field::GEO, Field::MEDIA
    ];

    public static function isAllowedFieldType($fieldType)
    {
        return in_array($fieldType, self::$allowFieldTypes);
    }

    public static function fromArray(array $info)
    {
        $dataType = new DataType();
        self::updateGenericInfoFromArray($info, $dataType);
        return $dataType;
    }

    public function addField(Field $field)
    {
        if (empty( $field )) {
            return;
        }
        $dataTypeOfField = $field->getDataType();
        if (empty( $dataTypeOfField )) {
            throw DataTypeException::invalidFieldType();
        }
        if ( !DataType::isAllowedFieldType($dataTypeOfField)) {
            throw DataTypeException::invalidFieldType();
        }
        parent::addField($field);
    }
    protected function updateDataTypeFieldIndexInAllRelatedContentTypes($allRelatedContentTypes, Field $existingField) {
        /** @var ContentType $contentType */
        foreach ($allRelatedContentTypes as $contentType) {
            $collectionName = $contentType->rootContentType()->getAlias();
            $relatedFields = $this->getAllRelatedEmbeddedFieldsInContentType($contentType);
            foreach ($relatedFields as $embeddedField) {
                $nestedFieldAlias = $embeddedField->getAlias();
                $existingField->updateIndexing($collectionName, $nestedFieldAlias);
            }
        }
    }

    protected function updateDataTypeFieldUniqueInAllRelatedContentTypes($allRelatedContentTypes, Field $existingField) {
        /** @var ContentType $contentType */
        foreach ($allRelatedContentTypes as $contentType) {
            $collectionName = $contentType->rootContentType()->getAlias();
            $relatedFields = $this->getAllRelatedEmbeddedFieldsInContentType($contentType);
            foreach ($relatedFields as $embeddedField) {
                $nestedFieldAlias = $embeddedField->getAlias();
                $existingField->updateUniqueIndexing($collectionName, $nestedFieldAlias);
            }
        }
    }

    public function updateField($fieldAlias, Field $field)
    {
        if (empty( $field )) {
            return;
        }
        $dataTypeOfField = $field->getDataType();
        if ( !DataType::isAllowedFieldType($dataTypeOfField)) {
            throw DataTypeException::invalidFieldType();
        }

        $updateResult = parent::updateField($fieldAlias, $field);
        /** @var Field $existingField */
        $existingField = $updateResult[ 'field' ];
        $oldIndexValue = $updateResult[ 'oldIndexValue' ];
        $oldUniqueValue = $updateResult[ 'oldUniqueValue' ];
        $allRelatedContentTypes = StructureService::getAllContentTypeRelatedToDataType($this->_id);

        if ($oldIndexValue !== $existingField->index()) {
            $this->updateDataTypeFieldIndexInAllRelatedContentTypes($allRelatedContentTypes, $existingField);
        }
        if ($oldUniqueValue != $existingField->isUnique()) {
            $this->updateDataTypeFieldUniqueInAllRelatedContentTypes($allRelatedContentTypes, $existingField);
        }
        $this->fields()->save($existingField);
        return $existingField;
    }

    public function updateFromArray(array $info)
    {
        self::updateGenericInfoFromArray($info, $this);
    }

    public function convertToStorableData(array $fieldData) {
        if($fieldData == null) {
            return null;
        }
        $value = [];

        foreach ($fieldData as $key => $objValue) {
            if($key ==='_id') {
                $value[$key] = $objValue;
                continue;
            }
            /** @var Field $field */
            $field = $this->getField($key);
            $concreteField = FieldFactory::getConcreteField($field);
            $storableValue = $concreteField->convertToStorableData($objValue);
            $value[$key] = $storableValue;
        }
        if(!array_key_exists('_id', $value)) {
            $value['_id'] = (string) new ObjectID();
        }
        return $value;
    }

    public function getAllRelatedEmbeddedFieldsInContentType(ContentType $contentType) {
        $allFields = $contentType->getAllFields();
        $relatedEmbeddedFields = [];
        /** @var Field $field */
        foreach ($allFields as $field) {
            $extraInfo = $field->getExtraDataTypeInfo();
            $isRelatedEmbeddedFieldType = ($field->getDataType() === Field::EMBEDDED || $field->getDataType() === Field::EMBEDDED_LIST) ;
            if($isRelatedEmbeddedFieldType && !empty($extraInfo) &&  isset($extraInfo['data_type_id']) &&
                $extraInfo['data_type_id'] === (string) $this->_id) {
                $relatedEmbeddedFields[] = $field;
            }
        }
        return $relatedEmbeddedFields;
    }
}