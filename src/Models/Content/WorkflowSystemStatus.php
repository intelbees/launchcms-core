<?php


namespace LaunchCMS\Models\Content;


class WorkflowSystemStatus
{
    const DRAFT = "draft";
    const UNPUBLISHED = "unpublished";
    const PUBLISHED = "published";
    const SCHEDULED_PUBLISH = "scheduled";
    const REJECTED = "rejected";
    public static $STATUS_ARRAY = [self::DRAFT, self::PUBLISHED, self::UNPUBLISHED, self::REJECTED, self::SCHEDULED_PUBLISH];
    public static function isSystemStatus($statusString) {
        return in_array($statusString, self::$STATUS_ARRAY);
    }
}