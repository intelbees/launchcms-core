<?php


namespace LaunchCMS\Models\Content;

use LaunchCMS\Models\IdentityModel;
use LaunchCMS\Utils\StringUtil;

/**
 * Class FieldGroup
 * @package LaunchCMS\Models\Content
 */
class FieldGroup extends IdentityModel
{
    protected static $unguarded = true;



    public function getPosition()
    {
        return isset( $this->attributes[ 'position' ] ) ? $this->attributes[ 'position' ] : -1;
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }



    public function updateFromFieldGroup(FieldGroup $fieldGroup)
    {
        if (isset( $fieldGroup->attributes[ 'name' ] )) {
            $this->setName($fieldGroup->getName());
        }
        if (isset( $fieldGroup->attributes[ 'alias' ] )) {
            $this->setAlias($fieldGroup->getAlias());
        }
        if (isset( $fieldGroup->attributes[ 'description' ] )) {
            $this->setDescription($fieldGroup->getDescription());
        }
        if (isset( $fieldGroup->attributes[ 'position' ] )) {
            $this->setPosition($fieldGroup->getPosition());
        }
    }

    public static function fromArray(array $info)
    {
        $fieldGroup = new FieldGroup();
        if (isset( $info[ 'name' ] )) {
            $fieldGroup->setName($info[ 'name' ]);
        }
        if (isset( $info[ 'description' ] )) {
            $fieldGroup->setDescription($info[ 'description' ]);
        }
        if (isset( $info[ 'alias' ] )) {
            $fieldGroup->setAlias($info[ 'alias' ]);
        } else {
            $alias = StringUtil::sanitize($fieldGroup->getName(), '_');
            $fieldGroup->setAlias($alias);
        }
        if (isset( $info[ 'position' ] )) {
            $fieldGroup->setPosition($info[ 'position' ]);
        }

        return $fieldGroup;
    }

}