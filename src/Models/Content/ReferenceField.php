<?php


namespace LaunchCMS\Models\Content;


use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Services\Facades\ContentService;
use LaunchCMS\Services\Facades\StructureService;

class ReferenceField extends Field
{
    const MESSAGE_INVALID_REFERENCE_CONTENT = 'launchcms.validation.invalid_reference_content';
    
    public function getReferenceContentTypeID() {
        if(!isset($this->getExtraDataTypeInfo()[self::CONTENT_TYPE_ID])) {
            return null;
        }
        return $this->getExtraDataTypeInfo()[self::CONTENT_TYPE_ID];
    }

    public function __construct($contentTypeID)
    {
        $this->setDataType(self::REFERENCE);
        $this->setExtraDataTypeInfo([ self::CONTENT_TYPE_ID => $contentTypeID ]);
    }

    public function validateValue($value, array &$errors)
    {
        if($value == null) {
            return true;
        }
        /** @var ContentType $contentType */
        $contentType = StructureService::getContentTypeFromCacheByID($this->getReferenceContentTypeID());
        $queryBuilder = ContentService::getQueryBuilder($contentType->getAlias());
        $content = $queryBuilder->where('_id', $value)->first();
        if(empty($content)) {
            $errors[] = trans(self::MESSAGE_INVALID_REFERENCE_CONTENT, [ 'type' => $this->getDataType() ]);
            return false;
        }
        return true;
    }


    public function validateFieldMetaData()
    {
        parent::validateFieldMetaData();
        $contentTypeID = $this->getReferenceContentTypeID();
        if(empty($contentTypeID)) {
            throw FieldException::invalidReferenceContentType();
        }
        $contentType = StructureService::getContentTypeFromCacheByID($contentTypeID);
        if(empty($contentType)) {
            throw FieldException::invalidReferenceContentType();
        }
        return true;
    }
}