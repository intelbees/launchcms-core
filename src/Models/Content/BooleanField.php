<?php


namespace LaunchCMS\Models\Content;


class BooleanField extends Field
{
    public function __construct()
    {
        $this->setDataType('boolean');
        $this->addValidationRule([ 'boolean' ]);
    }

    public function validateValue($value, array &$errors)
    {
        if ($value != null && !is_bool($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }

        return true;
    }
}