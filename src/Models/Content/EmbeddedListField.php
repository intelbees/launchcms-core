<?php


namespace LaunchCMS\Models\Content;


use LaunchCMS\Models\Content\Helper\FieldFactory;

class EmbeddedListField extends EmbeddedField
{
    public function __construct($dataTypeID)
    {
        $this->setDataType(self::EMBEDDED_LIST);
        $this->setExtraDataTypeInfo([ self::DATA_TYPE_ID => $dataTypeID ]);
    }

    protected function validateEmbeddedValue($value, DataType $dataType, array &$errors) {
        foreach ($value as $key => $objValue) {
            if($key === '_id') {
                continue;
            }
            $field = $dataType->getField($key);
            if (empty( $field )) {
                $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);

                return false;
            }
            $concreteField = FieldFactory::getConcreteField($field);
            if ( !$concreteField->validateValue($objValue, $errors)) {
                return false;
            }
        }
        return true;
    }

    public function validateValue($value, array &$errors)
    {
        if($value == null) {
            return true;
        }
        if ( !is_array($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeStructureObject();
        foreach($value as $embeddedObject) {
            if(!is_array($embeddedObject)) {
                $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
                return false;
            }
            $validateEmbeddedElement = $this->validateEmbeddedValue($embeddedObject, $dataType, $errors);
            if(!$validateEmbeddedElement) {
                return false;
            }
        }

        return true;
    }

    public function convertToStorableData($fieldData) {
        if($fieldData == null) {
            return null;
        }
        $value = [];
        foreach ($fieldData as $embeddedDataRow) {
            $persistentDataRow = parent::convertToStorableData($embeddedDataRow);
            if($persistentDataRow != null) {
                $value[] = $persistentDataRow;
            }
        }
        return $value;
    }

    


}