<?php


namespace LaunchCMS\Models\Content;

class MediaField extends Field
{
    const ORIGINAL_FILE_PATH = 'original_path';
    const FILE_CONVERSION_DATA = 'conversions';
    const FILE_TYPE = 'file_type';
    const CONFIG_CONVERSIONS = 'image_resize_aliases';

    public function __construct(array $resizeAliases = null)
    {
        $this->setDataType(self::MEDIA);
        if(!empty($resizeAliases)) {
            $this->setExtraDataTypeInfo([ static::CONFIG_CONVERSIONS => $resizeAliases ]);
        }
    }

    public function getImageResizeAliases() {
        $extraDataTypeInfo = $this->getExtraDataTypeInfo() != null ?  $this->getExtraDataTypeInfo(): [];
        return isset($extraDataTypeInfo[static::CONFIG_CONVERSIONS]) ? $extraDataTypeInfo[static::CONFIG_CONVERSIONS]: [];
    }

    /**
     * @param $path file path relative to root
     * @param null $fileType
     * @param array|null $fileConversions  File conversion must be an array contains [key, value],
     * where key is the resize_alias, value is the resize file path (relative to root)
     * @return array
     */
    public static function createFileData($path, $fileType = null, array $fileConversions = null)
    {
        return [
            static::ORIGINAL_FILE_PATH => $path,
            static::FILE_TYPE => $fileType,
            static::FILE_CONVERSION_DATA => $fileConversions
        ];
    }

    public function validateValue($value, array &$errors)
    {
        if (empty( $value )) {
            return true;
        }
        if ( !is_array($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);

            return false;
        }

        if ( !isset( $value[ static::ORIGINAL_FILE_PATH ] )) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);

            return false;
        }


        return true;
    }


    public function formatToIndexedData($value)
    {
        if ($value == null) {
            return null;
        }

        return [ static::ORIGINAL_FILE_PATH => $value[ static::ORIGINAL_FILE_PATH ] ];
    }


}