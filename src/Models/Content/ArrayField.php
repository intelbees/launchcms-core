<?php


namespace LaunchCMS\Models\Content;


use LaunchCMS\Services\Exceptions\FieldException;

class ArrayField extends Field
{

    /** @var DataType */
    protected $dataTypeStructureObject = null;
    public function __construct($elementType)
    {
        $this->setDataType(self::ARRAY_FIELD);
        $this->setExtraDataTypeInfo([ self::ARRAY_ELEMENT_TYPE => $elementType ]);
    }

    public function geElementType()
    {
        if(!isset($this->getExtraDataTypeInfo()[self::ARRAY_ELEMENT_TYPE])) {
            return null;
        }
        return $this->getExtraDataTypeInfo()[self::ARRAY_ELEMENT_TYPE];
    }


    public function validateValue($value, array &$errors)
    {
        if($value == null) {
            return true;
        }
        if ( !is_array($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }
        $elementType = $this->geElementType();
        foreach ($value as $element) {
            if($elementType === self::STRING && is_string($element)) {
                return true;
            }
            if($elementType === self::INTEGER && is_integer($element)) {
                return true;
            }
            if($elementType === self::BOOLEAN && is_bool($element)) {
                return true;
            }
            if($elementType === self::DOUBLE && is_double($element)) {
                return true;
            }
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }

        return true;
    }

    public function validateFieldMetaData()
    {
        parent::validateFieldMetaData();
        $elementType = $this->geElementType();
        if(empty($elementType)) {
            throw FieldException::invalidArrayElementType();
        }
        $supportedArrayElementTypes = [self::STRING, self::INTEGER, self::DOUBLE, self::BOOLEAN];
        if(!in_array($elementType, $supportedArrayElementTypes)) {
            throw FieldException::invalidArrayElementType();
        }
        return true;
    }

}