<?php


namespace LaunchCMS\Models\Content;


use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Models\Content\Helper\FieldFactory;
use LaunchCMS\Services\Exceptions\FieldException;
use LaunchCMS\Services\Facades\StructureService;

class EmbeddedField extends Field
{
    /** @var DataType */
    protected $dataTypeStructureObject = null;
    public function __construct($dataTypeID)
    {
        $this->setDataType(self::EMBEDDED);
        $this->setExtraDataTypeInfo([ self::DATA_TYPE_ID => $dataTypeID ]);
    }

    public function getDataTypeID()
    {
        if(!isset($this->getExtraDataTypeInfo()[self::DATA_TYPE_ID])) {
            return null;
        }
        return $this->getExtraDataTypeInfo()[self::DATA_TYPE_ID];
    }

    public function getDataTypeStructureObject() {
        if($this->dataTypeStructureObject == null) {
            $dataTypeID = $this->getDataTypeID();
            $this->dataTypeStructureObject = StructureService::getDataTypeFromCacheByID($dataTypeID);
        }
        return $this->dataTypeStructureObject;
    }

    public function validateValue($value, array &$errors)
    {
        if($value == null) {
            return true;
        }
        if ( !is_array($value)) {
            $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
            return false;
        }
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeStructureObject();
        foreach ($value as $key => $objValue) {
            $field = $dataType->getField($key);
            if (empty( $field )) {
                $errors[] = trans(self::MESSAGE_VALUE_NOT_MATCH_DATA_TYPE, [ 'type' => $this->getDataType() ]);
                return false;
            }
            $concreteField = FieldFactory::getConcreteField($field);
            if ( !$concreteField->validateValue($objValue, $errors)) {
                return false;
            }
        }

        return true;
    }

    public function validateFieldMetaData()
    {
        parent::validateFieldMetaData();
        $dataTypeID = $this->getDataTypeID();
        if(empty($dataTypeID)) {
            throw FieldException::invalidReferenceDataType();
        }
        $contentType = StructureService::getDataTypeFromCacheByID($dataTypeID);
        if(empty($contentType)) {
            throw FieldException::invalidReferenceDataType();
        }
        return true;
    }
    
    public function formatToIndexedData($value) {
        $dataTypeID = $this->getExtraDataTypeInfo()[ self::DATA_TYPE_ID ];
        $data = [];
        /** @var DataType $dataType */
        $dataType = StructureService::getDataTypeFromCacheByID($dataTypeID);
        foreach ($value as $key => $objValue) {
            if($key === '_id') {
                continue;
            }
            /** @var Field $field */
            $field = $dataType->getField($key);
            if ($field->allowFullTextSearch()) {
                $concreteField = FieldFactory::getConcreteField($field);
                $data[$key] = $concreteField->formatToIndexedData($value);
            }
        }
        return $data;
    }

    public function convertToStorableData($fieldData) {
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeStructureObject();
        return $dataType->convertToStorableData($fieldData);
    }

    public function processFieldDataBeforeSaving(Content &$content, $fieldDataAlias, $fieldData)
    {
        $content->attributes[ $fieldDataAlias ] = $this->convertToStorableData($fieldData);
    }

    public function makeIndexing($collectionName, $embeddedFieldAlias = null) {
        /** @var DataType $dataType */
        $dataType = $this->getDataTypeStructureObject();
        $allDataTypeFields = $dataType->getAllFields();
        $fieldAlias = $this->getAlias();
        foreach ($allDataTypeFields as $field) {
            $concreteField = FieldFactory::getConcreteField($field);
            $concreteField->makeIndexing($collectionName, $fieldAlias);
        }
    }
}